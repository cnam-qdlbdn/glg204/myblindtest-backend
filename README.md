## Liens

- https://ktor.io/ (authentification par session)
- https://documentation.red-gate.com/fd/quickstart-api-184127575.html
- https://jdbi.org/
- https://github.com/oshai/kotlin-logging/tree/master
- https://github.com/sksamuel/hoplite
- https://kosi-libs.org/kodein/7.19/index.html
- https://cucumber.io/docs/installation/kotlin/

## Pour build le projet

Il faut au moins Java 17. Maven n'est pas nécessaire, le projet contient un wrapper Maven.

Il faut également Docker avec docker-compose. Le fichier `docker-compose.yml` est adapté à ma configuration
personnelle (volume de stockage, timezone, etc.), n'hésitez pas à modifier la configuration selon vos besoins.

Pour build, lancez `mvn package`.

Pour se connecter à l'appli (une fois le frontend démarré), vous pouver créer un utilisateur (via le bouton "
S'inscrire"), ou vous connecter avec l'utilisateur pré-rempli en mode test `User`/`mdp`.