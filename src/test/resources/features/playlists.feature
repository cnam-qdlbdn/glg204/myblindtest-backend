#language: fr
@library @playlist
#noinspection NonAsciiCharacters
Fonctionnalité: Gestion des playlists

  Contexte:
    Soit un utilisateur enregistré et connecté

  Scénario: Créer une playlist
    Soit une playlist avec le nom "Test"
    Quand l'utilisateur crée cette playlist
    Alors la playlist est créée

  Plan du scénario: Erreur de création d'une playlist
    Soit une playlist avec le nom "<name>"
    Et cette playlist existe déjà : <already_exists>
    Quand l'utilisateur crée cette playlist
    Alors l'erreur de création de playlist suivante est renvoyée : "<error_label>"
    Exemples:
      | name                                                 | already_exists | error_label                                            |
      |                                                      | non            | Le nom de la playlist doit avoir au moins 1 caractères |
      | nom de playlist beaucoup trop long pour être accepté | non            | Le nom de la playlist doit avoir au plus 50 caractères |
      | test                                                 | oui            | La playlist [test] existe déjà                         |

  Scénario: Importer une playlist
    Soit une playlist Deezer avec l'URL https://www.deezer.com/fr/playlist/7089916404
    Quand l'utilisateur importe cette playlist
    Alors les chansons de la playlist Deezer sont importées
    Et la playlist est créée
    Et les chansons importées sont ajoutées à la playlist

  Scénario: Éditer une playlist
    Soit une playlist existante avec le nom "Existante"
    Quand l'utilisateur édite cette playlist avec le nom Existante2 et une nouvelle chanson
    Alors le nom de cette playlist est modifié
    Et la chanson est ajoutée à la playlist

  Scénario: Supprimer une playlist
    Soit une playlist quelconque existante avec des chansons associées
    Quand l'utilisateur supprime cette playlist
    Alors la playlist est supprimée
    Mais les chansons associées ne sont pas supprimées

  # TODO tests pour les playlists dynamiques
