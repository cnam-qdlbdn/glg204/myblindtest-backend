#language: fr
@auth
Fonctionnalité: Authentification

  Pour accéder à l'application, un joueur doit s'enregistrer avec des informations de compte, qui lui serviront pour se
  connecter et l'identifier.

  @register
  Scénario: Enregistrement nominal
    Soit un utilisateur inexistant avec le login User
    Et aucun autre utilisateur avec le même login
    Quand l'utilisateur s'enregistre
    Alors son compte est créé

  @register
  Scénario: Enregistrement impossible
    Soit un utilisateur inexistant avec le login User
    Et un autre utilisateur avec le même login
    Quand l'utilisateur s'enregistre
    Alors une erreur de création de compte est renvoyée
    Et son compte n'est pas créé

  @login
  Scénario: Connexion nominale
    Soit un utilisateur existant avec le login User et le mot de passe password
    Quand il se connecte avec son login et son mot de passe
    Alors l'utilisateur est connecté

  @login
  Scénario: Erreur de connexion à cause du mot de passe
    Soit un utilisateur existant avec le login User et le mot de passe password
    Quand il se connecte avec son login et un mauvais mot de passe
    Alors une erreur de connexion est renvoyée
    Et l'utilisateur n'est pas connecté
