#language: fr
@library @tag
Fonctionnalité: Gestion des tags

  Contexte:
    Soit un utilisateur enregistré et connecté

  Plan du scénario: Ajouter un tag
    Soit un tag nommé '<name>' avec un regroupement <grouping>
    Quand l'utilisateur ajoute ce tag
    Alors le tag est créé
    Exemples:
      | name            | grouping |
      | Musique de film | DIRECT   |
      | Genre           | VALEUR   |

  Scénario: Supprimer un tag
    Soit un tag quelconque existant
    Quand l'utilisateur supprime ce tag
    Alors le tag est supprimé