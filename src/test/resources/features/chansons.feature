#language: fr
@library @song
Fonctionnalité: Gestion des chansons

  Contexte:
    Soit un utilisateur enregistré et connecté

  Scénario: Ajouter une chanson depuis Deezer
    Soit une chanson Deezer avec l'URL https://www.deezer.com/track/636405
    Quand l'utilisateur ajoute cette chanson Deezer à sa bibliothèque
    Alors la chanson est ajoutée à la bibliothèque de l'utilisateur

  # TODO ajouter une chanson depuis un fichier

  Scénario: Supprimer une chanson
    Soit une chanson quelconque existante dans la bibliothèque de l'utilisateur
    Quand l'utilisateur supprime cette chanson de sa bibliothèque
    Alors la chanson est supprimée de la bibliothèque de l'utilisateur