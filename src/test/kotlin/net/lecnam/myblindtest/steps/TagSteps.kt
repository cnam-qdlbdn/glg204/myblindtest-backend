package net.lecnam.myblindtest.steps

import io.cucumber.java8.Fr
import io.kotest.assertions.ktor.client.shouldHaveStatus
import io.kotest.common.runBlocking
import io.kotest.matchers.equals.shouldBeEqual
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.nulls.shouldNotBeNull
import io.ktor.client.call.body
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.request.delete
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.client.statement.HttpResponse
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.contentType
import io.ktor.serialization.jackson.jackson
import io.ktor.server.application.*
import io.ktor.server.testing.TestApplication
import net.lecnam.myblindtest.base.InsertedId
import net.lecnam.myblindtest.librairie.tag.CreateTagDTO
import net.lecnam.myblindtest.librairie.tag.TagDAO
import net.lecnam.myblindtest.librairie.tag.TagInsertForm
import net.lecnam.myblindtest.librairie.tag.TypeRegroupementTag
import net.lecnam.myblindtest.module
import org.kodein.di.instance
import org.kodein.di.ktor.closestDI
import kotlin.properties.Delegates.notNull

@Suppress("unused")
class TagSteps : Fr {
    init {
        lateinit var createTagDTO: CreateTagDTO
        var idTag by notNull<Long>()
        lateinit var response: HttpResponse

        lateinit var app: TestApplication
        lateinit var internalApp: Application

        Before("@tag") { _ ->
            app = TestApplication {
                application {
                    module(eagerDI = false)

                    internalApp = this
                }
            }

            app.start()
        }

        After("@tag") { _ ->
            app.stop()
        }

        Soit("un tag nommé {string} avec un regroupement {word}") { nom: String, regroupement: String ->
            createTagDTO = CreateTagDTO(nom, null, TypeRegroupementTag.valueOf(regroupement))
        }

        Soit("un tag quelconque existant") {
            val tagDAO by closestDI { internalApp }.instance<TagDAO>()
            idTag = tagDAO.insert(
                TagInsertForm(
                    nom = "toDelete",
                    typeRegroupement = TypeRegroupementTag.DIRECT,
                    refJoueur = commonSteps!!.connectedJoueur.id
                )
            )
        }

        Quand("l'utilisateur ajoute ce tag") {
            response = runBlocking {
                val client = app.createClient {
                    install(ContentNegotiation) {
                        jackson()
                    }
                }

                client.post("/tag") {
                    contentType(ContentType.Application.Json)
                    headers.append("Cookie", commonSteps!!.authenticationCookie)
                    setBody(createTagDTO)
                }
            }
        }

        Quand("l'utilisateur supprime ce tag") {
            response = runBlocking {
                val client = app.createClient { }

                client.delete("/tag/$idTag") {
                    headers.append("Cookie", commonSteps!!.authenticationCookie)
                }
            }
        }

        Alors("le tag est créé") {
            response shouldHaveStatus HttpStatusCode.Created
            runBlocking {
                val insertedId = response.body<InsertedId>().id
                val tagDAO by closestDI { internalApp }.instance<TagDAO>()
                val tag = tagDAO.findById(insertedId, commonSteps!!.connectedJoueur.id)

                tag.shouldNotBeNull()
                tag.nom shouldBeEqual createTagDTO.nom
                tag.description.shouldBeNull()
                tag.typeRegroupement shouldBeEqual createTagDTO.typeRegroupement
            }
        }

        Alors("le tag est supprimé") {
            response shouldHaveStatus HttpStatusCode.NoContent

            val tagDAO by closestDI { internalApp }.instance<TagDAO>()
            val tag = tagDAO.findById(idTag, commonSteps!!.connectedJoueur.id)

            tag.shouldBeNull()
        }
    }
}
