package net.lecnam.myblindtest.steps

import io.cucumber.java8.Fr
import io.kotest.assertions.ktor.client.shouldHaveStatus
import io.kotest.matchers.collections.shouldContain
import io.kotest.matchers.collections.shouldContainOnly
import io.kotest.matchers.collections.shouldNotContain
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.string.shouldContain
import io.ktor.client.call.body
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.client.statement.HttpResponse
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.contentType
import io.ktor.serialization.jackson.jackson
import io.ktor.server.application.*
import io.ktor.server.auth.UserPasswordCredential
import io.ktor.server.testing.TestApplication
import kotlinx.coroutines.runBlocking
import net.lecnam.myblindtest.authentication.AuthenticationDAO
import net.lecnam.myblindtest.authentication.AuthenticationService
import net.lecnam.myblindtest.exceptions.ErrorMessageContainer
import net.lecnam.myblindtest.module
import net.lecnam.myblindtest.plugins.SESSION_KEY
import org.junit.Assume
import org.kodein.di.instance
import org.kodein.di.ktor.closestDI

@Suppress("unused")
class AuthenticationSteps : Fr {

    init {
        lateinit var loginExistant: String
        lateinit var passwordExistant: String
        lateinit var loginInexistant: String
        lateinit var passwordRegister: String
        lateinit var response: HttpResponse

        lateinit var app: TestApplication
        lateinit var internalApp: Application

        Before("@auth") { _ ->
            app = TestApplication {
                application {
                    module(eagerDI = false)

                    internalApp = this
                }
            }

            app.start()
        }

        After("@auth") { _ ->
            app.stop()
        }

        Soit("un utilisateur existant avec le login {word} et le mot de passe {word}") { login: String, pass: String ->
            loginExistant = login
            passwordExistant = pass

            val authenticationDAO by closestDI { internalApp }.instance<AuthenticationDAO>()
            authenticationDAO.register(loginExistant, passwordExistant)
        }

        Soit("un utilisateur inexistant avec le login {word}") { login: String ->
            loginInexistant = login
        }

        Et("aucun autre utilisateur avec le même login") {
            val authenticationDAO by closestDI { internalApp }.instance<AuthenticationDAO>()

            Assume.assumeFalse(authenticationDAO.existsByLogin(loginInexistant))
        }

        Et("un autre utilisateur avec le même login") {
            val authenticationDAO by closestDI { internalApp }.instance<AuthenticationDAO>()

            authenticationDAO.register(loginInexistant, "whatever")
        }

        Quand("il se connecte avec son login et son mot de passe") {
            response = runBlocking {
                val client = app.createClient {
                    install(ContentNegotiation) {
                        jackson()
                    }
                }

                client.post("/login") {
                    contentType(ContentType.Application.Json)
                    setBody(UserPasswordCredential(loginExistant, passwordExistant))
                }
            }
        }

        Quand("il se connecte avec son login et un mauvais mot de passe") {
            response = runBlocking {
                val client = app.createClient {
                    install(ContentNegotiation) {
                        jackson()
                    }
                }

                client.post("/login") {
                    contentType(ContentType.Application.Json)
                    setBody(UserPasswordCredential(loginExistant, passwordExistant + "1"))
                }
            }
        }

        Quand("l'utilisateur s'enregistre") {
            response = runBlocking {
                val client = app.createClient {
                    install(ContentNegotiation) {
                        jackson()
                    }
                }

                passwordRegister = "monMdpSuperSecret"
                client.post("/register") {
                    contentType(ContentType.Application.Json)
                    setBody(UserPasswordCredential(loginInexistant, passwordRegister))
                }
            }
        }

        Alors("l'utilisateur est connecté") {
            response shouldHaveStatus HttpStatusCode.OK
            response.headers.names() shouldContain "Set-Cookie"
            response.headers["Set-Cookie"] shouldContain Regex("^$SESSION_KEY=.+")
        }

        Alors("une erreur de connexion est renvoyée") {
            response shouldHaveStatus HttpStatusCode.Unauthorized
            runBlocking {
                response.body<ErrorMessageContainer>().reasons.shouldContainOnly("Identifiants invalides")
            }
        }

        Et("l'utilisateur n'est pas connecté") {
            response.headers.names() shouldNotContain "Set-Cookie"
        }

        Alors("son compte est créé") {
            response shouldHaveStatus HttpStatusCode.OK

            val authenticationService by closestDI { internalApp }.instance<AuthenticationService>()
            authenticationService.findMatchingJoueur(loginInexistant, passwordRegister).shouldNotBeNull()
        }

        Alors("une erreur de création de compte est renvoyée") {
            response shouldHaveStatus HttpStatusCode.BadRequest
            runBlocking {
                response.body<ErrorMessageContainer>().reasons.shouldContainOnly("Le login [$loginInexistant] est déjà utilisé")
            }
        }

        Alors("son compte n'est pas créé") {
            val authenticationService by closestDI { internalApp }.instance<AuthenticationService>()
            authenticationService.findMatchingJoueur(loginInexistant, passwordRegister).shouldBeNull()
        }
    }
}
