package net.lecnam.myblindtest.steps

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.cucumber.java8.Fr
import io.kotest.assertions.ktor.client.shouldHaveStatus
import io.kotest.common.runBlocking
import io.kotest.matchers.equals.shouldBeEqual
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldNotBe
import io.kotest.matchers.types.shouldBeTypeOf
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.delete
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.client.statement.HttpResponse
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.contentType
import io.ktor.serialization.jackson.jackson
import io.ktor.server.application.*
import io.ktor.server.response.header
import io.ktor.server.response.respond
import io.ktor.server.routing.get
import io.ktor.server.routing.routing
import io.ktor.server.testing.TestApplication
import net.lecnam.myblindtest.api.deezer.Track
import net.lecnam.myblindtest.base.InsertedId
import net.lecnam.myblindtest.librairie.chanson.ChansonDAO
import net.lecnam.myblindtest.librairie.chanson.ChansonDeezer
import net.lecnam.myblindtest.librairie.chanson.ChansonService
import net.lecnam.myblindtest.librairie.chanson.ChansonUrl
import net.lecnam.myblindtest.module
import org.junit.Assume
import org.kodein.di.instance
import org.kodein.di.ktor.closestDI
import java.io.File
import kotlin.properties.Delegates.notNull

@Suppress("unused")
class ChansonSteps : Fr {
    init {
        lateinit var response: HttpResponse
        lateinit var deezerUrl: String
        lateinit var deezerTrackFile: File
        var idChanson by notNull<Long>()

        lateinit var app: TestApplication
        lateinit var internalApp: Application

        Before("@song") { _ ->
            val httpClient by lazy {
                app.createClient {
                    install(io.ktor.client.plugins.contentnegotiation.ContentNegotiation) {
                        jackson()
                    }
                }
            }

            app = TestApplication {
                application {
                    module(httpClient, eagerDI = false)

                    internalApp = this
                }

                externalServices {
                    hosts("https://api.deezer.com") {
                        routing {
                            get("track/(?<trackId>\\d+)".toRegex()) {
                                val trackId = call.parameters["trackId"]!!
                                deezerTrackFile =
                                    File(this@ChansonSteps.javaClass.getResource("/deezer/track_$trackId.json")!!.file)
                                Assume.assumeTrue(
                                    "Le fichier track_$trackId.json doit exister",
                                    deezerTrackFile.exists() && deezerTrackFile.isFile
                                )

                                call.response.header("Content-Type", "application/json")
                                call.respond(deezerTrackFile.readText())
                            }
                        }
                    }
                }
            }

            app.start()
        }

        After("@song") { _ ->
            app.stop()
        }

        Soit("une chanson Deezer avec l'URL {word}") { url: String ->
            deezerUrl = url
        }

        Soit("une chanson quelconque existante dans la bibliothèque de l'utilisateur") {
            val chansonService by closestDI { internalApp }.instance<ChansonService>()

            idChanson = chansonService.insert(
                ChansonUrl.Deezer("https://api.deezer.com/track/998635"),
                commonSteps!!.connectedJoueur.id
            )
                .getOrNull()!!
        }

        Quand("l'utilisateur ajoute cette chanson {word} à sa bibliothèque") { typeChanson: String ->
            Assume.assumeTrue("$typeChanson non implémenté", typeChanson in listOf("Deezer"))

            if (typeChanson == "Deezer") {
                response = runBlocking {
                    val client by closestDI { internalApp }.instance<HttpClient>()

                    client.post("/chanson/deezer") {
                        contentType(ContentType.Application.Json)
                        headers.append("Cookie", commonSteps!!.authenticationCookie)
                        setBody(ChansonUrl.Deezer(deezerUrl))
                    }
                }
            }
        }

        Quand("l'utilisateur supprime cette chanson de sa bibliothèque") {
            response = runBlocking {
                val client by closestDI { internalApp }.instance<HttpClient>()

                client.delete("/chanson/$idChanson") {
                    headers.append("Cookie", commonSteps!!.authenticationCookie)
                }
            }
        }

        Alors("la chanson est ajoutée à la bibliothèque de l'utilisateur") {
            response shouldHaveStatus HttpStatusCode.Created
            runBlocking {
                val insertedId = response.body<InsertedId>().id
                val chansonDAO by closestDI { internalApp }.instance<ChansonDAO>()
                val objectMapper by closestDI { internalApp }.instance<ObjectMapper>()
                val chanson = chansonDAO.findById(insertedId, commonSteps!!.connectedJoueur.id)

                val track = objectMapper.readValue<Track>(deezerTrackFile)

                chanson.shouldNotBeNull()
                chanson.titre shouldBeEqual track.title
                chanson.artiste shouldNotBe null
                chanson.artiste!!.nom shouldBeEqual track.artist.name
                chanson.shouldBeTypeOf<ChansonDeezer>()
                chanson.idDeezer shouldBeEqual track.id
                chanson.extrait shouldBeEqual track.preview
                chanson.image shouldBeEqual track.artist.picture_xl
            }
        }

        Alors("la chanson est supprimée de la bibliothèque de l'utilisateur") {
            response shouldHaveStatus HttpStatusCode.NoContent

            val chansonDAO by closestDI { internalApp }.instance<ChansonDAO>()

            val chanson = chansonDAO.findById(idChanson, commonSteps!!.connectedJoueur.id)

            chanson.shouldBeNull()
        }
    }
}
