package net.lecnam.myblindtest.steps

import com.sksamuel.hoplite.ConfigLoaderBuilder
import io.cucumber.java8.Fr
import io.kotest.common.runBlocking
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.cookies.HttpCookies
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.http.ContentType
import io.ktor.http.contentType
import io.ktor.serialization.jackson.jackson
import io.ktor.server.application.*
import io.ktor.server.auth.UserPasswordCredential
import io.ktor.server.testing.TestApplication
import net.lecnam.myblindtest.Configuration
import net.lecnam.myblindtest.authentication.AuthenticationDAO
import net.lecnam.myblindtest.joueur.JoueurDTO
import net.lecnam.myblindtest.joueur.JoueurService
import net.lecnam.myblindtest.module
import net.lecnam.myblindtest.plugins.SESSION_KEY
import net.lecnam.myblindtest.plugins.migrateDatabase
import org.kodein.di.instance
import org.kodein.di.ktor.closestDI
import org.testcontainers.containers.PostgreSQLContainer

var commonSteps: CommonSteps? = null

class CommonSteps : Fr {

    private var _configuration = loadConfiguration()

    private val dbContainer = PostgreSQLContainer("postgres:15.3")
        .withUsername(_configuration.connection.db.username)
        .withPassword(_configuration.connection.db.password.value)
        .withEnv("TZ", "GMT+11")
        .withEnv("PGTZ", "GMT+11")

    private lateinit var _authenticationCookie: String

    val authenticationCookie
        get() = _authenticationCookie

    private lateinit var _connectedJoueur: JoueurDTO

    val connectedJoueur
        get() = _connectedJoueur

    init {
        Before { _ ->
            dbContainer.start()
            System.setProperty("config.override.connection.db.url", dbContainer.jdbcUrl)
            System.setProperty("config.override.flyway.runClean", true.toString())
            _configuration = this.loadConfiguration()
            migrateDatabase(_configuration)

            commonSteps = this
        }

        After { _ ->
            dbContainer.stop()

            commonSteps = null
        }

        Soit("un utilisateur enregistré et connecté") {
            lateinit var app: TestApplication
            lateinit var internalApp: Application

            val httpClient by lazy {
                app.createClient {
                    install(HttpCookies)
                    install(ContentNegotiation) {
                        jackson()
                    }
                }
            }

            app = TestApplication {
                application {
                    module(httpClient, eagerDI = false)

                    internalApp = this
                }
            }

            app.start()

            val authenticationDAO by closestDI { internalApp }.instance<AuthenticationDAO>()
            val joueurService by closestDI { internalApp }.instance<JoueurService>()
            val login = "CucumberTestUser"
            val password = "mdp"
            authenticationDAO.register(login, password)

            val response = runBlocking {
                httpClient.post("/login") {
                    contentType(ContentType.Application.Json)
                    setBody(UserPasswordCredential(login, password))
                }
            }
            _authenticationCookie = response.headers
                .getAll("Set-Cookie")
                ?.find { it.startsWith(SESSION_KEY) }!!
            _connectedJoueur = joueurService.findById(authenticationDAO.findMatching(login, password)!!)!!

            app.stop()
        }
    }

    private fun loadConfiguration() = ConfigLoaderBuilder.default()
        .build()
        .loadConfigOrThrow<Configuration>("/config.yml")
}
