package net.lecnam.myblindtest.steps

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.cucumber.java8.Fr
import io.kotest.assertions.ktor.client.shouldHaveStatus
import io.kotest.common.runBlocking
import io.kotest.matchers.collections.shouldBeEmpty
import io.kotest.matchers.collections.shouldContainExactlyInAnyOrder
import io.kotest.matchers.collections.shouldHaveSingleElement
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.equals.shouldBeEqual
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldBe
import io.ktor.client.call.body
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.request.delete
import io.ktor.client.request.patch
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.client.statement.HttpResponse
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.contentType
import io.ktor.serialization.jackson.jackson
import io.ktor.server.application.*
import io.ktor.server.response.header
import io.ktor.server.response.respond
import io.ktor.server.routing.get
import io.ktor.server.routing.routing
import io.ktor.server.testing.TestApplication
import net.lecnam.myblindtest.api.deezer.DeezerPlaylist
import net.lecnam.myblindtest.base.InsertedId
import net.lecnam.myblindtest.exceptions.ErrorMessageContainer
import net.lecnam.myblindtest.librairie.chanson.ArtisteDTO
import net.lecnam.myblindtest.librairie.chanson.ChansonDAO
import net.lecnam.myblindtest.librairie.chanson.ChansonDeezerInsertForm
import net.lecnam.myblindtest.librairie.chanson.ChansonUrl
import net.lecnam.myblindtest.librairie.playlist.CreatePlaylistDTO
import net.lecnam.myblindtest.librairie.playlist.PlaylistDAO
import net.lecnam.myblindtest.librairie.playlist.PlaylistDTO
import net.lecnam.myblindtest.librairie.playlist.PlaylistInsertForm
import net.lecnam.myblindtest.librairie.playlist.PlaylistUpdateForm
import net.lecnam.myblindtest.librairie.playlist.UpdatePlaylistDTO
import net.lecnam.myblindtest.module
import org.junit.Assume
import org.kodein.di.instance
import org.kodein.di.ktor.closestDI
import java.io.File
import kotlin.properties.Delegates.notNull

@Suppress("unused")
class PlaylistSteps : Fr {

    private lateinit var internalApp: Application

    private lateinit var deezerPlaylistFile: File

    init {
        lateinit var nomPlaylist: String
        var existingPlaylistId by notNull<Long>()
        var insertedChansonId by notNull<Long>()
        lateinit var updatedPlaylist: PlaylistDTO
        lateinit var deezerPlaylistUrl: String
        lateinit var importedChansons: List<Long>
        lateinit var response: HttpResponse

        lateinit var app: TestApplication

        Before("@playlist") { _ ->
            val httpClient by lazy {
                app.createClient {
                    install(ContentNegotiation) {
                        jackson()
                    }
                }
            }

            app = TestApplication {
                application {
                    module(httpClient, eagerDI = false)

                    internalApp = this
                }

                externalServices {
                    hosts("https://api.deezer.com") {
                        routing {
                            get("track/(?<trackId>\\d+)".toRegex()) {
                                val trackId = call.parameters["trackId"]!!
                                val deezerTrackFile =
                                    File(this@PlaylistSteps.javaClass.getResource("/deezer/track_$trackId.json")!!.file)
                                Assume.assumeTrue(
                                    "Le fichier track_$trackId.json doit exister",
                                    deezerTrackFile.exists() && deezerTrackFile.isFile
                                )

                                call.response.header("Content-Type", "application/json")
                                call.respond(deezerTrackFile.readText())
                            }

                            get("playlist/(?<playlistId>\\d+)".toRegex()) {
                                val playlistId = call.parameters["playlistId"]!!
                                deezerPlaylistFile =
                                    File(this@PlaylistSteps.javaClass.getResource("/deezer/playlist_$playlistId.json")!!.file)
                                Assume.assumeTrue(
                                    "Le fichier playlist_$playlistId.json doit exister",
                                    deezerPlaylistFile.exists() && deezerPlaylistFile.isFile
                                )

                                call.response.header("Content-Type", "application/json")
                                call.respond(deezerPlaylistFile.readText())
                            }
                        }
                    }
                }
            }

            app.start()
        }

        After("@playlist") { _ ->
            app.stop()
        }

        Soit("une playlist avec le nom {string}") { nom: String ->
            nomPlaylist = nom
        }

        Et("cette playlist existe déjà : {word}") { alreadyExistsStr: String ->
            val playlistAlreadyExists = when (alreadyExistsStr) {
                "oui" -> true
                "non" -> false
                else -> throw IllegalArgumentException(alreadyExistsStr)
            }

            if (playlistAlreadyExists) {
                val playlistDAO: PlaylistDAO by closestDI { internalApp }.instance()
                playlistDAO.insert(PlaylistInsertForm(commonSteps!!.connectedJoueur.id, nomPlaylist))
            }
        }

        Soit("une playlist existante avec le nom {string}") { nom: String ->
            val playlistDAO: PlaylistDAO by closestDI { internalApp }.instance()

            existingPlaylistId = playlistDAO.insert(PlaylistInsertForm(commonSteps!!.connectedJoueur.id, nom))
        }

        Soit("une playlist quelconque existante avec des chansons associées") {
            insertedChansonId = insertChanson()

            val playlistDAO: PlaylistDAO by closestDI { internalApp }.instance()
            existingPlaylistId = playlistDAO.insert(PlaylistInsertForm(commonSteps!!.connectedJoueur.id, "test"))

            playlistDAO.update(
                PlaylistUpdateForm(
                    existingPlaylistId,
                    commonSteps!!.connectedJoueur.id,
                    chansons = listOf(insertedChansonId)
                )
            )
        }

        Soit("une playlist Deezer avec l'URL {word}") { url: String ->
            deezerPlaylistUrl = url
        }

        Quand("l'utilisateur crée cette playlist") {
            response = runBlocking {
                val client = app.createClient {
                    install(ContentNegotiation) {
                        jackson()
                    }
                }

                client.post("/playlist") {
                    contentType(ContentType.Application.Json)
                    headers.append("Cookie", commonSteps!!.authenticationCookie)
                    setBody(CreatePlaylistDTO(nomPlaylist))
                }
            }
        }

        Quand("l'utilisateur édite cette playlist avec le nom {word} et une nouvelle chanson") { nom: String ->
            nomPlaylist = nom

            insertedChansonId = insertChanson()

            response = runBlocking {
                val client = app.createClient {
                    install(ContentNegotiation) {
                        jackson()
                    }
                }

                client.patch("/playlist/$existingPlaylistId") {
                    contentType(ContentType.Application.Json)
                    headers.append("Cookie", commonSteps!!.authenticationCookie)
                    setBody(UpdatePlaylistDTO(nom, listOf(insertedChansonId)))
                }
            }
        }

        Quand("l'utilisateur supprime cette playlist") {
            response = runBlocking {
                val client = app.createClient { }

                client.delete("/playlist/$existingPlaylistId") {
                    headers.append("Cookie", commonSteps!!.authenticationCookie)
                }
            }
        }

        Quand("l'utilisateur importe cette playlist") {
            response = runBlocking {
                val client = app.createClient {
                    install(ContentNegotiation) {
                        jackson()
                    }
                }

                client.post("/playlist/deezer") {
                    contentType(ContentType.Application.Json)
                    headers.append("Cookie", commonSteps!!.authenticationCookie)
                    setBody(ChansonUrl.Deezer(deezerPlaylistUrl))
                }
            }
        }

        Alors("la playlist est créée") {
            response shouldHaveStatus HttpStatusCode.Created

            runBlocking {
                val insertedId = response.body<InsertedId>().id
                val playlistDAO by closestDI { internalApp }.instance<PlaylistDAO>()
                val playlist = playlistDAO.findById(insertedId, commonSteps!!.connectedJoueur.id)

                playlist.shouldNotBeNull()
                if (!::deezerPlaylistFile.isInitialized) {
                    playlist.nom shouldBeEqual nomPlaylist
                    playlist.chansons.shouldBeEmpty()
                }
            }
        }

        Alors("l'erreur de création de playlist suivante est renvoyée : {string}") { errorMessage: String ->
            response shouldHaveStatus HttpStatusCode.BadRequest

            runBlocking {
                val errorReasons = response.body<ErrorMessageContainer>().reasons
                errorReasons shouldHaveSingleElement errorMessage
            }
        }

        Alors("le nom de cette playlist est modifié") {
            response shouldHaveStatus HttpStatusCode.OK

            runBlocking {
                val playlist = response.body<PlaylistDTO>()
                updatedPlaylist = playlist

                playlist.nom shouldBeEqual nomPlaylist
            }
        }

        Et("la chanson est ajoutée à la playlist") {
            updatedPlaylist.chansons shouldHaveSize 1
            updatedPlaylist.chansons.first() shouldBeEqual insertedChansonId
        }

        Alors("la playlist est supprimée") {
            response shouldHaveStatus HttpStatusCode.NoContent

            val playlistDAO: PlaylistDAO by closestDI { internalApp }.instance()
            playlistDAO.existsById(existingPlaylistId, commonSteps!!.connectedJoueur.id) shouldBe false
        }

        Mais("les chansons associées ne sont pas supprimées") {
            val chansonDAO: ChansonDAO by closestDI { internalApp }.instance()

            chansonDAO.existsById(insertedChansonId, commonSteps!!.connectedJoueur.id) shouldBe true
        }

        Alors("les chansons de la playlist Deezer sont importées") {
            response shouldHaveStatus HttpStatusCode.Created

            runBlocking {
                val chansonDAO by closestDI { internalApp }.instance<ChansonDAO>()
                val objectMapper by closestDI { internalApp }.instance<ObjectMapper>()

                val playlistDeezer = objectMapper.readValue<DeezerPlaylist>(deezerPlaylistFile)

                importedChansons = chansonDAO.findAll(commonSteps!!.connectedJoueur.id).map { it.id }
                importedChansons shouldHaveSize playlistDeezer.tracks.data.size
            }
        }

        Et("les chansons importées sont ajoutées à la playlist") {
            runBlocking {
                val insertedId = response.body<InsertedId>().id
                val playlistDAO by closestDI { internalApp }.instance<PlaylistDAO>()
                val playlist = playlistDAO.findById(insertedId, commonSteps!!.connectedJoueur.id)

                playlist.shouldNotBeNull()
                playlist.chansons shouldContainExactlyInAnyOrder importedChansons
            }
        }
    }

    private fun insertChanson(): Long {
        val chansonDAO: ChansonDAO by closestDI { internalApp }.instance()
        return chansonDAO.insertChanson(
            ChansonDeezerInsertForm(
                "title",
                ArtisteDTO("artiste"),
                commonSteps!!.connectedJoueur.id,
                123456,
                "extrait",
                "image"
            )
        )
    }
}
