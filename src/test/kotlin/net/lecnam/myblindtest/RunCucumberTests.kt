package net.lecnam.myblindtest

import io.cucumber.junit.platform.engine.Constants
import org.junit.platform.suite.api.ConfigurationParameter
import org.junit.platform.suite.api.IncludeEngines
import org.junit.platform.suite.api.SelectClasspathResource
import org.junit.platform.suite.api.Suite

@Suite
@IncludeEngines("cucumber")
@SelectClasspathResource("features")
// @ConfigurationParameter(key = "cucumber.filter.tags", value = "@tag")
@ConfigurationParameter(key = Constants.GLUE_PROPERTY_NAME, value = "net.lecnam.myblindtest.steps")
@ConfigurationParameter(key = Constants.PLUGIN_PROPERTY_NAME, value = "pretty")
@ConfigurationParameter(key = Constants.PLUGIN_PROPERTY_NAME, value = "html:target/cucumber-reports.html")
class RunCucumberTests
