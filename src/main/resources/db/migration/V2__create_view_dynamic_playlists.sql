create view dynamic_playlist as
select t.id as id_tag, t.fk_joueur as id_joueur, null as valeur_tag, array_agg(vt.fk_chanson) as chansons
from tag t
         join valeur_tag vt on t.id = vt.fk_tag
where type_regroupement = 'DIRECT'
group by t.id
union
select t.id as id_tag, t.fk_joueur as id_joueur, vt.valeur as valeur_tag, array_agg(vt.fk_chanson) as chansons
from tag t
         join valeur_tag vt on t.id = vt.fk_tag
where type_regroupement = 'VALEUR'
group by t.id, vt.valeur;