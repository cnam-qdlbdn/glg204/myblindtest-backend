create
    extension if not exists pgcrypto;

create table joueur
(
    id            bigserial   not null primary key,
    login         varchar(50) not null unique,
    pseudo        varchar(50) unique   default null,
    passhash      text        not null,
    date_creation timestamptz not null default now()
);

create table joueur_auth
(
    id         uuid        not null primary key default gen_random_uuid(),
    fk_joueur  bigint      not null references joueur (id) on delete cascade,
    date_debut timestamptz not null,
    date_fin   timestamptz not null,
    CHECK ( date_fin > date_debut )
);

create table tag
(
    id                bigserial not null primary key,
    nom               text      not null,
    description       text,
    type_regroupement text      not null,
    fk_joueur         bigint    not null references joueur (id),
    unique (fk_joueur, nom)
);

create table artiste
(
    id  bigserial not null primary key,
    nom text      not null
);

create table chanson
(
    id         bigserial not null primary key,
    titre      text      not null,
    fk_artiste bigint references artiste (id),
    fk_joueur  bigint    not null references joueur (id)
);

create table chanson_deezer
(
    id_chanson bigint not null primary key references chanson (id) on delete cascade,
    id_deezer  bigint not null,
    extrait    text   not null, -- d'après les guidelines Deezer, il est interdit de stocker de la donnée audio provenant de chez eux
    image      text   not null  -- pareil
);

-- J'ajouterai les chansons "fichier" plus tard. Trop compliqué pour le moment :(

create table playlist
(
    id        bigserial   not null primary key,
    nom       varchar(50) not null,
    fk_joueur bigint      not null references joueur (id),
    unique (nom, fk_joueur)
);

create table valeur_tag
(
    fk_chanson bigint not null references chanson (id) on delete cascade,
    fk_tag     bigint not null references tag (id) on delete cascade,
    valeur     text   not null,
    primary key (fk_chanson, fk_tag)
);

create table chanson_dans_playlist
(
    fk_playlist bigint not null references playlist (id) on delete cascade,
    fk_chanson  bigint not null references chanson (id) on delete cascade,
    primary key (fk_playlist, fk_chanson)
);

create table partie
(
    id             bigserial   not null primary key,
    code_partie    text        not null,
    nombre_manches int         not null,
    numero_manche  int         not null default 1,
    check_text     boolean     not null, -- si non, les joueurs n'ont pas à saisir de texte pour leur réponse (ça se fait à l'oral). Implique le buzzer
    buzzer         boolean     not null, -- si oui, le buzzer est bloqué jusqu'à ce que tous les participants aient buzzés, puis il est reset. Sinon, pas de buzzer, tout le monde répond en même temps
    mode_blitz     boolean     not null, -- le plus rapide, sinon tout le monde réponds jusqu'à la fin du temps.
    check_all      boolean     not null, -- si oui, les joueurs doivent tout deviner. Incompatible avec le buzzer
    etat           text        not null,
    fk_createur    bigint      not null references joueur (id),
    ref_playlist   jsonb       not null, -- indique soit une playlist statique, soit une playlist dynamique par l'id du tag, du joueur, et la valeur du tag s'il y en a
    nom_playlist   text        not null,
    date_creation  timestamptz not null default now()
);

create table chansons_partie
(
    fk_partie  bigint not null references partie (id) on delete cascade,
    fk_chanson bigint not null references chanson (id) on delete cascade, -- on ne veut pas empêcher la suppression d'un tag une fois la partie terminée
    sequence   int    not null,
    primary key (fk_partie, fk_chanson)
);

create table resultat_manche
(
    id            bigserial not null primary key,
    fk_partie     bigint    not null references partie (id) on delete cascade,
    fk_joueur     bigint    not null references joueur (id) on delete cascade,
    numero_manche int       not null,
    is_titre      boolean   not null,
    is_artiste    boolean   not null,
    fk_tag        bigint references tag (id),
    score         int       not null
);

create table participant
(
    fk_joueur     bigint  not null references joueur (id),
    fk_partie     bigint  not null references partie (id) on delete cascade,
    maitre_joueur boolean not null default false,
    connected     boolean not null default true,
    primary key (fk_joueur, fk_partie)
);

create table historique_partie
(
    id            bigserial   not null primary key,
    nom_playlist  text        not null,
    score         int         not null,
    maitre_joueur boolean     not null,
    fk_joueur     bigint      not null references joueur (id) on delete cascade,
    date_creation timestamptz not null default now()
);
