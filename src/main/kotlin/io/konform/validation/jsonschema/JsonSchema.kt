package io.konform.validation.jsonschema

import io.konform.validation.Constraint
import io.konform.validation.ValidationBuilder
import kotlin.math.roundToInt

inline fun <reified T> ValidationBuilder<*>.type() =
    addConstraint(
        "doit être du type "
    ) { it is T }

fun <T> ValidationBuilder<T>.enum(vararg allowed: T) =
    addConstraint(
        "doit faire partie de: {0}",
        allowed.joinToString("', '", "'", "'")
    ) { it in allowed }

inline fun <reified T : Enum<T>> ValidationBuilder<String>.enum(): Constraint<String> {
    val enumNames = enumValues<T>().map { it.name }
    return addConstraint(
        "doit faire partie de: {0}",
        enumNames.joinToString("', '", "'", "'")
    ) { it in enumNames }
}

fun <T> ValidationBuilder<T>.const(expected: T) =
    addConstraint(
        "doit être {0}",
        expected?.let { "'$it'" } ?: "null"
    ) { expected == it }

fun <T : Number> ValidationBuilder<T>.multipleOf(factor: Number): Constraint<T> {
    val factorAsDouble = factor.toDouble()
    require(factorAsDouble > 0) { "multipleOf nécessite que le facteur soit strictement positif" }
    return addConstraint("doit être un multiple de '{0}'", factor.toString()) {
        val division = it.toDouble() / factorAsDouble
        division.compareTo(division.roundToInt()) == 0
    }
}

fun <T : Number> ValidationBuilder<T>.maximum(maximumInclusive: Number) = addConstraint(
    "doit être au plus '{0}'",
    maximumInclusive.toString()
) { it.toDouble() <= maximumInclusive.toDouble() }

fun <T : Number> ValidationBuilder<T>.exclusiveMaximum(maximumExclusive: Number) = addConstraint(
    "doit être moins que '{0}'",
    maximumExclusive.toString()
) { it.toDouble() < maximumExclusive.toDouble() }

fun <T : Number> ValidationBuilder<T>.minimum(minimumInclusive: Number) = addConstraint(
    "doit être au moins '{0}'",
    minimumInclusive.toString()
) { it.toDouble() >= minimumInclusive.toDouble() }

fun <T : Number> ValidationBuilder<T>.exclusiveMinimum(minimumExclusive: Number) = addConstraint(
    "doit être plus que '{0}'",
    minimumExclusive.toString()
) { it.toDouble() > minimumExclusive.toDouble() }

fun ValidationBuilder<String>.minLength(length: Int): Constraint<String> {
    require(length >= 0) { IllegalArgumentException("minLength nécessite que la longueur soit >= 0") }
    return addConstraint(
        "doit avoir au moins {0} caractères",
        length.toString()
    ) { it.length >= length }
}

fun ValidationBuilder<String>.maxLength(length: Int): Constraint<String> {
    require(length >= 0) { IllegalArgumentException("maxLength nécessite que la longueur soit >= 0") }
    return addConstraint(
        "doit avoir au plus {0} caractères",
        length.toString()
    ) { it.length <= length }
}

fun ValidationBuilder<String>.pattern(pattern: String) = pattern(pattern.toRegex())

fun ValidationBuilder<String>.pattern(pattern: Regex) = addConstraint(
    "doit correspondre au pattern attendu",
    pattern.toString()
) { it.matches(pattern) }

inline fun <reified T> ValidationBuilder<T>.minItems(minSize: Int): Constraint<T> = addConstraint(
    "doit avoir au moins {0} éléments",
    minSize.toString()
) {
    when (it) {
        is Iterable<*> -> it.count() >= minSize
        is Array<*> -> it.count() >= minSize
        is Map<*, *> -> it.count() >= minSize
        else -> throw IllegalStateException("minItems ne peut pas être appliqué au type ${T::class}")
    }
}

inline fun <reified T> ValidationBuilder<T>.maxItems(maxSize: Int): Constraint<T> = addConstraint(
    "doit avoir au plus {0} éléments",
    maxSize.toString()
) {
    when (it) {
        is Iterable<*> -> it.count() <= maxSize
        is Array<*> -> it.count() <= maxSize
        is Map<*, *> -> it.count() <= maxSize
        else -> throw IllegalStateException("maxItems ne peut pas être appliqué au type ${T::class}")
    }
}

inline fun <reified T : Map<*, *>> ValidationBuilder<T>.minProperties(minSize: Int): Constraint<T> =
    minItems(minSize) hint "doit avoir au moins {0} propriétés"

inline fun <reified T : Map<*, *>> ValidationBuilder<T>.maxProperties(maxSize: Int): Constraint<T> =
    maxItems(maxSize) hint "doit avoir au plus {0} propriétés"

inline fun <reified T> ValidationBuilder<T>.uniqueItems(unique: Boolean): Constraint<T> = addConstraint(
    "tous les éléments doivent être uniques"
) {
    !unique || when (it) {
        is Iterable<*> -> it.distinct().count() == it.count()
        is Array<*> -> it.distinct().count() == it.count()
        else -> throw IllegalStateException("uniqueItems ne peut pas être appliqué au type ${T::class}")
    }
}
