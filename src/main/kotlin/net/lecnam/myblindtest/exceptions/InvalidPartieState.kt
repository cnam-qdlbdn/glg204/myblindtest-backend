package net.lecnam.myblindtest.exceptions

class InvalidPartieState private constructor(override val message: String) : MyBlindTestException {
    companion object {
        fun forEtat(idPartie: Long): InvalidPartieState {
            return InvalidPartieState("Enchaînement d'état invalide pour la partie [$idPartie]")
        }

        fun forNumeroManche(idPartie: Long): InvalidPartieState {
            return InvalidPartieState("Impossible de passer à la manche suivante pour la partie [$idPartie]")
        }
    }

    override fun toString(): String {
        return "InvalidPartieState(message='$message')"
    }
}
