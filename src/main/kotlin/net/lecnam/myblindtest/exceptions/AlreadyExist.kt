package net.lecnam.myblindtest.exceptions

interface AlreadyExist : MyBlindTestException {
    data class Login(private val login: String) : MyBlindTestException {
        override val message = "Le login [$login] est déjà utilisé"
    }

    data class ChansonDeezer(
        val id: Long,
        private val titre: String
    ) : MyBlindTestException {
        override val message = "La chanson Deezer [$id] ($titre) existe déjà dans votre bibliothèque"
    }

    data class Playlist(private val playlist: String) : MyBlindTestException {
        override val message = "La playlist [$playlist] existe déjà"
    }

    data class Tag(private val tag: String) : MyBlindTestException {
        override val message = "Le tag [$tag] existe déjà"
    }
}
