package net.lecnam.myblindtest.exceptions

data class ErrorMessageContainer(
    val reasons: List<String>
)

fun List<String>.toErrorMessageContainer() = ErrorMessageContainer(this)

fun String.toErrorMessageContainer() = listOf(this).toErrorMessageContainer()
