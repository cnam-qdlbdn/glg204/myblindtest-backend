package net.lecnam.myblindtest.exceptions

sealed interface EntityNotFoundForJoueur : MyBlindTestException {
    data class Tag(private val id: Long) : EntityNotFoundForJoueur {
        override val message: String = "Le tag $id n'existe pas pour le joueur connecté"
    }

    data class Chanson(private val id: Long) : EntityNotFoundForJoueur {
        override val message: String = "La chanson $id n'existe pas pour le joueur connecté"
    }

    data class Playlist(private val id: Long) : EntityNotFoundForJoueur {
        override val message: String = "La playlist $id n'existe pas pour le joueur connecté"
    }

    data class DynamicPlaylist(private val idTag: Long, private val valeurTag: String?) : EntityNotFoundForJoueur {
        override val message: String =
            "La playlist dynamique n'existe pas pour le joueur connecté, le tag $idTag ${if (valeurTag != null) "et la valeur $valeurTag" else ""}".trim()
    }
}
