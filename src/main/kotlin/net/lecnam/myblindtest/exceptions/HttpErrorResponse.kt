package net.lecnam.myblindtest.exceptions

import io.ktor.http.HttpStatusCode
import io.ktor.http.HttpStatusCode.Companion.BadRequest
import io.ktor.http.HttpStatusCode.Companion.NotFound

data class HttpErrorResponse(
    val httpStatusCode: HttpStatusCode,
    val message: ErrorMessageContainer
)

fun List<String>.badRequest() = HttpErrorResponse(BadRequest, this.toErrorMessageContainer())
fun String.badRequest() = HttpErrorResponse(BadRequest, this.toErrorMessageContainer())
fun List<String>.notFound() = HttpErrorResponse(NotFound, this.toErrorMessageContainer())
fun String.notFound() = HttpErrorResponse(NotFound, this.toErrorMessageContainer())

fun ErrorMessageContainer.notFound() = HttpErrorResponse(NotFound, this)
