package net.lecnam.myblindtest.exceptions

data class NoDeezerPreview(override val message: String) : MyBlindTestException
