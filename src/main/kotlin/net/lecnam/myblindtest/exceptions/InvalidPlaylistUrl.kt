package net.lecnam.myblindtest.exceptions

data class InvalidPlaylistUrl(override val message: String) : MyBlindTestException
