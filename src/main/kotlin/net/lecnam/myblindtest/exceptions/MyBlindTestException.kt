package net.lecnam.myblindtest.exceptions

interface MyBlindTestException {
    val message: String
}
