package net.lecnam.myblindtest.exceptions

sealed interface EntityNotFound : MyBlindTestException {
    data class Chanson(val id: Long) : EntityNotFound {
        override val message: String = "La chanson $id n'existe pas"
    }

    data class Playlist(private val id: Long) : EntityNotFound {
        override val message: String = "La playlist $id n'existe pas"
    }

    data class Partie(override val message: String) : EntityNotFound {
        companion object {
            fun byCodePartie(codePartie: String) = Partie("La partie $codePartie n'existe pas, ou est terminée")

            fun byId(id: Long) = Partie("La partie avec l'id [$id] n'existe pas")
        }
    }
}
