package net.lecnam.myblindtest.exceptions

data class NoPlaylistForJoueur(private val idJoueur: Long) : MyBlindTestException {
    override val message = "Il n'existe aucune playlist pour le joueur [$idJoueur] avec laquelle créer une partie"
}
