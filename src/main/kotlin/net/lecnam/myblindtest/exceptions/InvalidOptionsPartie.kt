package net.lecnam.myblindtest.exceptions

data class InvalidOptionsPartie(private val codePartie: String) : MyBlindTestException {
    override val message = "Groupe d'options invalide pour la partie [$codePartie]"
}
