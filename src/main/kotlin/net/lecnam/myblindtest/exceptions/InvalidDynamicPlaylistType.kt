package net.lecnam.myblindtest.exceptions

data class InvalidDynamicPlaylistType(private val idTag: Long) : MyBlindTestException {
    override val message =
        "La playlist dynamique pour le joueur courant et le tag $idTag ne peut pas être récupérée avec ces paramètres"
}
