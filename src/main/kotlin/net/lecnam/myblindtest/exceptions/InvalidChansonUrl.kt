package net.lecnam.myblindtest.exceptions

data class InvalidChansonUrl(override val message: String) : MyBlindTestException
