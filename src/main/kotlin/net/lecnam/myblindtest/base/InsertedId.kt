package net.lecnam.myblindtest.base

data class InsertedId(val id: Long)

fun Long.toInsertedId() = InsertedId(this)
