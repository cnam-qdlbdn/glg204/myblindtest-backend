package net.lecnam.myblindtest.base

interface HasId {
    val id: Long
}
