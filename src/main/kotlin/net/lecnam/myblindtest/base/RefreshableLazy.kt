package net.lecnam.myblindtest.base

interface RefreshableLazy<T> {
    operator fun invoke(): T

    fun refresh(): T
}

private class RefreshableLazyImpl<T>(val initializer: () -> T) : RefreshableLazy<T> {
    var value = lazy(initializer)

    override operator fun invoke(): T {
        return value.value
    }

    override fun refresh(): T {
        value = lazy(initializer)
        return invoke()
    }
}

fun <T> refreshableLazy(initializer: () -> T): RefreshableLazy<T> = RefreshableLazyImpl(initializer)
