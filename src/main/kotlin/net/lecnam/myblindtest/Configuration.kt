package net.lecnam.myblindtest

import com.sksamuel.hoplite.ConfigLoaderBuilder
import com.sksamuel.hoplite.Secret
import java.time.Duration

data class Configuration(
    val connection: Connection,
    val ktor: KtorConf,
    val flyway: FlywayConf = FlywayConf(),
    val mode: Mode = Mode()
) {
    data class Connection(
        val db: DB,
        val redis: Redis
    ) {
        data class DB(
            val url: String,
            val username: String,
            val password: Secret,
            val maximumPoolSize: Int = 10
        )

        data class Redis(
            val host: String,
            val port: Int,
            val username: String?,
            val password: Secret?
        )
    }

    data class KtorConf(
        val session: SessionConf,
        val server: ServerConf
    ) {
        data class SessionConf(
            val sign: Secret,
            val encrypt: Secret,
            val maxDuration: Duration,
            val secure: Boolean
        )

        data class ServerConf(
            val port: Int
        )
    }

    data class FlywayConf(
        val runClean: Boolean = false
    )

    data class Mode(
        val dev: Boolean = false
    )
}

fun getConfiguration() = ConfigLoaderBuilder.default().build()
    .loadConfigOrThrow<Configuration>("/config.yml")
