/* ktlint-disable filename */
package net.lecnam.myblindtest.ext

import io.konform.validation.ValidationError

fun ValidationError.toMessage(vararg pathToProperty: Pair<String, String>) = if (message[0].isUpperCase()) message
else {
    val property = pathToProperty.toMap()[dataPath] ?: throw NotImplementedError(dataPath)
    "$property $message"
}
