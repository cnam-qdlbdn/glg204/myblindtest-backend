/* ktlint-disable filename */
package net.lecnam.myblindtest.ext

import io.ktor.http.HttpStatusCode
import io.ktor.http.URLParserException
import io.ktor.http.Url
import io.ktor.server.application.*
import io.ktor.server.auth.principal
import io.ktor.server.response.respond
import io.ktor.server.websocket.DefaultWebSocketServerSession
import io.ktor.util.pipeline.PipelineContext
import net.lecnam.myblindtest.exceptions.ErrorMessageContainer
import net.lecnam.myblindtest.exceptions.HttpErrorResponse
import net.lecnam.myblindtest.plugins.UserSession

fun PipelineContext<Unit, ApplicationCall>.getPrincipalAuth() =
    call.principal<UserSession>()!!.auth

fun DefaultWebSocketServerSession.getPrincipalAuth() =
    call.principal<UserSession>()!!.auth

suspend fun PipelineContext<Unit, ApplicationCall>.badRequest(errorMessageContainer: ErrorMessageContainer) =
    call.respond(HttpStatusCode.BadRequest, errorMessageContainer)

suspend fun PipelineContext<Unit, ApplicationCall>.respondError(httpErrorResponse: HttpErrorResponse) =
    call.respond(httpErrorResponse.httpStatusCode, httpErrorResponse.message)

fun String.isValidUrl() = try {
    Url(this)
    true
} catch (_: URLParserException) {
    false
}
