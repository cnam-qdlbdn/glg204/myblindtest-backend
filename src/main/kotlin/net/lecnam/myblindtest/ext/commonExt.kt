/* ktlint-disable filename */
package net.lecnam.myblindtest.ext

fun <T> T.returnIfEqual(toCompare: T, toReturn: () -> T) = if (this == toCompare) this else toReturn()
fun <T> T.returnIfEqual(toCompare: T, toReturn: T) = this.returnIfEqual(toCompare) { toReturn }
