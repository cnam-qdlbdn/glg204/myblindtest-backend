package net.lecnam.myblindtest.librairie.tag

import io.github.oshai.KotlinLogging
import io.konform.validation.ValidationErrors
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.*
import io.ktor.server.auth.authenticate
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.Routing
import io.ktor.server.routing.delete
import io.ktor.server.routing.get
import io.ktor.server.routing.patch
import io.ktor.server.routing.post
import io.ktor.server.routing.route
import net.lecnam.myblindtest.base.toInsertedId
import net.lecnam.myblindtest.exceptions.badRequest
import net.lecnam.myblindtest.exceptions.notFound
import net.lecnam.myblindtest.exceptions.toErrorMessageContainer
import net.lecnam.myblindtest.ext.badRequest
import net.lecnam.myblindtest.ext.getPrincipalAuth
import net.lecnam.myblindtest.ext.respondError
import net.lecnam.myblindtest.ext.toMessage
import net.lecnam.myblindtest.plugins.SESSION_KEY
import org.kodein.di.instance
import org.kodein.di.ktor.closestDI

private val logger = KotlinLogging.logger {}

fun Routing.tagRouting() {
    authenticate(SESSION_KEY) {
        route("/tag") {
            post {
                val dto = call.receive<CreateTagDTO>()
                val tagService by closestDI().instance<TagService>()
                val auth = getPrincipalAuth()

                tagService.insert(dto.toInsertForm(auth.joueur.id))
                    .mapLeft { v ->
                        v.use(
                            { tagErrorMapping(it) },
                            { listOf(it.message) }
                        ).toErrorMessageContainer()
                    }
                    .fold(
                        { badRequest(it) },
                        {
                            logger.info { "Tag [${dto.nom}] créé pour [${auth.joueur.login}]" }

                            call.respond(HttpStatusCode.Created, it.toInsertedId())
                        }
                    )
            }

            patch("/{id}") {
                val tagId = call.parameters["id"]!!.toLong()
                val dto = call.receive<UpdateTagDTO>()
                val auth = getPrincipalAuth()
                val tagService by closestDI().instance<TagService>()

                tagService.update(dto.toUpdateForm(tagId, auth.joueur.id))
                    .mapLeft { e ->
                        e.use(
                            { tagErrorMapping(it).badRequest() },
                            { it.message.notFound() },
                            { it.message.badRequest() }
                        )
                    }
                    .fold(
                        { respondError(it) },
                        { call.respond(it) }
                    )
            }

            delete("/{id}") {
                val tagId = call.parameters["id"]!!.toLong()
                val tagService by closestDI().instance<TagService>()
                val auth = getPrincipalAuth()

                tagService.delete(tagId, auth.joueur.id)
                    .fold(
                        { call.respond(HttpStatusCode.NotFound, it.message.toErrorMessageContainer()) },
                        {
                            logger.info { "Tag [$tagId] supprimé pour [${auth.joueur.login}]" }

                            call.respond(HttpStatusCode.NoContent)
                        }
                    )
            }

            get("/{id}") {
                val tagId = call.parameters["id"]!!.toLong()
                val tagService by closestDI().instance<TagService>()
                val auth = getPrincipalAuth()

                tagService.findById(tagId, auth.joueur.id)
                    .fold(
                        { call.respond(HttpStatusCode.NotFound, it.message.toErrorMessageContainer()) },
                        { call.respond(it) }
                    )
            }

            get {
                val tagService by closestDI().instance<TagService>()
                val auth = getPrincipalAuth()

                call.respond(tagService.findAll(auth.joueur.id))
            }
        }
    }
}

private fun tagErrorMapping(it: ValidationErrors) = it.map { e ->
    e.toMessage(
        ".nom" to "Le nom",
        ".description" to "La description"
    )
}
