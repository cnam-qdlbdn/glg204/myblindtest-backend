package net.lecnam.myblindtest.librairie.tag

import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys
import org.jdbi.v3.sqlobject.statement.SqlQuery
import org.jdbi.v3.sqlobject.statement.SqlUpdate
import org.jdbi.v3.sqlobject.transaction.Transaction

interface TagDAO {
    @SqlQuery("select * from tag where fk_joueur = :idJoueur")
    fun findAll(idJoueur: Long): List<Tag>

    @SqlQuery("select * from tag where id = :idEntity and fk_joueur = :idJoueur")
    fun findById(idEntity: Long, idJoueur: Long): Tag?

    @SqlQuery("select exists(select 1 from tag where nom = :nomTag and fk_joueur = :idJoueur)")
    fun existsByNom(idJoueur: Long, nomTag: String): Boolean

    @Transaction
    @SqlUpdate(
        """insert into tag (nom, description, type_regroupement, fk_joueur) values (
        :entityToInsert.nom, 
        :entityToInsert.description, 
        :entityToInsert.typeRegroupement, 
        :entityToInsert.refJoueur
        )"""
    )
    @GetGeneratedKeys("id")
    fun insert(entityToInsert: TagInsertForm): Long

    @Transaction
    @SqlUpdate("delete from tag where id = :idEntity")
    fun delete(idEntity: Long)

    @Transaction
    @GetGeneratedKeys
    @SqlUpdate(
        """update tag 
        set nom = :nom, description = :description, type_regroupement = :typeRegroupement
        where id = :id and fk_joueur = :refJoueur
        """
    )
    fun update(tag: TagUpdateForm): Tag
}
