package net.lecnam.myblindtest.librairie.tag

import arrow.core.Either
import arrow.core.left
import arrow.core.right
import com.athaydes.kunion.Union
import com.athaydes.kunion.Union.U2.Companion.toU2A
import com.athaydes.kunion.Union.U2.Companion.toU2B
import io.konform.validation.Invalid
import io.konform.validation.Valid
import io.konform.validation.Validation
import io.konform.validation.ValidationBuilder
import io.konform.validation.ValidationErrors
import io.konform.validation.ifPresent
import io.konform.validation.jsonschema.minLength
import io.konform.validation.jsonschema.pattern
import net.lecnam.myblindtest.exceptions.AlreadyExist
import net.lecnam.myblindtest.exceptions.EntityNotFoundForJoueur

class TagServiceImpl(private val tagDAO: TagDAO) : TagService {
    private val validateInsertTag = Validation {
        (TagInsertForm::nom)(validationNomTag())
        (TagInsertForm::description)(validationDescriptionTag())
    }

    private val validateUpdateTag = Validation {
        TagUpdateForm::nom.ifPresent(validationNomTag())
        (TagUpdateForm::description)(validationDescriptionTag())
    }

    companion object {
        private fun validationNomTag(): ValidationBuilder<String>.() -> Unit = {
            minLength(1)
            pattern("^(?:\\S+\\s)*\\S*$") hint "Le nom du tag ne doit ni commencer ni finir par des espaces, et " +
                "ne pas comporter de multiples espaces consécutifs"
        }

        private fun validationDescriptionTag(): ValidationBuilder<String?>.() -> Unit = {
            ifPresent {
                minLength(1)
            }
        }
    }

    override fun insert(tag: TagInsertForm): Either<Union.U2<ValidationErrors, AlreadyExist.Tag>, Long> {
        return when (val validationResult = validateInsertTag(tag)) {
            is Invalid -> validationResult.errors.toU2A().left()
            is Valid -> {
                if (tagDAO.existsByNom(tag.refJoueur, tag.nom)) {
                    AlreadyExist.Tag(tag.nom).toU2B().left()
                } else {
                    tagDAO.insert(tag).right()
                }
            }
        }
    }

    override fun update(tag: TagUpdateForm): Either<Union.U3<ValidationErrors, EntityNotFoundForJoueur.Tag, AlreadyExist.Tag>, TagDTO> {
        val validationResult = validateUpdateTag(tag)
        if (validationResult is Invalid) {
            return Union.U3.ofA(validationResult.errors).left()
        }

        val existingTag = tagDAO.findById(tag.id, tag.refJoueur)
            ?: return Union.U3.ofB(EntityNotFoundForJoueur.Tag(tag.id)).left()

        if (tag.nom != null && tagDAO.existsByNom(tag.refJoueur, tag.nom)) {
            return Union.U3.ofC(AlreadyExist.Tag(tag.nom)).left()
        }

        // On est presque obligé de faire comme ça.
        // L'autre possibilité, c'est d'implémenter manuellement toutes les méthodes
        // du DAO pour faire comme avec les playlists, JUSTE pour l'update
        val fullTagForm = tag.copy(
            nom = tag.nom ?: existingTag.nom,
            description = tag.description.let { if (it.isNullOrBlank()) null else it } ?: existingTag.description,
            typeRegroupement = tag.typeRegroupement ?: existingTag.typeRegroupement
        )

        return tagDAO.update(fullTagForm).toDTO().right()
    }

    override fun delete(id: Long, idJoueur: Long): Either<EntityNotFoundForJoueur.Tag, Unit> {
        return findById(id, idJoueur)
            .map { tagDAO.delete(id) }
    }

    override fun findById(id: Long, idJoueur: Long): Either<EntityNotFoundForJoueur.Tag, TagDTO> {
        return tagDAO.findById(id, idJoueur)?.toDTO()?.right() ?: EntityNotFoundForJoueur.Tag(id).left()
    }

    override fun findAll(idJoueur: Long) = tagDAO.findAll(idJoueur).map { it.toDTO() }
}
