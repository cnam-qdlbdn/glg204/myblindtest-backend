package net.lecnam.myblindtest.librairie.tag

import arrow.core.Either
import com.athaydes.kunion.Union
import io.konform.validation.ValidationErrors
import net.lecnam.myblindtest.exceptions.AlreadyExist
import net.lecnam.myblindtest.exceptions.EntityNotFoundForJoueur

interface TagService {
    fun insert(tag: TagInsertForm): Either<Union.U2<ValidationErrors, AlreadyExist.Tag>, Long>

    fun update(tag: TagUpdateForm): Either<Union.U3<ValidationErrors, EntityNotFoundForJoueur.Tag, AlreadyExist.Tag>, TagDTO>

    fun delete(id: Long, idJoueur: Long): Either<EntityNotFoundForJoueur.Tag, Unit>

    fun findAll(idJoueur: Long): List<TagDTO>

    fun findById(id: Long, idJoueur: Long): Either<EntityNotFoundForJoueur.Tag, TagDTO>
}
