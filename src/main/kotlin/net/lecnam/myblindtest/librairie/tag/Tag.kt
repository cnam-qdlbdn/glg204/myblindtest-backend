package net.lecnam.myblindtest.librairie.tag

import net.lecnam.myblindtest.base.HasId
import net.lecnam.myblindtest.base.HasRefJoueur
import org.jdbi.v3.core.mapper.reflect.ColumnName

interface TagData {
    val nom: String
    val description: String?
    val typeRegroupement: TypeRegroupementTag
}

data class Tag(
    override val id: Long,
    override val nom: String,
    override val description: String?,
    override val typeRegroupement: TypeRegroupementTag,

    @ColumnName("fk_joueur")
    override val refJoueur: Long
) : HasId, HasRefJoueur, TagData

enum class TypeRegroupementTag {
    DIRECT,
    VALEUR
}

data class TagInsertForm(
    override val nom: String,
    override val description: String? = null,
    override val typeRegroupement: TypeRegroupementTag,
    override val refJoueur: Long
) : HasRefJoueur, TagData

data class TagUpdateForm(
    override val id: Long,
    val nom: String?,
    val description: String?,
    val typeRegroupement: TypeRegroupementTag?,
    override val refJoueur: Long
) : HasId, HasRefJoueur

data class CreateTagDTO(
    override val nom: String,
    override val description: String?,
    override val typeRegroupement: TypeRegroupementTag
) : TagData

data class UpdateTagDTO(
    val nom: String?,
    val description: String?,
    val typeRegroupement: TypeRegroupementTag?
)

data class TagDTO(
    override val id: Long,
    override val nom: String,
    override val description: String?,
    override val typeRegroupement: TypeRegroupementTag
) : HasId, TagData

fun CreateTagDTO.toInsertForm(refJoueur: Long) = TagInsertForm(
    nom,
    description,
    typeRegroupement,
    refJoueur
)

fun UpdateTagDTO.toUpdateForm(id: Long, idJoueur: Long) = TagUpdateForm(
    id,
    nom,
    description,
    typeRegroupement,
    idJoueur
)

fun Tag.toDTO() = TagDTO(
    id,
    nom,
    description,
    typeRegroupement
)
