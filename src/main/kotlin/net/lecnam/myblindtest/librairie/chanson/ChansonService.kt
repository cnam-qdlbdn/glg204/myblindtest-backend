package net.lecnam.myblindtest.librairie.chanson

import arrow.core.Either
import com.athaydes.kunion.Union
import io.konform.validation.ValidationErrors
import net.lecnam.myblindtest.exceptions.AlreadyExist
import net.lecnam.myblindtest.exceptions.EntityNotFound
import net.lecnam.myblindtest.exceptions.EntityNotFoundForJoueur
import net.lecnam.myblindtest.exceptions.InvalidChansonUrl
import net.lecnam.myblindtest.exceptions.NoDeezerPreview

interface ChansonService {
    fun insert(
        deezer: ChansonUrl.Deezer,
        idJoueur: Long
    ): Either<Union.U3<InvalidChansonUrl, NoDeezerPreview, AlreadyExist.ChansonDeezer>, Long>

    fun insert(deezerInsertForm: ChansonDeezerInsertForm, idJoueur: Long): Either<AlreadyExist.ChansonDeezer, Long>

    fun delete(id: Long, idJoueur: Long): Either<EntityNotFoundForJoueur.Chanson, Unit>

    fun updateWithValeurTag(updateForm: ChansonUpdateForm): Either<Union.U3<ValidationErrors, EntityNotFoundForJoueur.Chanson, List<EntityNotFoundForJoueur.Tag>>, ChansonDTO>

    fun findAll(idJoueur: Long): List<ChansonDTO>

    fun findById(id: Long, idJoueur: Long): Either<EntityNotFoundForJoueur.Chanson, ChansonDTO>

    fun findById(id: Long): Either<EntityNotFound.Chanson, ChansonDTO>

    fun existsById(id: Long, idJoueur: Long): Boolean
}
