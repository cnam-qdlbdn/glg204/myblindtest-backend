package net.lecnam.myblindtest.librairie.chanson.valeurTag

data class ValeurTag(
    val tag: Long,
    val valeur: String
)
