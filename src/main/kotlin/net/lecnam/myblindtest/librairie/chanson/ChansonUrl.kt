package net.lecnam.myblindtest.librairie.chanson

sealed interface ChansonUrl {
    val url: String

    data class Deezer(override val url: String) : ChansonUrl
}
