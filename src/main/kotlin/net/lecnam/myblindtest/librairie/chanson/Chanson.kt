package net.lecnam.myblindtest.librairie.chanson

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonTypeName
import net.lecnam.myblindtest.api.deezer.Track
import net.lecnam.myblindtest.base.HasId
import net.lecnam.myblindtest.base.HasRefJoueur
import net.lecnam.myblindtest.librairie.chanson.valeurTag.ValeurTag

interface ChansonData {
    val titre: String
    val artiste: ArtisteData?
}

interface ChansonAdditionnalData {
    val valeursTags: List<ValeurTag>
    val playlists: List<Long>
}

interface ChansonDeezerData : ChansonData {
    val idDeezer: Long
    val extrait: String
    val image: String
}

sealed interface ChansonDB : ChansonData, ChansonAdditionnalData, HasId, HasRefJoueur {
    override val valeursTags: List<ValeurTag>
}

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonSubTypes(
    JsonSubTypes.Type(value = ChansonDeezerDTO::class, name = "deezer")
) // TODO [sources chanson] ajouter les autres sources de chanson au besoin
sealed interface ChansonDTO : ChansonData, ChansonAdditionnalData, HasId {
    override val artiste: ArtisteDTO?
    override val valeursTags: List<ValeurTag>
}

data class ChansonDeezerInsertForm(
    override val titre: String,
    override val artiste: ArtisteDTO,
    override val refJoueur: Long,
    override val idDeezer: Long,
    override val extrait: String,
    override val image: String
) : ChansonDeezerData, HasRefJoueur

data class ChansonFichierInsertForm(
    override val titre: String,
    override val artiste: ArtisteDTO?,
    override val refJoueur: Long

    /*
    Il faut stocker uniquement (à long terme) l'extrait sélectionné. Lors de l'import du fichier, dans un premier temps
    le fichier est stocké dans une table temporaire pour être découpé sur une portion spécifique par le joueur. À chaque
    fois qu'il veut changer sa sélection (30 secondes obligatoire), on envoie la commande au serveur, qui renvoie une
    url "statique" (mais temporaire) du contenu découpé via ffmpeg/ffprobe.
    Liens utiles :
    - https://stackoverflow.com/questions/10437750/how-to-get-the-real-actual-duration-of-an-mp3-file-vbr-or-cbr-server-side (probablement des pinailleurs qui veulent la durée "jouable", mais je m'en fiche je veux juste la durée attendue de la chanson)
    - https://stackoverflow.com/questions/6239350/how-to-extract-duration-time-from-ffmpeg-output/22243834#22243834
    - https://stackoverflow.com/questions/7945747/how-can-you-only-extract-30-seconds-of-audio-using-ffmpeg

    Ceci étant, je ne vais pas m'occuper des chansons au format fichier dans un premier temps, vu la complexité pour les
    intégrer. Ça n'enlève en rien la structure en héritage des chansons, mais je ferai marcher le reste avant.
     */
) : ChansonData, HasRefJoueur

data class ChansonDeezer(
    override val id: Long,
    override val titre: String,
    override val artiste: Artiste,
    override val valeursTags: List<ValeurTag>,
    override val refJoueur: Long,
    override val idDeezer: Long,
    override val extrait: String,
    override val image: String,
    override val playlists: List<Long>
) : ChansonDB, ChansonDeezerData

@JsonTypeName("deezer")
data class ChansonDeezerDTO(
    override val id: Long,
    override val titre: String,
    override val artiste: ArtisteDTO,
    override val valeursTags: List<ValeurTag>,
    override val idDeezer: Long,
    override val extrait: String,
    override val image: String,
    override val playlists: List<Long>
) : ChansonDTO, ChansonDeezerData

data class ChansonUpdateForm(
    override val id: Long,
    override val refJoueur: Long,
    val valeursTags: List<ValeurTag>?
) : HasId, HasRefJoueur

fun Track.toChansonDeezerInsertForm(idJoueur: Long) = ChansonDeezerInsertForm(
    title,
    ArtisteDTO(artist.name),
    idJoueur,
    id,
    preview,
    artist.picture_xl
)

fun ChansonDB.toDTO() = when (this) {
    is ChansonDeezer -> ChansonDeezerDTO(
        id,
        titre,
        artiste.toDTO(),
        valeursTags,
        idDeezer,
        extrait,
        image,
        playlists
    )
}
