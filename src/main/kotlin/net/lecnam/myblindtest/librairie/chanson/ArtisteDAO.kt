package net.lecnam.myblindtest.librairie.chanson

import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys
import org.jdbi.v3.sqlobject.statement.SqlQuery
import org.jdbi.v3.sqlobject.statement.SqlUpdate
import org.jdbi.v3.sqlobject.transaction.Transaction

interface ArtisteDAO {
    @Transaction
    @SqlUpdate("insert into artiste (nom) values (:entityToInsert.nom)")
    @GetGeneratedKeys("id")
    fun insertArtiste(entityToInsert: ArtisteDTO): Long

    @SqlQuery("select * from artiste where id = :id")
    fun getById(id: Long): Artiste

    @SqlQuery("select * from artiste where nom = :nom")
    fun findByNom(nom: String): Artiste?
}
