package net.lecnam.myblindtest.librairie.chanson

import arrow.core.flatMap
import com.athaydes.kunion.Union
import io.github.oshai.KotlinLogging
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.*
import io.ktor.server.auth.authenticate
import io.ktor.server.request.httpMethod
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.Routing
import io.ktor.server.routing.delete
import io.ktor.server.routing.get
import io.ktor.server.routing.post
import io.ktor.server.routing.put
import io.ktor.server.routing.route
import io.ktor.util.pipeline.PipelineContext
import net.lecnam.myblindtest.base.SingleValueWrapper
import net.lecnam.myblindtest.base.toInsertedId
import net.lecnam.myblindtest.exceptions.HttpErrorResponse
import net.lecnam.myblindtest.exceptions.badRequest
import net.lecnam.myblindtest.exceptions.notFound
import net.lecnam.myblindtest.exceptions.toErrorMessageContainer
import net.lecnam.myblindtest.ext.getPrincipalAuth
import net.lecnam.myblindtest.ext.respondError
import net.lecnam.myblindtest.ext.toMessage
import net.lecnam.myblindtest.librairie.chanson.valeurTag.ValeurTag
import net.lecnam.myblindtest.plugins.SESSION_KEY
import org.kodein.di.instance
import org.kodein.di.ktor.closestDI

private val logger = KotlinLogging.logger {}

fun Routing.chansonRouting() {
    authenticate(SESSION_KEY) {
        route("/chanson") {
            post("/deezer") {
                val dto = call.receive<ChansonUrl.Deezer>()
                val chansonService by closestDI().instance<ChansonService>()
                val auth = getPrincipalAuth()

                chansonService.insert(dto, auth.joueur.id)
                    .mapLeft { e ->
                        e.use(
                            { it.message.badRequest() },
                            { HttpErrorResponse(HttpStatusCode.NoContent, it.message.toErrorMessageContainer()) },
                            { it.message.badRequest() }
                        )
                    }
                    .fold(
                        { respondError(it) },
                        { call.respond(HttpStatusCode.Created, it.toInsertedId()) }
                    )
            }

            delete("/{id}") {
                val chansonId = call.parameters["id"]!!.toLong()
                val chansonService by closestDI().instance<ChansonService>()
                val auth = getPrincipalAuth()

                chansonService.delete(chansonId, auth.joueur.id)
                    .fold(
                        { call.respond(HttpStatusCode.NotFound, listOf(it.message).toErrorMessageContainer()) },
                        {
                            logger.info { "Chanson [$chansonId] supprimée pour [${auth.joueur.login}]" }

                            call.respond(HttpStatusCode.NoContent)
                        }
                    )
            }

            get("/{id}") {
                val chansonId = call.parameters["id"]!!.toLong()
                val chansonService by closestDI().instance<ChansonService>()
                val auth = getPrincipalAuth()

                chansonService.findById(chansonId, auth.joueur.id)
                    .fold(
                        { call.respond(HttpStatusCode.NotFound, listOf(it.message).toErrorMessageContainer()) },
                        { call.respond(it) }
                    )
            }

            get {
                val chansonService by closestDI().instance<ChansonService>()
                val auth = getPrincipalAuth()

                call.respond(chansonService.findAll(auth.joueur.id))
            }

            route("/{id}/tag/{tagId}") {
                put {
                    updateValeurTag { vts, vt -> vts.filterNot { it.tag == vt.tag } + vt }
                }

                delete {
                    updateValeurTag { vts, vt -> vts.filterNot { it.tag == vt.tag } }
                }
            }
        }
    }
}

private suspend fun PipelineContext<Unit, ApplicationCall>.updateValeurTag(operator: (List<ValeurTag>, ValeurTag) -> List<ValeurTag>) {
    val chansonId = call.parameters["id"]!!.toLong()
    val tagId = call.parameters["tagId"]!!.toLong()
    val dto = if (call.request.httpMethod == HttpMethod.Delete) null else call.receive<SingleValueWrapper>()
    val refJoueur = getPrincipalAuth().joueur.id
    val chansonService by closestDI().instance<ChansonService>()

    chansonService.findById(chansonId, refJoueur)
        .mapLeft { Union.U3.ofB(it) }
        .map {
            ChansonUpdateForm(
                it.id,
                refJoueur,
                operator(it.valeursTags, ValeurTag(tagId, dto?.value ?: ""))
            )
        }
        .flatMap { chansonService.updateWithValeurTag(it) }
        .mapLeft { u ->
            u.use(
                { v -> v.map { it.toMessage() }.badRequest() },
                { it.message.notFound() },
                { l -> l.map { it.message }.notFound() }
            )
        }
        .fold(
            { respondError(it) },
            { call.respond(it) }
        )
}
