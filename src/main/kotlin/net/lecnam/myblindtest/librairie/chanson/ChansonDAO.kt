package net.lecnam.myblindtest.librairie.chanson

import io.github.oshai.KotlinLogging
import net.lecnam.myblindtest.base.HasId
import net.lecnam.myblindtest.base.HasRefJoueur
import net.lecnam.myblindtest.librairie.chanson.valeurTag.ValeurTag
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.annotation.JdbiProperty
import org.jdbi.v3.core.kotlin.KotlinMapper
import org.jdbi.v3.core.kotlin.bindKotlin
import org.jdbi.v3.core.kotlin.inTransactionUnchecked
import org.jdbi.v3.core.kotlin.mapTo
import org.jdbi.v3.core.kotlin.useTransactionUnchecked
import org.jdbi.v3.core.kotlin.withHandleUnchecked
import org.jdbi.v3.core.mapper.reflect.ColumnName
import org.jdbi.v3.core.statement.Query
import kotlin.streams.asSequence

private val logger = KotlinLogging.logger {}

class ChansonDAO(
    private val jdbi: Jdbi,
    private val artisteDAO: ArtisteDAO
) {
    // TODO [sources chanson] trouver une pirouette pour le moment où il y aura effectivement plusieurs sources

    companion object {
        private const val FIND_ALL_NO_PLAYER = """
            select 
                c.id as c_id, 
                titre as c_titre, 
                c.fk_joueur as c_fk_joueur, 
                extrait as c_extrait, 
                image as c_image, 
                id_deezer as c_id_deezer, 
                a.id as a_id, 
                a.nom as a_nom,
                vt.fk_tag as vt_tag,
                vt.valeur as vt_valeur,
                cdp.fk_playlist as p_id
            from chanson c
            join chanson_deezer d on c.id = d.id_chanson
            join artiste a on a.id = c.fk_artiste
            left join valeur_tag vt on c.id = vt.fk_chanson
            left join chanson_dans_playlist cdp on c.id = cdp.fk_chanson
        """

        private const val FIND_ALL = """
            $FIND_ALL_NO_PLAYER            
            where c.fk_joueur = :idJoueur
        """
    }

    fun findAll(idJoueur: Long): List<ChansonDB> {
        return jdbi.withHandleUnchecked { h ->
            h.select(FIND_ALL)
                .bind("idJoueur", idJoueur)
                .mapToChansonDeezer()
                .toList()
        }
    }

    fun findById(idEntity: Long, idJoueur: Long): ChansonDB? {
        return jdbi.withHandleUnchecked { h ->
            h.select("$FIND_ALL and c.id = :idEntity")
                .bind("idJoueur", idJoueur)
                .bind("idEntity", idEntity)
                .mapToChansonDeezer()
                .firstOrNull()
        }
    }

    fun findById(idEntity: Long): ChansonDB? {
        return jdbi.withHandleUnchecked { h ->
            h.select("$FIND_ALL_NO_PLAYER where c.id = :idEntity")
                .bind("idEntity", idEntity)
                .mapToChansonDeezer()
                .firstOrNull()
        }
    }

    fun findByIdDeezer(idDeezer: Long, idJoueur: Long): ChansonDeezer? {
        return jdbi.withHandleUnchecked { h ->
            h.select("$FIND_ALL and d.id_deezer = :idDeezer")
                .bind("idDeezer", idDeezer)
                .bind("idJoueur", idJoueur)
                .mapToChansonDeezer()
                .firstOrNull()
        }
    }

    fun existsById(idEntity: Long, idJoueur: Long): Boolean {
        return jdbi.withHandleUnchecked { h ->
            h.select("select exists(select 1 from chanson where fk_joueur = :idJoueur and id = :idEntity)")
                .bind("idJoueur", idJoueur)
                .bind("idEntity", idEntity)
                .mapTo<Boolean>()
                .one()
        }
    }

    fun insertChanson(entity: ChansonDeezerInsertForm): Long {
        return jdbi.inTransactionUnchecked { h ->
            val artiste = artisteDAO.findByNom(entity.artiste.nom) ?: entity.artiste.let {
                val insertedId = artisteDAO.insertArtiste(entity.artiste)
                artisteDAO.getById(insertedId)
            }

            val idChanson =
                h.createUpdate("insert into chanson (titre, fk_joueur, fk_artiste) values (:titre, :refJoueur, :refArtiste)")
                    .bindKotlin(entity)
                    .bind("refArtiste", artiste.id)
                    .executeAndReturnGeneratedKeys("id")
                    .mapTo<Long>()
                    .one()

            h.createUpdate("insert into chanson_deezer (id_chanson, id_deezer, extrait, image) values (:idChanson, :idDeezer, :extrait, :image)")
                .bindKotlin(entity)
                .bind("idChanson", idChanson)
                .execute()

            idChanson
        }
    }

    // TODO [sources chanson] insertion des autres sources de chanson

    fun delete(idEntity: Long) {
        jdbi.useTransactionUnchecked { h ->
            h.createUpdate("delete from chanson where id = :idEntity")
                .bind("idEntity", idEntity)
                .execute()
        }
    }

    fun update(entity: ChansonUpdateForm): ChansonDB {
        jdbi.inTransactionUnchecked { h ->
            if (!existsById(entity.id, entity.refJoueur)) {
                throw IllegalArgumentException("Tentative de mise à jour d'une chanson n'appartenant pas au joueur")
            }

            val valeursTags = entity.valeursTags
            if (valeursTags != null) {
                val anyTagNotRelatedToJoueur = valeursTags.isNotEmpty() &&
                    h.select("select exists(select * from tag where id in (<ids>) and fk_joueur <> :idJoueur)")
                        .bindList("ids", valeursTags.map { it.tag })
                        .bind("idJoueur", entity.refJoueur)
                        .mapTo<Boolean>()
                        .one()

                if (anyTagNotRelatedToJoueur) {
                    throw IllegalArgumentException("Tentative de mise à jour des valeurs de tag avec un tag n'appartenant pas au joueur")
                }

                h.createUpdate("delete from valeur_tag where fk_chanson = :idChanson")
                    .bind("idChanson", entity.id)
                    .execute()

                if (valeursTags.isNotEmpty()) {
                    val valeursTagsBatch = h.prepareBatch(
                        """
                            insert into valeur_tag (fk_chanson, fk_tag, valeur) 
                            values (:idChanson, :idTag, :valeur)
                            """
                    )
                    valeursTags.forEach { (idTag, valeur) ->
                        valeursTagsBatch
                            .bind("idChanson", entity.id)
                            .bind("idTag", idTag)
                            .bind("valeur", valeur)
                            .add()
                    }
                    valeursTagsBatch.execute()
                }

                logger.debug { "[${entity.id}] Mise à jour de la liste des valeurs de tags --> [$valeursTags]" }
            }
        }

        return findById(entity.id, entity.refJoueur)!!
    }

    private data class ChansonDeezerWithoutArtistes(
        override val id: Long,
        @ColumnName("fk_joueur")
        override val refJoueur: Long,
        override val titre: String,
        override val idDeezer: Long,
        override val extrait: String,
        override val image: String
    ) : ChansonDeezerData, ChansonAdditionnalData, HasId, HasRefJoueur {
        @JdbiProperty(map = false)
        override var artiste: Artiste? = null

        @JdbiProperty(map = false)
        override val valeursTags = mutableListOf<ValeurTag>()

        @JdbiProperty(map = false)
        override val playlists = mutableListOf<Long>()

        fun toDB() = ChansonDeezer(
            id,
            titre,
            artiste!!,
            valeursTags,
            refJoueur,
            idDeezer,
            extrait,
            image,
            playlists
        )
    }

    private fun Query.mapToChansonDeezer() = this
        .registerRowMapper(KotlinMapper(ChansonDeezerWithoutArtistes::class, "c"))
        .registerRowMapper(KotlinMapper(ValeurTag::class, "vt"))
        .registerRowMapper(KotlinMapper(Artiste::class, "a"))
        .reduceRows { map: MutableMap<Long, ChansonDeezerWithoutArtistes>, rowView ->
            val chanson = map.computeIfAbsent(rowView.getColumn("c_id", Long::class.javaObjectType)) {
                rowView.getRow(ChansonDeezerWithoutArtistes::class.java)
            }

            if (chanson.artiste == null) {
                chanson.artiste = rowView.getRow(Artiste::class.java)
            }

            val refTag: Long? = rowView.getColumn("vt_tag", Long::class.javaObjectType)
            if (refTag != null) {
                val valeurTag = rowView.getRow(ValeurTag::class.java)
                if (valeurTag !in chanson.valeursTags) {
                    chanson.valeursTags += valeurTag
                }
            }

            val refPlaylist: Long? = rowView.getColumn("p_id", Long::class.javaObjectType)
            if (refPlaylist != null && refPlaylist !in chanson.playlists) {
                chanson.playlists += refPlaylist
            }
        }
        .map { it.toDB() }
        .asSequence()
}
