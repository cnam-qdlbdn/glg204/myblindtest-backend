package net.lecnam.myblindtest.librairie.chanson

import net.lecnam.myblindtest.base.HasId

interface ArtisteData {
    val nom: String
}

data class Artiste(
    override val id: Long,
    override val nom: String
) : HasId, ArtisteData

data class ArtisteDTO(override val nom: String) : ArtisteData

fun Artiste.toDTO() = ArtisteDTO(nom)
