package net.lecnam.myblindtest.librairie.chanson

import arrow.core.Either
import arrow.core.flatMap
import arrow.core.left
import arrow.core.right
import com.athaydes.kunion.Union
import io.github.oshai.KotlinLogging
import io.konform.validation.Invalid
import io.konform.validation.Validation
import io.konform.validation.ValidationErrors
import io.konform.validation.jsonschema.minLength
import io.konform.validation.jsonschema.pattern
import io.konform.validation.onEach
import kotlinx.coroutines.runBlocking
import net.lecnam.myblindtest.api.deezer.DeezerService
import net.lecnam.myblindtest.exceptions.AlreadyExist
import net.lecnam.myblindtest.exceptions.EntityNotFound
import net.lecnam.myblindtest.exceptions.EntityNotFoundForJoueur
import net.lecnam.myblindtest.exceptions.InvalidChansonUrl
import net.lecnam.myblindtest.exceptions.NoDeezerPreview
import net.lecnam.myblindtest.librairie.chanson.valeurTag.ValeurTag
import net.lecnam.myblindtest.librairie.tag.TagService

private val logger = KotlinLogging.logger {}

class ChansonServiceImpl(
    private val chansonDAO: ChansonDAO,
    private val tagService: TagService,
    private val deezerService: DeezerService
) : ChansonService {

    private val validateUpdateForm = Validation {
        ChansonUpdateForm::valeursTags ifPresent {
            onEach {
                ValeurTag::valeur {
                    minLength(1)
                    pattern("^(\\S|((?!.*\\s{2})\\S.*\\S)?)$".toRegex())
                }
            }
        }
    }

    override fun insert(
        deezer: ChansonUrl.Deezer,
        idJoueur: Long
    ): Either<Union.U3<InvalidChansonUrl, NoDeezerPreview, AlreadyExist.ChansonDeezer>, Long> {
        return runBlocking {
            deezerService.getDeezerTrackFromUrl(deezer.url)
                .mapLeft { u ->
                    u.use(
                        { Union.U3.ofA(it) },
                        { Union.U3.ofB(it) }
                    )
                }
                .map { it.toChansonDeezerInsertForm(idJoueur) }
                .onLeft {
                    logger.debug { "[$idJoueur] erreur import chanson : $it" }
                }
                .flatMap { b ->
                    insert(b, idJoueur).mapLeft { Union.U3.ofC(it) }
                }
        }
    }

    override fun insert(
        deezerInsertForm: ChansonDeezerInsertForm,
        idJoueur: Long
    ): Either<AlreadyExist.ChansonDeezer, Long> {
        return with(deezerInsertForm) {
            val chanson = chansonDAO.findByIdDeezer(idDeezer, idJoueur)

            if (chanson == null) deezerInsertForm.right()
            else AlreadyExist.ChansonDeezer(chanson.id, titre).left()
        }
            .map {
                logger.debug { "[$idJoueur] Insertion de la chanson $it" }
                chansonDAO.insertChanson(it)
            }
    }

    override fun delete(id: Long, idJoueur: Long) =
        findById(id, idJoueur)
            .map { chansonDAO.delete(id) }

    override fun updateWithValeurTag(updateForm: ChansonUpdateForm): Either<Union.U3<ValidationErrors, EntityNotFoundForJoueur.Chanson, List<EntityNotFoundForJoueur.Tag>>, ChansonDTO> {
        val validationResult = validateUpdateForm(updateForm)
        if (validationResult is Invalid) {
            return Union.U3.ofA(validationResult.errors).left()
        }

        val chanson = findById(updateForm.id, updateForm.refJoueur)
        if (chanson is Either.Left) {
            return Union.U3.ofB(chanson.value).left()
        }

        val valeursTags = updateForm.valeursTags
        if (valeursTags != null) {
            val founds = valeursTags
                .map { e -> tagService.findById(e.tag, updateForm.refJoueur) }

            if (founds.any { it is Either.Left }) {
                return Union.U3.ofC(
                    founds
                        .filterIsInstance<Either.Left<EntityNotFoundForJoueur.Tag>>()
                        .map { it.value }
                )
                    .left()
            }
        }

        return chansonDAO.update(updateForm).toDTO().right()
    }

    override fun findAll(idJoueur: Long) = chansonDAO.findAll(idJoueur).map { it.toDTO() }

    override fun findById(id: Long, idJoueur: Long) =
        chansonDAO.findById(id, idJoueur)
            ?.toDTO()
            ?.right()
            ?: EntityNotFoundForJoueur.Chanson(id).left()

    override fun findById(id: Long) = chansonDAO.findById(id)
        ?.toDTO()
        ?.right()
        ?: EntityNotFound.Chanson(id).left()

    override fun existsById(id: Long, idJoueur: Long) = chansonDAO.existsById(id, idJoueur)
}
