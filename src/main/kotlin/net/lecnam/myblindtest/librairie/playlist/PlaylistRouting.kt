package net.lecnam.myblindtest.librairie.playlist

import arrow.core.Either
import arrow.core.flatMap
import io.github.oshai.KotlinLogging
import io.konform.validation.ValidationError
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.*
import io.ktor.server.auth.authenticate
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.Routing
import io.ktor.server.routing.delete
import io.ktor.server.routing.get
import io.ktor.server.routing.patch
import io.ktor.server.routing.post
import io.ktor.server.routing.put
import io.ktor.server.routing.route
import io.ktor.util.pipeline.PipelineContext
import net.lecnam.myblindtest.base.toInsertedId
import net.lecnam.myblindtest.exceptions.HttpErrorResponse
import net.lecnam.myblindtest.exceptions.badRequest
import net.lecnam.myblindtest.exceptions.notFound
import net.lecnam.myblindtest.exceptions.toErrorMessageContainer
import net.lecnam.myblindtest.ext.badRequest
import net.lecnam.myblindtest.ext.getPrincipalAuth
import net.lecnam.myblindtest.ext.respondError
import net.lecnam.myblindtest.ext.toMessage
import net.lecnam.myblindtest.librairie.playlist.dynamic.DynamicPlaylistService
import net.lecnam.myblindtest.plugins.SESSION_KEY
import org.kodein.di.instance
import org.kodein.di.ktor.closestDI

private val logger = KotlinLogging.logger {}

fun Routing.playlistRouting() {
    authenticate(SESSION_KEY) {
        route("/playlist") {
            post {
                val dto = call.receive<CreatePlaylistDTO>()
                val playlistService: PlaylistService by closestDI().instance()
                val auth = getPrincipalAuth()

                playlistService.insert(dto.toInsertForm(auth.joueur.id))
                    .mapLeft { e ->
                        e.use(
                            { it.map { v -> playlistErrorMapping(v) } },
                            { listOf(it.message) }
                        ).toErrorMessageContainer()
                    }
                    .fold(
                        { badRequest(it) },
                        {
                            logger.info { "Playlist [${dto.nom}] créée pour [${auth.joueur.login}]" }

                            call.respond(HttpStatusCode.Created, it.toInsertedId())
                        }
                    )
            }

            post("/deezer") {
                val dto = call.receive<PlaylistUrl.Deezer>()
                val playlistService: PlaylistService by closestDI().instance()
                val auth = getPrincipalAuth()

                // TODO améliorer l'import d'une playlist pour mieux gérer les erreurs de retour de Deezer
                playlistService.insert(dto.url, auth.joueur.id)
                    .mapLeft { e ->
                        e.use(
                            { it.message },
                            { it.message }
                        ).toErrorMessageContainer()
                    }
                    .fold(
                        { badRequest(it) },
                        { call.respond(HttpStatusCode.Created, it.toInsertedId()) }
                    )
            }

            delete("/{id}") {
                val playlistId = call.parameters["id"]!!.toLong()
                val playlistService: PlaylistService by closestDI().instance()
                val auth = getPrincipalAuth()

                playlistService.delete(playlistId, auth.joueur.id)
                    .fold(
                        { call.respond(HttpStatusCode.NotFound, it.message.toErrorMessageContainer()) },
                        {
                            logger.info { "Playlist [$playlistId] supprimée pour [${auth.joueur.login}]" }

                            call.respond(HttpStatusCode.NoContent)
                        }
                    )
            }

            get("/{id}") {
                val playlistId = call.parameters["id"]!!.toLong()
                val playlistService: PlaylistService by closestDI().instance()
                val auth = getPrincipalAuth()

                playlistService.findById(playlistId, auth.joueur.id)
                    .fold(
                        { call.respond(HttpStatusCode.NotFound, it.message.toErrorMessageContainer()) },
                        { call.respond(it) }
                    )
            }

            get {
                val playlistService by closestDI().instance<PlaylistService>()
                val auth = getPrincipalAuth()

                call.respond(playlistService.findAll(auth.joueur.id))
            }

            patch("/{id}") {
                val playlistId = call.parameters["id"]!!.toLong()
                val dto = call.receive<UpdatePlaylistDTO>()

                updatePlaylist(dto, playlistId)
                    .fold(
                        { respondError(it) },
                        { call.respond(it) }
                    )
            }

            put("/{id}/chanson/{chansonId}") {
                updateChansonIdOnly { a, b -> a + b }
            }

            delete("/{id}/chanson/{chansonId}") {
                updateChansonIdOnly { a, b -> a - b }
            }

            route("/dynamic") {
                get {
                    val dpService: DynamicPlaylistService by closestDI().instance()
                    val auth = getPrincipalAuth()

                    call.respond(dpService.findAll(auth.joueur.id))
                }

                get("/{tagId}") {
                    val tagId = call.parameters["tagId"]!!.toLong()
                    val valeurTag = call.request.queryParameters["valeurTag"]
                    val dpService: DynamicPlaylistService by closestDI().instance()
                    val auth = getPrincipalAuth()

                    dpService.findBy(tagId, auth.joueur.id, valeurTag)
                        .fold(
                            { u ->
                                respondError(
                                    u.use(
                                        { it.message.badRequest() },
                                        { it.message.notFound() }
                                    )
                                )
                            },
                            { call.respond(it) }
                        )
                }
            }
        }
    }
}

private fun PipelineContext<Unit, ApplicationCall>.updatePlaylist(
    dto: UpdatePlaylistDTO,
    playlistId: Long
): Either<HttpErrorResponse, PlaylistDTO> {
    val playlistService: PlaylistService by closestDI().instance()
    val auth = getPrincipalAuth()

    return playlistService.update(dto.toUpdateForm(playlistId, auth.joueur.id))
        .mapLeft { u ->
            u.use(
                { v -> v.map { playlistErrorMapping(it) }.badRequest() },
                { it.message.badRequest() },
                { it.message.notFound() },
                { l -> l.map { it.message }.notFound() }
            )
        }
}

private suspend fun PipelineContext<Unit, ApplicationCall>.updateChansonIdOnly(operator: (List<Long>, Long) -> List<Long>) {
    val playlistId = call.parameters["id"]!!.toLong()
    val chansonId = call.parameters["chansonId"]!!.toLong()
    val playlistService: PlaylistService by closestDI().instance()
    val auth = getPrincipalAuth()

    playlistService.findById(playlistId, auth.joueur.id)
        .mapLeft { it.message.toErrorMessageContainer().notFound() }
        .map { UpdatePlaylistDTO(chansons = operator(it.chansons, chansonId)) }
        .flatMap { updatePlaylist(it, playlistId) }
        .fold(
            { respondError(it) },
            { call.respond(it) }
        )
}

private fun playlistErrorMapping(v: ValidationError) = v.toMessage(".nom" to "Le nom de la playlist")
