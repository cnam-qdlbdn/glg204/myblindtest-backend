package net.lecnam.myblindtest.librairie.playlist.dynamic

import arrow.core.Either
import com.athaydes.kunion.Union
import net.lecnam.myblindtest.exceptions.EntityNotFoundForJoueur
import net.lecnam.myblindtest.exceptions.InvalidDynamicPlaylistType

interface DynamicPlaylistService {
    fun findAll(idJoueur: Long): List<DynamicPlaylistDTO>

    fun findBy(
        idTag: Long,
        idJoueur: Long,
        valeurTag: String?
    ): Either<Union.U2<InvalidDynamicPlaylistType, EntityNotFoundForJoueur.DynamicPlaylist>, DynamicPlaylistDTO>
}
