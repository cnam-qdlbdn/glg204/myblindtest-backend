package net.lecnam.myblindtest.librairie.playlist

sealed interface PlaylistUrl {
    val url: String

    data class Deezer(override val url: String) : PlaylistUrl
}
