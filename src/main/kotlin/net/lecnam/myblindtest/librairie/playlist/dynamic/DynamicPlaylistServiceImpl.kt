package net.lecnam.myblindtest.librairie.playlist.dynamic

import arrow.core.Either
import arrow.core.flatMap
import arrow.core.left
import arrow.core.right
import com.athaydes.kunion.Union
import com.athaydes.kunion.Union.U2.Companion.toU2A
import com.athaydes.kunion.Union.U2.Companion.toU2B
import net.lecnam.myblindtest.exceptions.EntityNotFoundForJoueur
import net.lecnam.myblindtest.exceptions.InvalidDynamicPlaylistType
import net.lecnam.myblindtest.librairie.tag.TagService
import net.lecnam.myblindtest.librairie.tag.TypeRegroupementTag

class DynamicPlaylistServiceImpl(
    private val dynamicPlaylistDAO: DynamicPlaylistDAO,
    private val tagService: TagService
) : DynamicPlaylistService {

    override fun findAll(idJoueur: Long) = dynamicPlaylistDAO.findAll(idJoueur).map { it.toDTO() }

    override fun findBy(
        idTag: Long,
        idJoueur: Long,
        valeurTag: String?
    ): Either<Union.U2<InvalidDynamicPlaylistType, EntityNotFoundForJoueur.DynamicPlaylist>, DynamicPlaylistDTO> {
        return tagService.findById(idTag, idJoueur)
            .mapLeft { EntityNotFoundForJoueur.DynamicPlaylist(idTag, valeurTag).toU2B() }
            .flatMap {
                if ((it.typeRegroupement == TypeRegroupementTag.DIRECT && valeurTag != null) || (it.typeRegroupement == TypeRegroupementTag.VALEUR && valeurTag == null)) {
                    InvalidDynamicPlaylistType(idTag).toU2A().left()
                } else {
                    dynamicPlaylistDAO.findBy(idTag, idJoueur, valeurTag)?.toDTO()?.right()
                        ?: EntityNotFoundForJoueur.DynamicPlaylist(idTag, valeurTag).toU2B().left()
                }
            }
    }
}
