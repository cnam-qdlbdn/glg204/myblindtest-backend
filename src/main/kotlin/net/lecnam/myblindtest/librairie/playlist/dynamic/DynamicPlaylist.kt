package net.lecnam.myblindtest.librairie.playlist.dynamic

import net.lecnam.myblindtest.base.HasRefJoueur
import org.jdbi.v3.core.mapper.reflect.ColumnName

interface DynamicPlaylistData {
    val idTag: Long
    val valeurTag: String?
    val chansons: List<Long>
}

data class DynamicPlaylistUniqueIdentifier(
    val idTag: Long,
    @ColumnName("id_joueur")
    val refJoueur: Long,
    val valeurTag: String?
)

data class DynamicPlaylist(
    override val idTag: Long,
    @ColumnName("id_joueur")
    override val refJoueur: Long,
    override val valeurTag: String?,
    override val chansons: List<Long>
) : HasRefJoueur, DynamicPlaylistData

data class DynamicPlaylistDTO(
    override val idTag: Long,
    override val valeurTag: String?,
    override val chansons: List<Long>
) : DynamicPlaylistData

fun DynamicPlaylist.toDTO() = DynamicPlaylistDTO(
    idTag,
    valeurTag,
    chansons
)
