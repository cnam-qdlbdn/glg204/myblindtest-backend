package net.lecnam.myblindtest.librairie.playlist

import io.github.oshai.KotlinLogging
import net.lecnam.myblindtest.base.HasId
import net.lecnam.myblindtest.base.HasRefJoueur
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.annotation.JdbiProperty
import org.jdbi.v3.core.kotlin.bindKotlin
import org.jdbi.v3.core.kotlin.inTransactionUnchecked
import org.jdbi.v3.core.kotlin.mapTo
import org.jdbi.v3.core.kotlin.useTransactionUnchecked
import org.jdbi.v3.core.kotlin.withHandleUnchecked
import org.jdbi.v3.core.mapper.reflect.ColumnName
import org.jdbi.v3.core.statement.Query
import kotlin.streams.asSequence

private val logger = KotlinLogging.logger {}

class PlaylistDAO(private val jdbi: Jdbi) {
    companion object {
        private const val FIND_ALL_NO_PLAYER = """
            select p.*, cp.fk_chanson as id_chanson 
            from playlist p 
            left join chanson_dans_playlist cp on p.id = cp.fk_playlist
        """

        private const val FIND_ALL = """
            $FIND_ALL_NO_PLAYER 
            where fk_joueur = :idJoueur
        """
    }

    fun findAll(idJoueur: Long): List<Playlist> {
        return jdbi.withHandleUnchecked { h ->
            h.select(FIND_ALL)
                .bind("idJoueur", idJoueur)
                .mapToPlaylist()
                .toList()
        }
    }

    fun findById(idEntity: Long, idJoueur: Long): Playlist? {
        return jdbi.withHandleUnchecked { h ->
            h.select("$FIND_ALL and id = :idEntity")
                .bind("idJoueur", idJoueur)
                .bind("idEntity", idEntity)
                .mapToPlaylist()
                .firstOrNull()
        }
    }

    fun findById(idEntity: Long): Playlist? {
        return jdbi.withHandleUnchecked { h ->
            h.select("$FIND_ALL_NO_PLAYER where id = :idEntity")
                .bind("idEntity", idEntity)
                .mapToPlaylist()
                .firstOrNull()
        }
    }

    fun existsById(idEntity: Long, idJoueur: Long): Boolean {
        return jdbi.withHandleUnchecked { h ->
            h.select("select exists(select 1 from playlist where id = :idEntity and fk_joueur = :idJoueur)")
                .bind("idJoueur", idJoueur)
                .bind("idEntity", idEntity)
                .mapTo<Boolean>()
                .one()
        }
    }

    fun existsByNom(idJoueur: Long, nom: String): Boolean {
        return jdbi.withHandleUnchecked { h ->
            h.select("select exists(select 1 from playlist where fk_joueur = :idJoueur and nom = :nom)")
                .bind("idJoueur", idJoueur)
                .bind("nom", nom)
                .mapTo<Boolean>()
                .one()
        }
    }

    fun insert(entity: PlaylistInsertForm): Long {
        return jdbi.inTransactionUnchecked { h ->
            h.createUpdate("insert into playlist (nom, fk_joueur) values (:nom, :refJoueur)")
                .bindKotlin(entity)
                .executeAndReturnGeneratedKeys("id")
                .mapTo<Long>()
                .one()
        }
    }

    fun update(entity: PlaylistUpdateForm): Playlist {
        jdbi.inTransactionUnchecked { h ->
            if (entity.nom != null) {
                val updateNom =
                    h.createUpdate("update playlist set nom = :nom where id = :id and fk_joueur = :refJoueur")
                        .bindKotlin(entity)
                        .execute()
                if (updateNom == 0) {
                    throw IllegalArgumentException("Tentative de mise à jour d'une playlist n'appartenant pas au joueur")
                }
                logger.debug { "[${entity.id}] Mise à jour du nom --> [${entity.nom}]" }
            }

            /*
            La liste d'id de chansons est à utiliser en mode erase/replace, donc c'est l'implémentation qui sera
            appliquée.
             */
            val chansons = entity.chansons
            if (chansons != null) {
                val anyChansonNotRelatedToJoueur =
                    h.select("select exists(select 1 from chanson where id in (<ids>) and fk_joueur <> :idJoueur)")
                        .bindList("ids", chansons)
                        .bind("idJoueur", entity.refJoueur)
                        .mapTo<Boolean>()
                        .one()

                if (anyChansonNotRelatedToJoueur) {
                    throw IllegalArgumentException("Tentative de mise à jour d'une playlist avec une chanson n'appartenant pas au joueur")
                }

                h.createUpdate("delete from chanson_dans_playlist where fk_chanson in (<chansons>) and fk_playlist = :idPlaylist")
                    .bindList("chansons", chansons)
                    .bind("idPlaylist", entity.id)
                    .execute()

                val linkPlaylistChansonBatch =
                    h.prepareBatch("insert into chanson_dans_playlist (fk_playlist, fk_chanson) values (:idPlaylist, :idChanson)")
                chansons.forEach {
                    linkPlaylistChansonBatch
                        .bind("idPlaylist", entity.id)
                        .bind("idChanson", it)
                        .add()
                }
                linkPlaylistChansonBatch.execute()

                logger.debug { "[${entity.id}] Mise à jour de la liste des chansons --> [$chansons]" }
            }
        }

        return findById(entity.id, entity.refJoueur)!!
    }

    fun delete(idEntity: Long) {
        jdbi.useTransactionUnchecked { h ->
            h.createUpdate("delete from playlist where id = :idEntity")
                .bind("idEntity", idEntity)
                .execute()
        }
    }

    private data class PlaylistWithoutChansons(
        override val id: Long,
        @ColumnName("fk_joueur")
        override val refJoueur: Long,
        override val nom: String
    ) : HasId, HasRefJoueur, PlaylistData, PlaylistAdditionnalData {
        @JdbiProperty(map = false)
        override val chansons = mutableListOf<Long>()

        fun toDB() = Playlist(id, refJoueur, nom, chansons)
    }

    private fun Query.mapToPlaylist() =
        reduceRows { map: MutableMap<Long, PlaylistWithoutChansons>, rowView ->
            val playlist = map.computeIfAbsent(rowView.getColumn("id", Long::class.javaObjectType)) {
                rowView.getRow(PlaylistWithoutChansons::class.java)
            }

            val idChanson: Long? = rowView.getColumn("id_chanson", Long::class.javaObjectType)
            if (idChanson != null && idChanson !in playlist.chansons) {
                playlist.chansons += idChanson
            }
        }
            .asSequence()
            .map { it.toDB() }
}
