package net.lecnam.myblindtest.librairie.playlist

import arrow.core.Either
import com.athaydes.kunion.Union
import io.konform.validation.ValidationErrors
import net.lecnam.myblindtest.exceptions.AlreadyExist
import net.lecnam.myblindtest.exceptions.EntityNotFound
import net.lecnam.myblindtest.exceptions.EntityNotFoundForJoueur
import net.lecnam.myblindtest.exceptions.InvalidPlaylistUrl

interface PlaylistService {
    fun insert(url: String, idJoueur: Long): Either<Union.U2<InvalidPlaylistUrl, AlreadyExist.Playlist>, Long>

    fun insert(playlist: PlaylistInsertForm): Either<Union.U2<ValidationErrors, AlreadyExist.Playlist>, Long>

    fun update(playlist: PlaylistUpdateForm): Either<Union.U4<ValidationErrors, AlreadyExist.Playlist, EntityNotFoundForJoueur.Playlist, List<EntityNotFoundForJoueur.Chanson>>, PlaylistDTO>

    fun delete(id: Long, idJoueur: Long): Either<EntityNotFoundForJoueur.Playlist, Unit>

    fun findAll(idJoueur: Long): List<PlaylistDTO>

    fun findById(id: Long, idJoueur: Long): Either<EntityNotFoundForJoueur.Playlist, PlaylistDTO>

    fun findById(id: Long): Either<EntityNotFound.Playlist, PlaylistDTO>
}
