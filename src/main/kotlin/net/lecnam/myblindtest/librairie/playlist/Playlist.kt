package net.lecnam.myblindtest.librairie.playlist

import net.lecnam.myblindtest.api.deezer.DeezerPlaylist
import net.lecnam.myblindtest.base.HasId
import net.lecnam.myblindtest.base.HasRefJoueur

const val PLAYLIST_NAME_MAX_LENGTH = 50

interface PlaylistData {
    val nom: String
}

interface PlaylistAdditionnalData {
    val chansons: List<Long>
}

data class Playlist(
    override val id: Long,
    override val refJoueur: Long,
    override val nom: String,
    override val chansons: List<Long>
) : HasId, HasRefJoueur, PlaylistData, PlaylistAdditionnalData

data class CreatePlaylistDTO(override val nom: String) : PlaylistData

data class UpdatePlaylistDTO(
    val nom: String? = null,
    val chansons: List<Long>? = null
)

data class PlaylistInsertForm(
    override val refJoueur: Long,
    override val nom: String
) : HasRefJoueur, PlaylistData

data class PlaylistUpdateForm(
    override val id: Long,
    override val refJoueur: Long,
    val nom: String? = null,
    val chansons: List<Long>? = null
) : HasId, HasRefJoueur

data class PlaylistDTO(
    override val id: Long,
    override val nom: String,
    override val chansons: List<Long>
) : HasId, PlaylistData, PlaylistAdditionnalData

fun CreatePlaylistDTO.toInsertForm(idJoueur: Long) = PlaylistInsertForm(idJoueur, nom)

fun UpdatePlaylistDTO.toUpdateForm(id: Long, idJoueur: Long) = PlaylistUpdateForm(
    id,
    idJoueur,
    nom,
    chansons
)

fun Playlist.toDTO() = PlaylistDTO(
    id,
    nom,
    chansons
)

fun DeezerPlaylist.toPlaylistInsertForm(idJoueur: Long) = PlaylistInsertForm(
    idJoueur,
    title.take(PLAYLIST_NAME_MAX_LENGTH)
)
