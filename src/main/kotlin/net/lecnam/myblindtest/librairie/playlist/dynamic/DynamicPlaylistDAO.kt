package net.lecnam.myblindtest.librairie.playlist.dynamic

import org.jdbi.v3.sqlobject.statement.SqlQuery

interface DynamicPlaylistDAO {
    @SqlQuery("select * from dynamic_playlist where id_joueur = :idJoueur")
    fun findAll(idJoueur: Long): List<DynamicPlaylist>

    @SqlQuery(
        """select * 
        from dynamic_playlist 
        where id_tag = :idTag 
        and id_joueur = :idJoueur
        and case when :valeurTag is null then valeur_tag is null else valeur_tag = :valeurTag end"""
    )
    fun findBy(idTag: Long, idJoueur: Long, valeurTag: String?): DynamicPlaylist?
}
