package net.lecnam.myblindtest.librairie.playlist

import arrow.core.Either
import arrow.core.flatMap
import arrow.core.left
import arrow.core.right
import com.athaydes.kunion.Union
import com.athaydes.kunion.Union.U2.Companion.toU2A
import com.athaydes.kunion.Union.U2.Companion.toU2B
import io.konform.validation.Invalid
import io.konform.validation.Valid
import io.konform.validation.Validation
import io.konform.validation.ValidationBuilder
import io.konform.validation.ValidationErrors
import io.konform.validation.jsonschema.maxLength
import io.konform.validation.jsonschema.minLength
import io.konform.validation.jsonschema.pattern
import kotlinx.coroutines.runBlocking
import net.lecnam.myblindtest.api.deezer.DeezerService
import net.lecnam.myblindtest.exceptions.AlreadyExist
import net.lecnam.myblindtest.exceptions.EntityNotFound
import net.lecnam.myblindtest.exceptions.EntityNotFoundForJoueur
import net.lecnam.myblindtest.exceptions.InvalidPlaylistUrl
import net.lecnam.myblindtest.librairie.chanson.ChansonService
import net.lecnam.myblindtest.librairie.chanson.ChansonUrl
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.kotlin.inTransactionUnchecked

class PlaylistServiceImpl(
    private val playlistDAO: PlaylistDAO,
    private val chansonService: ChansonService,
    private val jdbi: Jdbi, // pour la gestion des transactions // TODO à remplacer par un TransactionManager
    private val deezerService: DeezerService
) : PlaylistService {

    private val validateInsertPlaylist = Validation {
        (PlaylistInsertForm::nom)(validateNomPlaylist())
    }

    private val validateUpdatePlaylist = Validation {
        PlaylistUpdateForm::nom.ifPresent(validateNomPlaylist())
    }

    companion object {
        private fun validateNomPlaylist(): ValidationBuilder<String>.() -> Unit = {
            minLength(1)
            maxLength(PLAYLIST_NAME_MAX_LENGTH)
            pattern("^(|(.*\\S.*))$".toRegex()) hint "Le nom de la playlist ne peut pas être uniquement composé d'espaces" // mais il peut être vide
        }
    }

    override fun insert(playlist: PlaylistInsertForm): Either<Union.U2<ValidationErrors, AlreadyExist.Playlist>, Long> {
        return when (val validationResult = validateInsertPlaylist(playlist)) {
            is Invalid -> validationResult.errors.toU2A().left()
            is Valid -> {
                if (playlistDAO.existsByNom(playlist.refJoueur, playlist.nom)) {
                    AlreadyExist.Playlist(playlist.nom).toU2B().left()
                } else {
                    playlistDAO.insert(playlist).right()
                }
            }
        }
    }

    override fun insert(
        url: String,
        idJoueur: Long
    ): Either<Union.U2<InvalidPlaylistUrl, AlreadyExist.Playlist>, Long> {
        val deezerPlaylistFromUrl = runBlocking {
            deezerService.getDeezerPlaylistFromUrl(url)
        }
        return jdbi.inTransactionUnchecked { h ->
            deezerPlaylistFromUrl
                .mapLeft { it.toU2A() }
                .map { p ->
                    val chansonsIds = p.tracks.data
                        .map {
                            chansonService.insert(ChansonUrl.Deezer(it.link), idJoueur)
                        }
                        .mapNotNull { e ->
                            e.fold(
                                { u -> u.use({ null }, { null }, { it.id }) },
                                { it }
                            )
                        } // "impossible" qu'il y ait une URL invalide, sauf s'il n'y a pas d'extrait disponible. Mais si une chanson existe déjà, on veut son ID

                    insert(p.toPlaylistInsertForm(idJoueur))
                        .mapLeft { u -> u.use({ null }, { it })!!.toU2B() } // idem que pour les chansons
                        .map { PlaylistUpdateForm(it, idJoueur, chansons = chansonsIds) }
                        .map { update(it).getOrNull()!!.id } // toutes ces erreurs ne doivent pas se produire
                }
                .flatMap { e -> e }
                .onLeft { h.rollback() }
        }
    }

    override fun update(playlist: PlaylistUpdateForm): Either<Union.U4<ValidationErrors, AlreadyExist.Playlist, EntityNotFoundForJoueur.Playlist, List<EntityNotFoundForJoueur.Chanson>>, PlaylistDTO> {
        val validationResult = validateUpdatePlaylist(playlist)
        if (validationResult is Invalid) {
            return Union.U4.ofA(validationResult.errors).left()
        }

        if (!playlistDAO.existsById(playlist.id, playlist.refJoueur)) {
            return Union.U4.ofC(EntityNotFoundForJoueur.Playlist(playlist.id)).left()
        }

        if (playlist.nom != null && playlistDAO.existsByNom(playlist.refJoueur, playlist.nom)) {
            return Union.U4.ofB(AlreadyExist.Playlist(playlist.nom)).left()
        }

        if (playlist.chansons != null) {
            val chansons = playlist.chansons

            val chansonsNotFound = chansons
                .filterNot { chansonService.existsById(it, playlist.refJoueur) }
                .map { EntityNotFoundForJoueur.Chanson(it) }

            if (chansonsNotFound.isNotEmpty()) {
                return Union.U4.ofD(chansonsNotFound).left()
            }
        }

        return playlistDAO.update(playlist).toDTO().right()
    }

    override fun delete(id: Long, idJoueur: Long): Either<EntityNotFoundForJoueur.Playlist, Unit> {
        return findById(id, idJoueur)
            .map { playlistDAO.delete(id) }
    }

    override fun findById(id: Long, idJoueur: Long): Either<EntityNotFoundForJoueur.Playlist, PlaylistDTO> {
        return playlistDAO.findById(id, idJoueur)?.toDTO()?.right() ?: EntityNotFoundForJoueur.Playlist(id).left()
    }

    override fun findById(id: Long): Either<EntityNotFound.Playlist, PlaylistDTO> {
        return playlistDAO.findById(id)?.toDTO()?.right() ?: EntityNotFound.Playlist(id).left()
    }

    override fun findAll(idJoueur: Long) = playlistDAO.findAll(idJoueur).map { it.toDTO() }
}
