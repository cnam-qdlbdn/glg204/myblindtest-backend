package net.lecnam.myblindtest.plugins

import net.lecnam.myblindtest.Configuration
import org.flywaydb.core.Flyway
import org.flywaydb.core.api.Location

fun migrateDatabase(configuration: Configuration) {
    with(configuration.connection.db) {
        val flyway = Flyway.configure()
            .cleanDisabled(!configuration.flyway.runClean)
            .dataSource(url, username, password.value)
            .apply {
                if (configuration.mode.dev) {
                    outOfOrder(true)
                    locations(*(locations + Location("classpath:db/testMigration")))
                }
            }
            .load()
        if (configuration.flyway.runClean) {
            flyway.clean()
        }
        flyway.migrate()
    }
}
