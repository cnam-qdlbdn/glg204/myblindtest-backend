package net.lecnam.myblindtest.plugins

import io.ktor.http.HttpStatusCode
import io.ktor.server.application.*
import io.ktor.server.auth.Principal
import io.ktor.server.auth.authentication
import io.ktor.server.auth.session
import io.ktor.server.response.respond
import io.ktor.server.sessions.SessionTransportTransformerEncrypt
import io.ktor.server.sessions.Sessions
import io.ktor.server.sessions.cookie
import io.ktor.util.hex
import net.lecnam.myblindtest.Configuration
import net.lecnam.myblindtest.authentication.AuthenticationInSession
import net.lecnam.myblindtest.authentication.AuthenticationService
import net.lecnam.myblindtest.exceptions.toErrorMessageContainer
import org.kodein.di.instance
import org.kodein.di.ktor.closestDI

const val SESSION_KEY = "myblindtest_session"

fun Application.configureSecurity() {
    authentication {
        session<UserSession>(SESSION_KEY) {
            validate { session ->
                val authenticationService by closestDI().instance<AuthenticationService>()
                if (authenticationService.isValid(session.auth)) {
                    session
                } else null
            }
            challenge {
                call.respond(HttpStatusCode.Unauthorized, "Authentification insuffisante".toErrorMessageContainer())
            }
        }
    }

    install(Sessions) {
        cookie<UserSession>(SESSION_KEY) {
            val configuration by this@configureSecurity.closestDI().instance<Configuration>()
            val sign = hex(configuration.ktor.session.sign.value)
            val encrypt = hex(configuration.ktor.session.encrypt.value)

            cookie.path = "/"
            cookie.secure = configuration.ktor.session.secure
            cookie.httpOnly = true
//            cookie.extensions["SameSite"] = "None"
            /*
            Le but n'est pas d'avoir une application ultra sécurisée. Oui vous pouvez certainement intercepter le
            cookie pour vous faire passer pour un autre joueur. Heureusement que ce n'est pas une application marchande !
            */
            cookie.maxAgeInSeconds = configuration.ktor.session.maxDuration.toSeconds()

            transform(SessionTransportTransformerEncrypt(encrypt, sign))
        }
    }
}

data class UserSession(
    val auth: AuthenticationInSession
) : Principal
