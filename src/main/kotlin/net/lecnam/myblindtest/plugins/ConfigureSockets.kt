package net.lecnam.myblindtest.plugins

import com.fasterxml.jackson.databind.ObjectMapper
import io.ktor.serialization.jackson.JacksonWebsocketContentConverter
import io.ktor.server.application.*
import io.ktor.server.websocket.WebSockets
import io.ktor.server.websocket.pingPeriod
import io.ktor.server.websocket.timeout
import org.kodein.di.instance
import org.kodein.di.ktor.closestDI
import java.time.Duration

fun Application.configureSockets() {
    val objectMapper: ObjectMapper by closestDI().instance()

    install(WebSockets) {
        contentConverter = JacksonWebsocketContentConverter(objectMapper)

        pingPeriod = Duration.ofSeconds(15)
        timeout = Duration.ofSeconds(15)
        maxFrameSize = Long.MAX_VALUE
        masking = false
    }
}
