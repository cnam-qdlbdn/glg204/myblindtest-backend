package net.lecnam.myblindtest.plugins

import io.ktor.server.application.*
import io.ktor.server.routing.routing
import net.lecnam.myblindtest.authentication.authenticationRouting
import net.lecnam.myblindtest.joueur.joueurRouting
import net.lecnam.myblindtest.librairie.chanson.chansonRouting
import net.lecnam.myblindtest.librairie.playlist.playlistRouting
import net.lecnam.myblindtest.librairie.tag.tagRouting
import net.lecnam.myblindtest.partie.partieRouting

fun Application.configureRouting() {
    routing {
        authenticationRouting()
        joueurRouting()
        tagRouting()
        chansonRouting()
        playlistRouting()
        partieRouting()
    }
}
