package net.lecnam.myblindtest.plugins

import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.ktor.client.HttpClient
import io.ktor.server.application.*
import io.lettuce.core.RedisClient
import io.lettuce.core.RedisCredentials
import io.lettuce.core.RedisCredentialsProvider
import io.lettuce.core.RedisURI
import net.lecnam.myblindtest.Configuration
import net.lecnam.myblindtest.api.deezer.DeezerService
import net.lecnam.myblindtest.api.deezer.DeezerServiceImpl
import net.lecnam.myblindtest.authentication.AuthenticationDAO
import net.lecnam.myblindtest.authentication.AuthenticationService
import net.lecnam.myblindtest.authentication.AuthenticationServiceImpl
import net.lecnam.myblindtest.getConfiguration
import net.lecnam.myblindtest.joueur.JoueurDAO
import net.lecnam.myblindtest.joueur.JoueurService
import net.lecnam.myblindtest.joueur.JoueurServiceImpl
import net.lecnam.myblindtest.librairie.chanson.ArtisteDAO
import net.lecnam.myblindtest.librairie.chanson.ChansonDAO
import net.lecnam.myblindtest.librairie.chanson.ChansonService
import net.lecnam.myblindtest.librairie.chanson.ChansonServiceImpl
import net.lecnam.myblindtest.librairie.playlist.PlaylistDAO
import net.lecnam.myblindtest.librairie.playlist.PlaylistService
import net.lecnam.myblindtest.librairie.playlist.PlaylistServiceImpl
import net.lecnam.myblindtest.librairie.playlist.dynamic.DynamicPlaylistDAO
import net.lecnam.myblindtest.librairie.playlist.dynamic.DynamicPlaylistService
import net.lecnam.myblindtest.librairie.playlist.dynamic.DynamicPlaylistServiceImpl
import net.lecnam.myblindtest.librairie.tag.TagDAO
import net.lecnam.myblindtest.librairie.tag.TagService
import net.lecnam.myblindtest.librairie.tag.TagServiceImpl
import net.lecnam.myblindtest.partie.PartieDAO
import net.lecnam.myblindtest.partie.PartieManager
import net.lecnam.myblindtest.partie.PartieService
import net.lecnam.myblindtest.partie.PartieServiceImpl
import net.lecnam.myblindtest.partie.historique.HistoriquePartieDAO
import net.lecnam.myblindtest.partie.historique.HistoriquePartieService
import net.lecnam.myblindtest.partie.historique.HistoriquePartieServiceImpl
import net.lecnam.myblindtest.partie.participant.ParticipantDAO
import net.lecnam.myblindtest.partie.participant.ParticipantService
import net.lecnam.myblindtest.partie.participant.ParticipantServiceImpl
import net.lecnam.myblindtest.partie.resultat.ResultatMancheDAO
import net.lecnam.myblindtest.partie.resultat.ResultatMancheService
import net.lecnam.myblindtest.partie.resultat.ResultatMancheServiceImpl
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.jackson2.Jackson2Config
import org.jdbi.v3.sqlobject.kotlin.onDemand
import org.kodein.di.DI
import org.kodein.di.bind
import org.kodein.di.bindProvider
import org.kodein.di.bindings.NoArgBindingDI
import org.kodein.di.eagerSingleton
import org.kodein.di.instance
import org.kodein.di.ktor.di
import org.kodein.di.provider
import org.kodein.di.singleton

fun Application.configureDI(httpClient: HttpClient, eager: Boolean) {
    di {
        import(diModule(httpClient, eager))
    }
}

fun diModule(httpClient: HttpClient? = null, eager: Boolean) = DI.Module(name = "full") {
    bind { provider { getConfiguration() } }
    if (httpClient != null) {
        bind { configuredSingleton(eager) { httpClient } }
    }
    bind {
        configuredSingleton(eager) {
            jacksonObjectMapper().apply {
                registerModule(JavaTimeModule())

                disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            }
        }
    }

    import(sgbdModule(eager))
    import(servicesModule(eager))
}

const val REDIS_PUBLISHER = "redis_publisher"
const val REDIS_SUBSCRIBER = "redis_subscriber"

private fun sgbdModule(eager: Boolean) = DI.Module(name = "SGBD") {
    bind {
        configuredSingleton(eager) {
            Jdbi.create(HikariDataSource(instance<Configuration>().connection.db.toHikariConfig()))
                .installPlugins()
                .also {
                    it.getConfig(Jackson2Config::class.java).mapper = instance()
                }
        }
    }
    bind(tag = REDIS_PUBLISHER) { redisClientSingleton(eager) }
    bind(tag = REDIS_SUBSCRIBER) { redisClientSingleton(eager) }

    bind { configuredSingleton(eager) { instance<Jdbi>().onDemand<JoueurDAO>() } }
    bind { configuredSingleton(eager) { instance<Jdbi>().onDemand<AuthenticationDAO>() } }
    bind { configuredSingleton(eager) { instance<Jdbi>().onDemand<TagDAO>() } }
    bind { configuredSingleton(eager) { ChansonDAO(instance(), instance()) } }
    bind { configuredSingleton(eager) { instance<Jdbi>().onDemand<ArtisteDAO>() } }
    bind { configuredSingleton(eager) { PlaylistDAO(instance()) } }
    bind { configuredSingleton(eager) { instance<Jdbi>().onDemand<DynamicPlaylistDAO>() } }
    bind { configuredSingleton(eager) { PartieDAO(instance(), instance()) } }
    bind { configuredSingleton(eager) { instance<Jdbi>().onDemand<ParticipantDAO>() } }
    bind { configuredSingleton(eager) { instance<Jdbi>().onDemand<ResultatMancheDAO>() } }
    bind { configuredSingleton(eager) { instance<Jdbi>().onDemand<HistoriquePartieDAO>() } }
}

private fun servicesModule(eager: Boolean) = DI.Module(name = "services") {
    bind<DeezerService> { configuredSingleton(eager) { DeezerServiceImpl(instance(), instance()) } }

    bind<AuthenticationService> {
        configuredSingleton(eager) {
            AuthenticationServiceImpl(
                instance(),
                instance(),
                instance(),
                instance()
            )
        }
    }
    bind<JoueurService> { configuredSingleton(eager) { JoueurServiceImpl(instance(), instance()) } }
    bind<TagService> { configuredSingleton(eager) { TagServiceImpl(instance()) } }
    bind<ChansonService> { configuredSingleton(eager) { ChansonServiceImpl(instance(), instance(), instance()) } }
    bind<PlaylistService> {
        configuredSingleton(eager) {
            PlaylistServiceImpl(
                instance(),
                instance(),
                instance(),
                instance()
            )
        }
    }
    bind<DynamicPlaylistService> { configuredSingleton(eager) { DynamicPlaylistServiceImpl(instance(), instance()) } }
    bind<PartieService> {
        configuredSingleton(eager) {
            PartieServiceImpl(
                instance(),
                instance(),
                instance(),
                instance()
            )
        }
    }
    bind<ParticipantService> { configuredSingleton(eager) { ParticipantServiceImpl(instance(), instance()) } }
    bind<ResultatMancheService> { configuredSingleton(eager) { ResultatMancheServiceImpl(instance()) } }
    bind<HistoriquePartieService> { configuredSingleton(eager) { HistoriquePartieServiceImpl(instance(), instance()) } }

    bindProvider {
        PartieManager(
            partieService = instance(),
            participantService = instance(),
            resultatMancheService = instance(),
            historiquePartieService = instance(),
            chansonService = instance(),
            jdbi = instance(),
            redisClientPub = instance(REDIS_PUBLISHER),
            redisClientSub = instance(REDIS_SUBSCRIBER),
            objectMapper = instance()
        )
    }
}

private fun DI.Builder.redisClientSingleton(
    eager: Boolean
) = configuredSingleton(eager) {
    val redisConfig = instance<Configuration>().connection.redis
    val redisURI = RedisURI.Builder.redis(redisConfig.host, redisConfig.port)
        .let {
            if (redisConfig.username != null && redisConfig.password != null) {
                it.withAuthentication(
                    RedisCredentialsProvider.from {
                        RedisCredentials.just(redisConfig.username, redisConfig.password.value.toCharArray())
                    }
                )
            } else it
        }
        .build()
    RedisClient.create(redisURI)
}

private inline fun <reified T : Any> DI.Builder.configuredSingleton(
    eager: Boolean,
    noinline creator: NoArgBindingDI<Any>.() -> T
) = if (eager) {
    eagerSingleton(creator)
} else {
    singleton(creator = creator)
}

private fun Configuration.Connection.DB.toHikariConfig(): HikariConfig {
    val hc = HikariConfig()
    hc.jdbcUrl = url
    hc.username = username
    hc.password = password.value
    hc.maximumPoolSize = maximumPoolSize

    return hc
}
