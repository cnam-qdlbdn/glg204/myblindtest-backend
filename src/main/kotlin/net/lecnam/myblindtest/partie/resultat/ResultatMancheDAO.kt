package net.lecnam.myblindtest.partie.resultat

import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys
import org.jdbi.v3.sqlobject.statement.SqlQuery
import org.jdbi.v3.sqlobject.statement.SqlUpdate
import org.jdbi.v3.sqlobject.transaction.Transaction

interface ResultatMancheDAO {
    @Transaction
    @SqlUpdate(
        """
        insert into resultat_manche (fk_partie, fk_joueur, numero_manche, is_titre, is_artiste, fk_tag, score)
        values (
            :resultatManche.refPartie,
            :resultatManche.refJoueur,
            :resultatManche.numeroManche,
            :resultatManche.isTitre,
            :resultatManche.isArtiste,
            :resultatManche.refTag,
            :resultatManche.score
        )
        """
    )
    @GetGeneratedKeys
    fun insert(resultatManche: CreateResultatManche): ResultatManche

    @SqlQuery("select * from resultat_manche where fk_partie = :idPartie")
    fun getAllByPartie(idPartie: Long): List<ResultatManche>
}
