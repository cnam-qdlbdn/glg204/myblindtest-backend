package net.lecnam.myblindtest.partie.resultat

interface ResultatMancheService {
    fun insert(resultatManche: CreateResultatManche): ResultatMancheDTO
}
