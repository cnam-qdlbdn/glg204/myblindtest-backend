package net.lecnam.myblindtest.partie.resultat

import net.lecnam.myblindtest.base.HasId
import net.lecnam.myblindtest.base.HasRefJoueur
import org.jdbi.v3.core.mapper.reflect.ColumnName

interface ResultatMancheData : HasRefJoueur {
    val refPartie: Long
    val numeroManche: Int
    val isTitre: Boolean
    val isArtiste: Boolean
    val refTag: Long?
    val score: Int
}

data class CreateResultatManche(
    override val refJoueur: Long,
    override val refPartie: Long,
    override val numeroManche: Int,
    override val isTitre: Boolean,
    override val isArtiste: Boolean,
    override val refTag: Long?,
    override val score: Int
) : ResultatMancheData

data class ResultatManche(
    override val id: Long,
    @ColumnName("fk_joueur")
    override val refJoueur: Long,
    @ColumnName("fk_partie")
    override val refPartie: Long,
    override val numeroManche: Int,
    override val isTitre: Boolean,
    override val isArtiste: Boolean,
    @ColumnName("fk_tag")
    override val refTag: Long?,
    override val score: Int
) : HasId, ResultatMancheData

data class ResultatMancheDTO(
    override val refJoueur: Long,
    override val refPartie: Long,
    override val numeroManche: Int,
    override val isTitre: Boolean,
    override val isArtiste: Boolean,
    override val refTag: Long?,
    override val score: Int
) : ResultatMancheData

fun ResultatManche.toDTO() = ResultatMancheDTO(
    refJoueur,
    refPartie,
    numeroManche,
    isTitre,
    isArtiste,
    refTag,
    score
)
