package net.lecnam.myblindtest.partie.resultat

class ResultatMancheServiceImpl(private val resultatMancheDAO: ResultatMancheDAO) : ResultatMancheService {
    // TODO valider les inputs (refPartie, refJoueur, refTag, etc.)
    override fun insert(resultatManche: CreateResultatManche) = resultatMancheDAO.insert(resultatManche).toDTO()
}
