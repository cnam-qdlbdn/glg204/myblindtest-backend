package net.lecnam.myblindtest.partie.websocket

data class GameState(
    val playersReady: Map<Long, Boolean> = emptyMap(),
    val songStarted: Boolean = false,
    val buzzerActivated: Long = NO_BUZZER_ACTIVATED,
    val alreadyBuzzed: Set<Long> = emptySet()
) {
    companion object {
        const val NO_BUZZER_ACTIVATED = -1L
    }
}
