package net.lecnam.myblindtest.partie.websocket

import net.lecnam.myblindtest.librairie.chanson.ChansonDTO
import net.lecnam.myblindtest.librairie.chanson.ChansonUrl
import net.lecnam.myblindtest.partie.PartieDTO
import net.lecnam.myblindtest.partie.UpdatePartieDTO
import net.lecnam.myblindtest.partie.historique.HistoriquePartieDTO
import net.lecnam.myblindtest.partie.resultat.ResultatMancheDTO

enum class TypeMessage(val shouldUpdatePartie: Boolean = false) {
    CONNECT, // in
    CONNECT_BROADCAST(true), // out
    DISCONNECT_BROADCAST(true), // out // pas de besoin de message "in" DISCONNECT, il suffira de fermer la connexion pour prévenir tout le monde
    UPDATE_ROOM_OPTIONS, // in
    UPDATE_ROOM_OPTIONS_ERROR, // out
    UPDATE_ROOM_OPTIONS_BROADCAST(true), // out
    INITIAL_GAME_INFORMATIONS, // out
    EVERYONE_OUT, // out
    START_GAME, // in // équivalent à NEXT_SONG, et permet aussi de démarrer la partie
    START_GAME_BROADCAST(true), // out
    START_GAME_TO_GAMEMASTER(true), // out

    EXIT, // in
    EXIT_BROADCAST(true), // out
    PLAYER_READY, // in
    PLAYER_READY_BROADCAST, // out // uniquement pour de la communication inter-backend
    START_SONG, // out
    BUZZER_TRIGGERED, // in
    BUZZER_TRIGGERED_BROADCAST, // out
    ANSWER, // in
    ANSWER_TO_GAMEMASTER, // out (specific)
    ANSWER_OK_FROM_GAMEMASTER, // in
    ANSWER_KO_FROM_GAMEMASTER, // in
    ANSWER_OK_BROADCAST(true), // out
    ANSWER_KO_BROADCAST(true), // out
    SONG_ENDED, // in // uniquement envoyé par le créateur de la partie
    NEXT_SONG_SIGNAL, // out // uniquement pour de la communication inter-backend
    NEXT_SONG(true), // out
    NEXT_SONG_TO_GAMEMASTER(true), // out
    PLEASE_WAIT_FOR_NEXT_SONG, // out // dans le cas où un joueur vient de se reconnecter, il faut qu'il attende la prochaine chanson
    GAME_FINISHED_SIGNAL, // out // uniquement pour de la communication inter-backend
    GAME_FINISHED(true) // out
}

data class Message(
    val type: TypeMessage,
    val content: Map<String, *>
)

sealed interface MessageContent {
    data class Connect(val codePartie: String) : MessageContent
    data class ConnectBroadcast(val refJoueur: Long) : MessageContent
    data class DisconnectBroadcast(val refJoueur: Long) : MessageContent
    data class UpdateRoomOptions(val updatePartie: UpdatePartieDTO) : MessageContent
    data class UpdateRoomOptionsError(val message: String) : MessageContent
    data class UpdateRoomOptionsBroadcast(val partie: PartieDTO) : MessageContent
    data class InitialGameInformations(val partie: PartieDTO) : MessageContent
    object EveryoneOut : MessageContent
    data class StartGameBroadcast(val partie: PartieDTO, val chansonUrl: ChansonUrl) : MessageContent
    data class StartGameToGamemaster(val partie: PartieDTO, val chanson: ChansonDTO) : MessageContent

    data class ExitBroadcast(val refJoueur: Long) : MessageContent
    data class PlayerReadyBroadcast(val refJoueur: Long) : MessageContent
    object StartSong : MessageContent
    data class BuzzerTriggeredBroadcast(val refJoueur: Long) : MessageContent
    data class AnswerOkBroadcast(val resultatManche: ResultatMancheDTO) : MessageContent
    data class AnswerKoBroadcast(val alreadyBuzzed: Set<Long>) : MessageContent
    object NextSongSignal : MessageContent
    data class NextSong(val partie: PartieDTO, val chansonUrl: ChansonUrl) : MessageContent
    data class NextSongToGamemaster(val partie: PartieDTO, val chanson: ChansonDTO) : MessageContent

    object PleaseWaitForNextSong : MessageContent

    object GameFinishedSignal : MessageContent
    data class GameFinished(val historiquesPartie: List<HistoriquePartieDTO>) : MessageContent
}
