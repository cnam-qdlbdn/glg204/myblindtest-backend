package net.lecnam.myblindtest.partie

import arrow.core.Either
import arrow.core.continuations.update
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.github.oshai.KotlinLogging
import io.ktor.server.websocket.DefaultWebSocketServerSession
import io.ktor.server.websocket.receiveDeserialized
import io.ktor.server.websocket.sendSerialized
import io.ktor.websocket.CloseReason
import io.ktor.websocket.Frame
import io.ktor.websocket.close
import io.ktor.websocket.readText
import io.ktor.websocket.send
import io.lettuce.core.RedisClient
import io.lettuce.core.pubsub.RedisPubSubAdapter
import io.lettuce.core.pubsub.RedisPubSubListener
import io.lettuce.core.pubsub.StatefulRedisPubSubConnection
import io.lettuce.core.pubsub.api.async.RedisPubSubAsyncCommands
import io.lettuce.core.pubsub.api.sync.RedisPubSubCommands
import kotlinx.coroutines.async
import kotlinx.coroutines.withTimeoutOrNull
import net.lecnam.myblindtest.authentication.AuthenticationInSession
import net.lecnam.myblindtest.base.RefreshableLazy
import net.lecnam.myblindtest.base.refreshableLazy
import net.lecnam.myblindtest.ext.getPrincipalAuth
import net.lecnam.myblindtest.ext.returnIfEqual
import net.lecnam.myblindtest.librairie.chanson.ChansonDTO
import net.lecnam.myblindtest.librairie.chanson.ChansonDeezerDTO
import net.lecnam.myblindtest.librairie.chanson.ChansonService
import net.lecnam.myblindtest.librairie.chanson.ChansonUrl
import net.lecnam.myblindtest.partie.historique.HistoriquePartieService
import net.lecnam.myblindtest.partie.participant.ParticipantService
import net.lecnam.myblindtest.partie.resultat.CreateResultatManche
import net.lecnam.myblindtest.partie.resultat.ResultatMancheService
import net.lecnam.myblindtest.partie.websocket.GameState
import net.lecnam.myblindtest.partie.websocket.Message
import net.lecnam.myblindtest.partie.websocket.MessageContent
import net.lecnam.myblindtest.partie.websocket.TypeMessage
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.kotlin.inTransactionUnchecked
import java.util.concurrent.atomic.AtomicReference
import kotlin.time.Duration.Companion.seconds

private val logger = KotlinLogging.logger {}

private const val REDIS_CHANNEL = "mbt:game_event"

class PartieManager(
    private val partieService: PartieService,
    private val participantService: ParticipantService,
    private val resultatMancheService: ResultatMancheService,
    private val historiquePartieService: HistoriquePartieService,
    private val chansonService: ChansonService,
    private val jdbi: Jdbi, // pour la gestion des transactions // TODO à remplacer par un TransactionManager
    private val redisClientPub: RedisClient,
    private val redisClientSub: RedisClient,
    private val objectMapper: ObjectMapper
) {
    private lateinit var redisPubConnection: StatefulRedisPubSubConnection<String, String>
    private lateinit var redisSubConnection: StatefulRedisPubSubConnection<String, String>
    private lateinit var redisPubAsync: RedisPubSubAsyncCommands<String, String>
    private lateinit var redisSubSync: RedisPubSubCommands<String, String>
    private lateinit var gameChannel: String
    private lateinit var redisListener: RedisPubSubListener<String, String>

    private lateinit var partieHolder: RefreshableLazy<PartieDTO>

    private val gameState: AtomicReference<GameState> = AtomicReference(GameState())

    suspend fun handle(webSocketServerSession: DefaultWebSocketServerSession) {
        webSocketServerSession.apply {
            // ============ onConnect
            val auth = getPrincipalAuth()

            var partieEntity = handleConnect(auth) ?: return
            partieHolder = refreshableLazy {
                partieService.findById(partieEntity.id).getOrNull()
                    ?: throw IllegalStateException("La partie avec l'id ${partieEntity.id} devrait exister")
            }

            try {
                if (partieEntity.etat == EtatPartie.CREATED) { // on sait déjà que c'est le créateur de la partie qui est connecté
                    logger.info { "[${partieEntity.codePartie}] Ouverture de la partie par son créateur [${auth.joueur.login}]" }
                    jdbi.inTransactionUnchecked {
                        partieService.nextEtat(partieEntity.id)
                        participantService.addParticipant(partieEntity.id, auth.joueur.id, true)
                    }
                }

                var partie = partieHolder()
                when (partie.etat) {
                    EtatPartie.OPENED -> {
                        if (auth.joueur.id != partie.refCreateur) {
                            logger.info { "[${partie.codePartie}] Ajout de [${auth.joueur.login}] à la liste des participants" }
                            participantService.addParticipant(partie.id, auth.joueur.id)
                            partie = partieHolder.refresh()
                        }
                    }

                    EtatPartie.STARTED -> {
                        if (partie.participants.none { it.refJoueur == auth.joueur.id }) {
                            throw IllegalStateException("Impossible de rejoindre une partie en cours")
                        }

                        logger.info { "[${partie.codePartie}] Re-connexion de [${auth.joueur.login}] à la partie" }
                        participantService.connectParticipant(partie.id, auth.joueur.id)
                        gameState.update { gs -> gs.copy(playersReady = partie.participants.associate { it.refJoueur to false }) }
                        sendSerialized(MessageContent.PleaseWaitForNextSong.toMessage())
                    }

                    else -> throw IllegalStateException("Situation impossible (à ce stade, la partie ne peut être que OPENED ou STARTED)")
                }

                sendSerialized(MessageContent.InitialGameInformations(partie).toMessage())

                // on le notifie de sa connexion (ainsi que tous les autres participants), et on démarre la machinerie Redis
                initRedisTools(auth)

                if (partie.participants.count { it.connected } > 1) {
                    logger.debug { "[${partie.codePartie}] Broadcast de la connexion de [${auth.joueur.login}]" }
                    redisPubAsync.publish(gameChannel, MessageContent.ConnectBroadcast(auth.joueur.id).toMessage())
                }

                for (frame in incoming) {
                    frame as Frame.Text

                    partie = partieHolder()
                    if (partie.etat == EtatPartie.OPENED) {
                        if (partie.participants.none { p -> p.refJoueur == auth.joueur.id && p.maitreJoueur }) { // si le joueur qui envoie les messages n'est pas le maitre joueur, c'est une erreur de code
                            throw IllegalStateException("Message reçu alors que le participant n'est pas le maitre joueur")
                        }

                        val message = objectMapper.readValue<Message>(frame.readText())

                        when (message.type) {
                            TypeMessage.UPDATE_ROOM_OPTIONS -> {
                                handleUpdateRoomOptionsMessage(message, auth)
                                partie = partieHolder()
                            }

                            TypeMessage.START_GAME -> {
                                partieEntity = getPartieEntity()

                                handleStartGameMessage(partieEntity, auth)
                                partie = partieHolder()
                                gameState.update { gs -> gs.copy(playersReady = partie.participants.associate { it.refJoueur to false }) }
                            }

                            else -> throw IllegalStateException("Type de message inattendu : $message")
                        }
                    } else if (partie.etat == EtatPartie.STARTED) {
                        val message = objectMapper.readValue<Message>(frame.readText())

                        when (message.type) {
                            TypeMessage.EXIT -> {
                                if (partie.refCreateur == auth.joueur.id) {
                                    close(CloseReason(CloseReason.Codes.NORMAL, "Merci d'avoir hébergé cette partie !"))
                                    // Le reste est géré par onClose
                                } else {
                                    logger.info { "[${partie.codePartie}] Départ de [${auth.joueur.login}], il n'est plus un participant" }
                                    participantService.removeParticipant(partie.id, auth.joueur.id)
                                    redisPubAsync.publish(
                                        gameChannel,
                                        MessageContent.ExitBroadcast(auth.joueur.id).toMessage()
                                    )
                                }
                            }

                            // TODO il faudrait rajouter un message SONG_UNAVAILABLE, pour signaler que la chanson ne peut pas être jouée, et passer à la chanson suivante
                            TypeMessage.PLAYER_READY -> {
                                // TODO ajouter un système de vérification de l'état de la manche avec une machine à état (pour n'accepter que certains messages à un certain moment)

                                redisPubAsync.publish(
                                    gameChannel,
                                    MessageContent.PlayerReadyBroadcast(auth.joueur.id).toMessage()
                                )
                            }

                            TypeMessage.BUZZER_TRIGGERED -> {
                                if (!partie.buzzer || partie.participants.any { it.maitreJoueur && it.refJoueur == auth.joueur.id }) {
                                    throw IllegalStateException("Le client ne devrait pas pouvoir envoyer de signal de buzzer !")
                                }

                                val (_, _, buzzerActivated, alreadyBuzzed) = gameState.get()
                                if (buzzerActivated == GameState.NO_BUZZER_ACTIVATED && auth.joueur.id !in alreadyBuzzed) {
                                    redisPubAsync.publish(
                                        gameChannel,
                                        MessageContent.BuzzerTriggeredBroadcast(auth.joueur.id).toMessage()
                                    )
                                } else {
                                    logger.warn { "[${partie.codePartie}] Le joueur [${auth.joueur.login}] a tenté de buzzer, mais le buzzer est déjà bloqué" }
                                }
                            }

                            TypeMessage.ANSWER -> {
                                if (!partie.checkText) {
                                    throw IllegalStateException("Le client ne devrait pas pouvoir envoyer de réponse textuelle !")
                                }

                                TODO("Faire un bon algo pour déterminer si le joueur a le droit de répondre, s'il répond dans les temps, envoyer la réponse au MJ, etc.")
                            }

                            TypeMessage.ANSWER_OK_FROM_GAMEMASTER -> {
                                if (!partie.buzzer) {
                                    throw IllegalStateException("Ce n'est pas une partie avec buzzer, il n'y a pas de maitre joueur pour valider les réponses")
                                }

                                if (partie.checkAll) {
                                    TODO("partie avec vérification sur tous les champs")
                                }

                                if (!partie.modeBlitz) {
                                    TODO("partie sans mode blitz")
                                } else {
                                    val resultatManche = resultatMancheService.insert(
                                        CreateResultatManche(
                                            refJoueur = gameState.get().buzzerActivated,
                                            refPartie = partie.id,
                                            numeroManche = partie.numeroManche,
                                            isTitre = false,
                                            isArtiste = false,
                                            refTag = null,
                                            score = 10 // TODO faire un meilleur scoring
                                        )
                                    )

                                    redisPubAsync.publish(
                                        gameChannel,
                                        MessageContent.AnswerOkBroadcast(resultatManche).toMessage()
                                    )
                                }
                            }

                            TypeMessage.ANSWER_KO_FROM_GAMEMASTER -> {
                                if (!partie.buzzer) {
                                    throw IllegalStateException("Ce n'est pas une partie avec buzzer, il n'y a pas de maitre joueur pour valider les réponses")
                                }

                                if (partie.checkAll) {
                                    TODO("partie avec vérification sur tous les champs")
                                }

                                if (!partie.modeBlitz) {
                                    TODO("partie sans mode blitz")
                                } else {
                                    // TODO le workflow sera différent en fonction des options
                                    //  Typiquement, s'il y a une vérification textuelle et qu'on est en mode blitz, on
                                    //  affiche la saisie qui a été faite (et qui est mauvaise). Mais si on est pas en
                                    //  mode blitz, il faut seulement afficher si la réponse est bonne ou mauvaise.

                                    redisPubAsync.publish(
                                        gameChannel,
                                        MessageContent.AnswerKoBroadcast(gameState.get().alreadyBuzzed).toMessage()
                                    )
                                }
                            }

                            TypeMessage.SONG_ENDED -> {
                                redisPubAsync.publish(gameChannel, message)
                            }

                            else -> throw IllegalStateException("Type de message inattendu : $message")
                        }
                    }
                }
            } catch (e: Exception) {
                logger.error(e) { "[${partieHolder().codePartie}] Arrêt prématuré de la session" }
                close(CloseReason(CloseReason.Codes.INTERNAL_ERROR, e.message!!))
            } finally {
                // ============ onClose
                onClose(auth)
            }
        }
    }

    private fun getPartieEntity() = partieService.getByCodePartie(partieHolder().codePartie.toCodePartie()).fold(
        { throw IllegalStateException("La partie avec l'id ${partieHolder().id} devrait exister") },
        { it }
    )

    private fun getChansonData(partieEntity: Partie, numeroManche: Int): Pair<ChansonDTO, ChansonUrl> {
        return chansonService.findById(partieEntity.chansons[numeroManche - 1])
            .map { chanson ->
                when (chanson) {
                    is ChansonDeezerDTO -> chanson to ChansonUrl.Deezer(chanson.extrait)
                }
            }
            .fold(
                { throw IllegalStateException("La chanson avec l'id ${it.id} devrait exister") },
                { it }
            )
    }

    private fun handleStartGameMessage(partieEntity: Partie, auth: AuthenticationInSession) {
        var partie = partieHolder()
        check(partie.participants.any { it.maitreJoueur && it.refJoueur == auth.joueur.id }) { "Seul le maître joueur peut démarrer la partie" }

        val chansonData = getChansonData(partieEntity, partie.numeroManche)

        partieService.nextEtat(partie.id)
            .fold(
                { u ->
                    throw IllegalStateException(
                        "Erreur lors du changement d'état : " + u.use(
                            { it.message },
                            { it.message }
                        )
                    )
                },
                {}
            )
        partie = partieHolder.refresh()

        redisPubAsync.publish(gameChannel, MessageContent.StartGameBroadcast(partie, chansonData.second).toMessage())
        redisPubAsync.publish(gameChannel, MessageContent.StartGameToGamemaster(partie, chansonData.first).toMessage())
    }

    private suspend fun DefaultWebSocketServerSession.handleConnect(auth: AuthenticationInSession): Partie? {
        // Le premier message doit être un CONNECT vers une partie existante
        val timeout = 10.seconds
        val message = withTimeoutOrNull(timeout) {
            receiveDeserialized<Message>()
        }

        if (message == null) {
            logger.warn { "[${auth.joueur.login}] Pas de message reçu en $timeout" }
            close(CloseReason(CloseReason.Codes.NORMAL, "No connect message received"))
            return null
        } else if (message.type != TypeMessage.CONNECT) {
            logger.warn { "[${auth.joueur.login}] Message reçu pas de type CONNECT" }
            close(CloseReason(CloseReason.Codes.NORMAL, "No connect message received"))
            return null
        }

        val connect = convert<MessageContent.Connect>(message)
        val partie = partieService.getByCodePartie(connect.codePartie.toCodePartie())
        when (partie) {
            is Either.Left -> {
                logger.warn { "[${auth.joueur.login}] Partie non trouvée pour le code [${connect.codePartie}]" }
                close(CloseReason(CloseReason.Codes.NORMAL, partie.value.message))
                return null
            }

            is Either.Right -> {
                val p = partie.value
                if (p.etat == EtatPartie.CREATED && p.refCreateur != auth.joueur.id) {
                    logger.warn { "[${auth.joueur.login}] Tentative de connexion à la partie [${p.codePartie}] qui n'est pas encore ouverte" }
                    close(
                        CloseReason(
                            CloseReason.Codes.NORMAL,
                            "La partie n'a pas encore été ouverte, seriez-vous devin ?"
                        )
                    )
                    return null
                }
            }
        }

        return partie.value
    }

    private suspend fun DefaultWebSocketServerSession.handleUpdateRoomOptionsMessage(
        message: Message,
        auth: AuthenticationInSession
    ) {
        var partie = partieHolder()
        val updateRoomOptions = convert<MessageContent.UpdateRoomOptions>(message)
        partieService.updateOptions(partie.id, updateRoomOptions.updatePartie)
            .mapLeft { u ->
                u.use(
                    { it.message },
                    { it.message },
                    { it.message },
                    { it.message }
                )
            }
            .fold(
                {
                    sendSerialized(MessageContent.UpdateRoomOptionsError(it).toMessage())
                },
                {
                    partie = partieHolder.refresh()
                    logger.debug { "[${partie.codePartie}] Broadcast de la mise à jour des paramètres de la partie par [${auth.joueur.login}]" }
                    logger.trace { "[${partie.codePartie}] Mise à jour des paramètres de la partie : $partie" }
                    redisPubAsync.publish(
                        gameChannel,
                        MessageContent.UpdateRoomOptionsBroadcast(partie).toMessage()
                    )
                }
            )
    }

    private fun onClose(auth: AuthenticationInSession) {
        logger.debug { "[${auth.joueur.login}] Traitement de la fermeture de la session à la partie [${partieHolder().codePartie}]" }

        if (::redisSubSync.isInitialized) {
            redisSubSync.unsubscribe(gameChannel)
            redisSubConnection.removeListener(redisListener)
        }

        // TODO vérifier si c'est nécessaire de refresh la partie ici
        val partie = partieHolder.refresh()
        when (partie.etat) {
            EtatPartie.CREATED -> throw IllegalStateException("Situation impossible (la partie est censée ne plus être CREATED)")
            EtatPartie.FINISHED -> {
                logger.debug { "[${partie.codePartie}] La partie est déjà terminée, plus rien à faire lors de la déconnexion" }
            }

            else -> {
                if (partie.refCreateur == auth.joueur.id) { // si le créateur quitte la partie, il éjecte tout le monde
                    logger.info { "[${partie.codePartie}] Le créateur [${auth.joueur.login}] s'est déconnecté du salon, la partie est finie, on kick tout le monde" }
                    partieService.finish(partie.id).getOrNull()!!
                    redisPubAsync.publish(gameChannel, MessageContent.EveryoneOut.toMessage())
                } else {
                    if (partie.etat == EtatPartie.OPENED) {
                        logger.info { "[${partie.codePartie}] Déconnexion de [${auth.joueur.login}], il n'est plus un participant" }
                        participantService.removeParticipant(partie.id, auth.joueur.id)
                    } else {
                        logger.info { "[${partie.codePartie}] Déconnexion de [${auth.joueur.login}]" }
                        participantService.disconnectParticipant(partie.id, auth.joueur.id)
                    }

                    if (partie.participants.count { it.connected } <= 1) { // si c'était le dernier participant
                        logger.info { "[${partie.codePartie}] Tout le monde s'est déconnecté, fin de partie" }
                        partieService.finish(partie.id).getOrNull()!!
                    } else {
                        // S'il reste des participants, on les prévient pour qu'ils mettent à jour leur affichage
                        logger.debug { "[${partie.codePartie}] Broadcast de la déconnexion de [${auth.joueur.login}]" }
                        redisPubAsync.publish(
                            gameChannel,
                            MessageContent.DisconnectBroadcast(auth.joueur.id).toMessage()
                        )
                    }
                }
            }
        }
    }

    @Suppress("DeferredResultUnused")
    private fun DefaultWebSocketServerSession.initRedisTools(auth: AuthenticationInSession) {
        redisPubConnection = redisClientPub.connectPubSub()
        redisSubConnection = redisClientSub.connectPubSub()
        redisPubAsync = redisPubConnection.async()
        redisSubSync = redisSubConnection.sync()
        gameChannel = "$REDIS_CHANNEL:${partieHolder().codePartie}"

        redisListener = object : RedisPubSubAdapter<String, String>() {
            override fun message(channel: String?, message: String?) {
                message?.also { m ->
                    async {
                        val mObj = objectMapper.readValue<Message>(m)
                        if (mObj.type.shouldUpdatePartie) {
                            partieHolder.refresh()
                        }

                        when (mObj.type) {
                            TypeMessage.CONNECT_BROADCAST -> {
                                // On n'envoie pas le broadcast de connexion à celui qui vient de se connecter
                                if (convert<MessageContent.ConnectBroadcast>(mObj).refJoueur == auth.joueur.id) {
                                    return@async
                                }
                            }

                            TypeMessage.DISCONNECT_BROADCAST -> {
                                val disconnect = convert<MessageContent.DisconnectBroadcast>(mObj)
                                gameState.update { gs ->
                                    gs.copy(
                                        playersReady = gs.playersReady + (disconnect.refJoueur to false),
                                        buzzerActivated = gs.buzzerActivated.returnIfEqual(
                                            disconnect.refJoueur,
                                            GameState.NO_BUZZER_ACTIVATED
                                        )
                                    )
                                }
                            }

                            TypeMessage.START_GAME_BROADCAST, TypeMessage.NEXT_SONG -> {
                                val partie = partieHolder()

                                // ici, on vérifie par rapport au maitre joueur plutôt qu'au créateur de la partie, au cas où le créateur joue seul (snif !)
                                if (partie.participants.any { it.maitreJoueur && it.refJoueur == auth.joueur.id }) {
                                    return@async
                                }
                            }

                            TypeMessage.START_GAME_TO_GAMEMASTER, TypeMessage.NEXT_SONG_TO_GAMEMASTER -> {
                                val partie = partieHolder()

                                // sil n'y a pas de maitre joueur, il n'y a personne à qui envoyer ce message
                                if (partie.participants.none { it.maitreJoueur && it.refJoueur == auth.joueur.id }) {
                                    return@async
                                }
                            }

                            TypeMessage.EXIT_BROADCAST -> {
                                val exit = convert<MessageContent.ExitBroadcast>(mObj)
                                gameState.update { gs ->
                                    gs.copy(
                                        playersReady = gs.playersReady - exit.refJoueur,
                                        buzzerActivated = gs.buzzerActivated.returnIfEqual(
                                            exit.refJoueur,
                                            GameState.NO_BUZZER_ACTIVATED
                                        )
                                    )
                                }
                            }

                            TypeMessage.BUZZER_TRIGGERED_BROADCAST -> {
                                val buzzerTriggered = convert<MessageContent.BuzzerTriggeredBroadcast>(mObj)
                                gameState.update { gs ->
                                    gs.copy(
                                        buzzerActivated = buzzerTriggered.refJoueur,
                                        alreadyBuzzed = gs.alreadyBuzzed + buzzerTriggered.refJoueur
                                    )
                                }
                            }

                            TypeMessage.PLAYER_READY_BROADCAST -> {
                                val playerReady = convert<MessageContent.PlayerReadyBroadcast>(mObj)
                                val (playersReady, _, _, _) = gameState.updateAndGet { gs ->
                                    gs.copy(
                                        playersReady = gs.playersReady + (playerReady.refJoueur to true)
                                    )
                                }

                                val partie = partieHolder()
                                // Si le créateur de la partie reçoit un message PLAYER_READY_BROADCAST et que tous les joueurs connectés sont ready, on lance la chanson
                                if (auth.joueur.id == partie.refCreateur &&
                                    partie.participants
                                        .filter { it.connected }
                                        .map { it.refJoueur }
                                        .toSet() == playersReady
                                        .filter { it.value }
                                        .keys
                                ) {
                                    redisPubAsync.publish(gameChannel, MessageContent.StartSong.toMessage())
                                }

                                return@async
                            }

                            TypeMessage.START_SONG -> {
                                gameState.update { gs -> gs.copy(songStarted = true) }
                            }

                            TypeMessage.ANSWER_OK_BROADCAST -> {
                                val partie = partieHolder()

                                // TODO déterminer les conditions de passage à la chanson suivante (checkAll, etc.)
                                if (partie.modeBlitz && !partie.checkAll) {
                                    publishNextRoundMessage(auth, partie)
                                } else {
                                    // TODO update gamestate
                                    TODO("gérer les autres cas quand la réponse est OK en fonction du paramétrage")
                                }
                            }

                            TypeMessage.ANSWER_KO_BROADCAST -> {
                                val partie = partieHolder()

                                if (partie.modeBlitz && !partie.checkAll) {
                                    val (_, _, _, alreadyBuzzed) = gameState.updateAndGet { gs ->
                                        val nextBuzzed =
                                            if (
                                                partie.participants
                                                    .filter { it.connected && !it.maitreJoueur }
                                                    .map { it.refJoueur }
                                                    .containsAll(gs.alreadyBuzzed)
                                            ) {
                                                emptySet()
                                            } else {
                                                gs.alreadyBuzzed
                                            }
                                        gs.copy(
                                            buzzerActivated = GameState.NO_BUZZER_ACTIVATED,
                                            alreadyBuzzed = nextBuzzed
                                        )
                                    }

                                    sendSerialized(
                                        MessageContent.AnswerKoBroadcast(alreadyBuzzed).toMessage()
                                    ) // on reforge le message à envoyer !
                                    return@async
                                } else {
                                    // TODO update gamestate
                                    TODO("gérer les autres cas quand la réponse est KO en fonction du paramétrage")
                                }
                            }

                            TypeMessage.SONG_ENDED -> {
                                gameState.update { gs -> gs.copy(songStarted = false) }
                                publishNextRoundMessage(auth, partieHolder())
                                return@async // le message SONG_ENDED n'a pas vocation à être republié
                            }

                            TypeMessage.NEXT_SONG_SIGNAL -> {
                                var partie = partieHolder()

                                gameState.update { gs ->
                                    gs.copy(
                                        playersReady = gs.playersReady.mapValues { false },
                                        songStarted = false,
                                        buzzerActivated = GameState.NO_BUZZER_ACTIVATED,
                                        alreadyBuzzed = emptySet()
                                    )
                                }

                                if (auth.joueur.id == partie.refCreateur) {
                                    partieService.nextManche(partie.id)
                                        .fold(
                                            { throw IllegalStateException("Le passage à la manche suivante devrait être possible") },
                                            { logger.info { "[${partie.codePartie}] Passage à la manche ${partie.numeroManche + 1}" } }
                                        )

                                    partie = partieHolder.refresh()
                                    val chansonData =
                                        getChansonData(getPartieEntity(), partie.numeroManche)
                                    redisPubAsync.publish(
                                        gameChannel,
                                        MessageContent.NextSong(partie, chansonData.second).toMessage()
                                    )
                                    redisPubAsync.publish(
                                        gameChannel,
                                        MessageContent.NextSongToGamemaster(partie, chansonData.first).toMessage()
                                    )
                                }
                                return@async // c'est un signal, il ne doit pas sortir
                            }

                            TypeMessage.GAME_FINISHED_SIGNAL -> {
                                val partie = partieHolder()

                                if (auth.joueur.id == partie.refCreateur) {
                                    partieService.nextEtat(partie.id)
                                    val historiqueParties =
                                        historiquePartieService.createHistoriquesFromPartie(partie.id)
                                            .fold(
                                                { throw IllegalStateException("La partie avec l'id ${partie.id} devrait exister") },
                                                { it }
                                            )

                                    redisPubAsync.publish(
                                        gameChannel,
                                        MessageContent.GameFinished(historiqueParties).toMessage()
                                    )
                                }
                                return@async // c'est un signal, il ne doit pas sortir
                            }

                            else -> {}
                        }

                        send(m)

                        // Si on voit passer un EVERYONE_OUT, on close
                        if (mObj.type == TypeMessage.EVERYONE_OUT) {
                            close(CloseReason(CloseReason.Codes.NORMAL, "L'hôte a fermé la partie"))
                        } else if (mObj.type == TypeMessage.GAME_FINISHED) {
                            close(CloseReason(CloseReason.Codes.NORMAL, "Merci d'avoir joué !"))
                        }
                    }
                }
            }
        }
        redisSubConnection.addListener(redisListener)
        redisSubSync.subscribe(gameChannel)
    }

    private fun publishNextRoundMessage(auth: AuthenticationInSession, partie: PartieDTO) {
        // C'est le créateur de la partie qui pilote la partie pour les clients
        if (auth.joueur.id == partie.refCreateur) {
            if (partie.numeroManche < partie.nombreManches) {
                redisPubAsync.publish(
                    gameChannel,
                    MessageContent.NextSongSignal.toMessage()
                )
            } else {
                redisPubAsync.publish(
                    gameChannel,
                    MessageContent.GameFinishedSignal.toMessage()
                )
            }
        }
    }

    private inline fun <reified T : MessageContent> convert(message: Message) =
        objectMapper.readValue<T>(objectMapper.writeValueAsString(message.content))

    private fun <T : MessageContent> T.toMessage(): Message {
        val content = objectMapper.readValue<Map<String, *>>(objectMapper.writeValueAsString(this))
        return when (this) {
            is MessageContent.Connect -> Message(TypeMessage.CONNECT, content)
            is MessageContent.ConnectBroadcast -> Message(TypeMessage.CONNECT_BROADCAST, content)
            is MessageContent.DisconnectBroadcast -> Message(TypeMessage.DISCONNECT_BROADCAST, content)
            is MessageContent.UpdateRoomOptionsError -> Message(TypeMessage.UPDATE_ROOM_OPTIONS_ERROR, content)
            is MessageContent.UpdateRoomOptionsBroadcast -> Message(TypeMessage.UPDATE_ROOM_OPTIONS_BROADCAST, content)
            is MessageContent.InitialGameInformations -> Message(TypeMessage.INITIAL_GAME_INFORMATIONS, content)
            is MessageContent.EveryoneOut -> Message(TypeMessage.EVERYONE_OUT, content)
            is MessageContent.StartGameBroadcast -> Message(TypeMessage.START_GAME_BROADCAST, content)
            is MessageContent.StartGameToGamemaster -> Message(TypeMessage.START_GAME_TO_GAMEMASTER, content)
            is MessageContent.ExitBroadcast -> Message(TypeMessage.EXIT_BROADCAST, content)
            is MessageContent.PlayerReadyBroadcast -> Message(TypeMessage.PLAYER_READY_BROADCAST, content)
            is MessageContent.StartSong -> Message(TypeMessage.START_SONG, content)
            is MessageContent.BuzzerTriggeredBroadcast -> Message(TypeMessage.BUZZER_TRIGGERED_BROADCAST, content)
            is MessageContent.AnswerOkBroadcast -> Message(TypeMessage.ANSWER_OK_BROADCAST, content)
            is MessageContent.AnswerKoBroadcast -> Message(TypeMessage.ANSWER_KO_BROADCAST, content)
            is MessageContent.NextSongSignal -> Message(TypeMessage.NEXT_SONG_SIGNAL, content)
            is MessageContent.NextSong -> Message(TypeMessage.NEXT_SONG, content)
            is MessageContent.NextSongToGamemaster -> Message(TypeMessage.NEXT_SONG_TO_GAMEMASTER, content)
            is MessageContent.PleaseWaitForNextSong -> Message(TypeMessage.PLEASE_WAIT_FOR_NEXT_SONG, content)
            is MessageContent.GameFinishedSignal -> Message(TypeMessage.GAME_FINISHED_SIGNAL, content)
            is MessageContent.GameFinished -> Message(TypeMessage.GAME_FINISHED, content)
            else -> throw NotImplementedError("$this")
        }
    }

    private fun RedisPubSubAsyncCommands<String, String>.publish(channel: String, message: Message) =
        publish(channel, objectMapper.writeValueAsString(message))
}
