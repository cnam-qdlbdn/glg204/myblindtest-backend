package net.lecnam.myblindtest.partie

import net.lecnam.myblindtest.base.HasId
import net.lecnam.myblindtest.partie.participant.Participant
import net.lecnam.myblindtest.partie.resultat.ResultatManche
import net.lecnam.myblindtest.partie.resultat.ResultatMancheDAO
import org.jdbi.v3.core.Handle
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.annotation.JdbiProperty
import org.jdbi.v3.core.kotlin.KotlinMapper
import org.jdbi.v3.core.kotlin.bindKotlin
import org.jdbi.v3.core.kotlin.inTransactionUnchecked
import org.jdbi.v3.core.kotlin.mapTo
import org.jdbi.v3.core.kotlin.withHandleUnchecked
import org.jdbi.v3.core.mapper.reflect.ColumnName
import org.jdbi.v3.core.qualifier.QualifiedType
import org.jdbi.v3.core.statement.Query
import org.jdbi.v3.json.Json
import java.time.OffsetDateTime
import kotlin.jvm.optionals.getOrNull
import kotlin.streams.asSequence

class PartieDAO(
    private val jdbi: Jdbi,
    private val resultatMancheDAO: ResultatMancheDAO
) {
    companion object {
        private const val FIND_PARTIE_BASE = """
        select p.*,
               pa.fk_partie as pa_fk_partie,
               pa.fk_joueur as pa_fk_joueur,
               pa.maitre_joueur as pa_maitre_joueur,
               c.fk_chanson as chanson_id
        from partie p
                 left join participant pa on p.id = pa.fk_partie
                 left join chansons_partie c on p.id = c.fk_partie
        %s
        order by c.sequence"""
    }

    fun insert(partie: CreatePartie) {
        jdbi.inTransactionUnchecked {
            val idPartie: Long = it.createUpdate(
                """insert into partie (code_partie, nombre_manches, check_text, buzzer, mode_blitz, check_all, etat, fk_createur, ref_playlist, nom_playlist) 
                    values (
                        :codePartie, 
                        :nombreManches, 
                        :checkText,
                        :buzzer, 
                        :modeBlitz,
                        :checkAll,
                        :etat, 
                        :refCreateur, 
                        :refPlaylist,
                        :nomPlaylist
                    )"""
            )
                .bindKotlin(partie)
                .executeAndReturnGeneratedKeys("id")
                .mapTo<Long>()
                .one()

            it.updateChansonsInPartie(partie, idPartie, false)
        }
    }

    private fun Handle.updateChansonsInPartie(partie: PartieData, idPartie: Long, cleanBefore: Boolean) {
        if (cleanBefore) {
            createUpdate("delete from chansons_partie where fk_partie = :idPartie")
                .bind("idPartie", idPartie)
                .execute()
        }

        if ("id" in partie.refPlaylist) {
            createUpdate(
                """
                insert into chansons_partie (fk_partie, fk_chanson, sequence)
                select p.id, c.fk_chanson, row_number() over (order by random()) as sequence
                from partie p
                        join playlist pl on pl.id = (p.ref_playlist -> 'id')::bigint
                        join chanson_dans_playlist c on pl.id = c.fk_playlist
                where p.id = :idPartie
                order by random()                
                """
            )
                .bind("idPartie", idPartie)
                .execute()
        } else {
            createUpdate(
                """
                insert into chansons_partie (fk_partie, fk_chanson, sequence)
                select p.id, pl2.id_chanson, row_number() over (order by random()) as sequence
                from partie p
                         join (select pl.id_tag, pl.id_joueur, pl.valeur_tag, unnest(pl.chansons) as id_chanson
                               from dynamic_playlist pl) as pl2
                              on pl2.id_tag = (p.ref_playlist -> 'id_tag')::bigint
                                  and pl2.id_joueur = (p.ref_playlist -> 'id_joueur')::bigint
                                  and (not p.ref_playlist ?? /*requis pour échapper le simple '?', qui est sinon considéré comme un paramètre*/ 'valeur_tag' or pl2.valeur_tag = (p.ref_playlist ->> 'valeur_tag')::text)
                where p.id = :idPartie
                order by random()                        
                """
            )
                .bind("idPartie", idPartie)
                .execute()
        }
    }

    fun getAllCodesPartie(): List<String> {
        return jdbi.withHandleUnchecked {
            it.select("select code_partie from partie")
                .mapTo<String>()
                .list()
        }
    }

    fun getByCodePartie(dto: CodePartieDTO): Partie? {
        return jdbi.withHandleUnchecked {
            it.select(FIND_PARTIE_BASE.format("where code_partie = :codePartie"))
                .bind("codePartie", dto.codePartie)
                .mapToPartie()
                .firstOrNull()
        }
    }

    fun getById(idPartie: Long): Partie? {
        return jdbi.withHandleUnchecked {
            it.select(FIND_PARTIE_BASE.format("where id = :idPartie"))
                .bind("idPartie", idPartie)
                .mapToPartie()
                .firstOrNull()
        }
    }

    fun existsById(idPartie: Long): Boolean {
        return jdbi.withHandleUnchecked {
            it.select("select 1 from partie where id = :idPartie")
                .bind("idPartie", idPartie)
                .mapTo<Int>()
                .findFirst()
                .getOrNull() != null
        }
    }

    fun updateEtat(idPartie: Long, etat: EtatPartie) {
        jdbi.inTransactionUnchecked {
            it.createUpdate("update partie set etat = :etat where id = :idPartie")
                .bind("idPartie", idPartie)
                .bind("etat", etat)
                .execute()
        }
    }

    fun updateOptions(partieUpdateForm: PartieUpdateForm) {
        jdbi.inTransactionUnchecked {
            val refPlaylist = it.select("select ref_playlist from partie where id = :id")
                .bind("id", partieUpdateForm.id)
                .mapTo(QualifiedType.of(Map::class.java as Class<Map<String, *>>).with(Json::class.java))
                .one()

            it.createUpdate(
                """
                update partie 
                set 
                    nombre_manches = :nombreManches,
                    check_text = :checkText,
                    buzzer = :buzzer,
                    mode_blitz = :modeBlitz,
                    check_all = :checkAll,
                    ref_playlist = :refPlaylist,
                    nom_playlist = :nomPlaylist
                where id = :id
                """
            )
                .bindKotlin(partieUpdateForm)
                .execute()

            if (partieUpdateForm.refPlaylist != refPlaylist) { // si ça a changé
                it.updateChansonsInPartie(partieUpdateForm, partieUpdateForm.id, true)
            }
        }
    }

    fun nextManche(idPartie: Long) {
        jdbi.inTransactionUnchecked {
            val updatedRows =
                it.createUpdate("update partie set numero_manche = numero_manche + 1 where id = :idPartie")
                    .bind("idPartie", idPartie)
                    .execute()

            if (updatedRows == 0) {
                throw IllegalArgumentException("Tentative de mise à jour d'une partie n'existant pas")
            }
        }
    }

    private data class PartieWithoutLinkedEntities(
        override val id: Long,
        @ColumnName("fk_createur")
        override val refCreateur: Long,
        override val codePartie: String,
        override val nombreManches: Int,
        override val numeroManche: Int,
        override val checkText: Boolean,
        override val buzzer: Boolean,
        override val modeBlitz: Boolean,
        override val checkAll: Boolean,
        override val etat: EtatPartie,
        @Json
        override val refPlaylist: Map<String, *>,
        override val nomPlaylist: String,
        override val dateCreation: OffsetDateTime
    ) : HasId, PartieDataAndMetadata, PartieAdditionalData {
        @JdbiProperty(map = false)
        override val participants = mutableListOf<Participant>()

        @JdbiProperty(map = false)
        override val resultatsManches = emptyList<ResultatManche>()

        @JdbiProperty(map = false)
        val chansons = mutableListOf<Long>()

        fun toDB(rms: List<ResultatManche>) = Partie(
            id,
            codePartie,
            nombreManches,
            numeroManche,
            checkText,
            buzzer,
            modeBlitz,
            checkAll,
            etat,
            refCreateur,
            refPlaylist,
            nomPlaylist,
            dateCreation,
            participants.sortedBy { it.refJoueur },
            rms,
            chansons
        )
    }

    private fun Query.mapToPartie() = this
        .registerRowMapper(KotlinMapper(Participant::class, "pa"))
        .reduceRows { map: MutableMap<Long, PartieWithoutLinkedEntities>, rowView ->
            val partie = map.computeIfAbsent(rowView.getColumn("id", Long::class.javaObjectType)) {
                rowView.getRow(PartieWithoutLinkedEntities::class.java)
            }

            if (rowView.getColumn("pa_fk_partie", Long::class.javaObjectType) != null) {
                val participant: Participant = rowView.getRow(Participant::class.java)
                if (participant !in partie.participants) {
                    partie.participants += participant
                }
            }

            val chansonId: Long? = rowView.getColumn("chanson_id", Long::class.javaObjectType)
            if (chansonId != null && chansonId !in partie.chansons) {
                partie.chansons += chansonId
            }
        }
        .map { it.toDB(resultatMancheDAO.getAllByPartie(it.id)) }
        .asSequence()
}
