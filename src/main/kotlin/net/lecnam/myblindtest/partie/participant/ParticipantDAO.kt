package net.lecnam.myblindtest.partie.participant

import org.jdbi.v3.sqlobject.statement.SqlUpdate
import org.jdbi.v3.sqlobject.transaction.Transaction

interface ParticipantDAO {
    @Transaction
    @SqlUpdate("insert into participant (fk_joueur, fk_partie, maitre_joueur) values (:idJoueur, :idPartie, :maitreJoueur)")
    fun addParticipant(idPartie: Long, idJoueur: Long, maitreJoueur: Boolean)

    @Transaction
    @SqlUpdate("delete from participant where fk_partie = :idPartie and fk_joueur = :idJoueur")
    fun removeParticipant(idPartie: Long, idJoueur: Long)

    @Transaction
    @SqlUpdate("update participant set connected = :connected where fk_partie = :idPartie and fk_joueur = :idJoueur")
    fun updateConnectionStatus(idPartie: Long, idJoueur: Long, connected: Boolean)
}
