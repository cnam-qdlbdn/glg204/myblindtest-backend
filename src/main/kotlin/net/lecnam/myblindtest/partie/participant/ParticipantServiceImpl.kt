package net.lecnam.myblindtest.partie.participant

import arrow.core.Either
import arrow.core.left
import arrow.core.right
import net.lecnam.myblindtest.exceptions.EntityNotFound
import net.lecnam.myblindtest.partie.PartieDAO

class ParticipantServiceImpl(
    private val partieDAO: PartieDAO,
    private val participantDAO: ParticipantDAO
) : ParticipantService {
    override fun addParticipant(
        idPartie: Long,
        idJoueur: Long,
        maitreJoueur: Boolean
    ): Either<EntityNotFound.Partie, Unit> {
        if (!partieDAO.existsById(idPartie)) {
            return EntityNotFound.Partie.byId(idPartie).left()
        }

        participantDAO.addParticipant(idPartie, idJoueur, maitreJoueur)
        return Unit.right()
    }

    override fun removeParticipant(
        idPartie: Long,
        idJoueur: Long
    ): Either<EntityNotFound.Partie, Unit> {
        if (!partieDAO.existsById(idPartie)) {
            return EntityNotFound.Partie.byId(idPartie).left()
        }

        participantDAO.removeParticipant(idPartie, idJoueur)
        return Unit.right()
    }

    override fun connectParticipant(idPartie: Long, idJoueur: Long): Either<EntityNotFound.Partie, Unit> {
        return updateConnectionStatus(idPartie, idJoueur, true)
    }

    override fun disconnectParticipant(idPartie: Long, idJoueur: Long): Either<EntityNotFound.Partie, Unit> {
        return updateConnectionStatus(idPartie, idJoueur, false)
    }

    private fun updateConnectionStatus(
        idPartie: Long,
        idJoueur: Long,
        status: Boolean
    ): Either<EntityNotFound.Partie, Unit> {
        if (!partieDAO.existsById(idPartie)) {
            return EntityNotFound.Partie.byId(idPartie).left()
        }

        participantDAO.updateConnectionStatus(idPartie, idJoueur, status)
        return Unit.right()
    }
}
