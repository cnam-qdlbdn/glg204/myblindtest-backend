package net.lecnam.myblindtest.partie.participant

import arrow.core.Either
import net.lecnam.myblindtest.exceptions.EntityNotFound

interface ParticipantService {
    fun addParticipant(
        idPartie: Long,
        idJoueur: Long,
        maitreJoueur: Boolean = false
    ): Either<EntityNotFound.Partie, Unit>

    fun removeParticipant(idPartie: Long, idJoueur: Long): Either<EntityNotFound.Partie, Unit>

    fun connectParticipant(idPartie: Long, idJoueur: Long): Either<EntityNotFound.Partie, Unit>

    fun disconnectParticipant(idPartie: Long, idJoueur: Long): Either<EntityNotFound.Partie, Unit>
}
