package net.lecnam.myblindtest.partie.participant

import net.lecnam.myblindtest.base.HasRefJoueur
import org.jdbi.v3.core.mapper.reflect.ColumnName

interface ParticipantData : HasRefJoueur {
    val maitreJoueur: Boolean
    val connected: Boolean
}

data class Participant(
    @ColumnName("fk_joueur")
    override val refJoueur: Long,
    @ColumnName("fk_partie")
    val refPartie: Long,
    override val maitreJoueur: Boolean = false,
    override val connected: Boolean = true
) : ParticipantData

data class ParticipantDTO(
    override val refJoueur: Long,
    override val maitreJoueur: Boolean,
    override val connected: Boolean
) : ParticipantData

fun Participant.toDTO() = ParticipantDTO(
    refJoueur,
    maitreJoueur,
    connected
)
