package net.lecnam.myblindtest.partie.historique

import net.lecnam.myblindtest.base.HasId
import net.lecnam.myblindtest.base.HasRefJoueur
import org.jdbi.v3.core.mapper.reflect.ColumnName

interface HistoriquePartieData : HasRefJoueur {
    val nomPlaylist: String
    val score: Int
    val maitreJoueur: Boolean
}

data class CreateHistoriquePartie(
    override val refJoueur: Long,
    override val nomPlaylist: String,
    override val score: Int,
    override val maitreJoueur: Boolean = false
) : HistoriquePartieData

data class HistoriquePartie(
    override val id: Long,
    @ColumnName("fk_joueur")
    override val refJoueur: Long,
    override val nomPlaylist: String,
    override val score: Int,
    override val maitreJoueur: Boolean
) : HasId, HistoriquePartieData

data class HistoriquePartieDTO(
    override val refJoueur: Long,
    override val nomPlaylist: String,
    override val score: Int,
    override val maitreJoueur: Boolean
) : HistoriquePartieData

fun HistoriquePartie.toDTO() = HistoriquePartieDTO(
    refJoueur,
    nomPlaylist,
    score,
    maitreJoueur
)
