package net.lecnam.myblindtest.partie.historique

import arrow.core.Either
import net.lecnam.myblindtest.exceptions.EntityNotFound

interface HistoriquePartieService {
    fun createHistoriquesFromPartie(idPartie: Long): Either<EntityNotFound.Partie, List<HistoriquePartieDTO>>

    fun getAllForJoueur(idJoueur: Long): List<HistoriquePartieDTO>
}
