package net.lecnam.myblindtest.partie.historique

import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys
import org.jdbi.v3.sqlobject.statement.SqlQuery
import org.jdbi.v3.sqlobject.statement.SqlUpdate
import org.jdbi.v3.sqlobject.transaction.Transaction

interface HistoriquePartieDAO {
    @Transaction
    @SqlUpdate(
        """
        insert into historique_partie (nom_playlist, score, maitre_joueur, fk_joueur)
        values (
            :historiquePartie.nomPlaylist,
            :historiquePartie.score,
            :historiquePartie.maitreJoueur,
            :historiquePartie.refJoueur
        )
        """
    )
    @GetGeneratedKeys
    fun insert(historiquePartie: CreateHistoriquePartie): HistoriquePartie

    @SqlQuery("select * from historique_partie where fk_joueur = :idJoueur")
    fun getAllByJoueur(idJoueur: Long): List<HistoriquePartie>
}
