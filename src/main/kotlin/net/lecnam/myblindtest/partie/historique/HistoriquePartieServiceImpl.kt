package net.lecnam.myblindtest.partie.historique

import arrow.core.Either
import net.lecnam.myblindtest.exceptions.EntityNotFound
import net.lecnam.myblindtest.partie.PartieService

class HistoriquePartieServiceImpl(
    private val partieService: PartieService,
    private val historiquePartieDAO: HistoriquePartieDAO
) : HistoriquePartieService {
    override fun createHistoriquesFromPartie(idPartie: Long): Either<EntityNotFound.Partie, List<HistoriquePartieDTO>> {
        return partieService.findById(idPartie)
            .map { partie ->
                val resultatsManches = partie.resultatsManches
                val participants = partie.participants

                participants
                    .map { participant ->
                        CreateHistoriquePartie(
                            participant.refJoueur,
                            partie.nomPlaylist,
                            resultatsManches.filter { it.refJoueur == participant.refJoueur }.sumOf { it.score },
                            participant.maitreJoueur
                        )
                    }
                    .map { historiquePartieDAO.insert(it).toDTO() }
            }
    }

    // TODO problème de dépendance cyclique sinon (si on fait la vérification de l'id du joueur) ? ou alors il faudrait
    //  modifier le JoueurDAO pour que le joueur contienne directement ses historiques de partie
    override fun getAllForJoueur(idJoueur: Long) =
        historiquePartieDAO.getAllByJoueur(idJoueur).map { it.toDTO() }
}
