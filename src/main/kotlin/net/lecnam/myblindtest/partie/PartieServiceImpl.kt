package net.lecnam.myblindtest.partie

import arrow.core.Either
import arrow.core.flatMap
import arrow.core.left
import arrow.core.right
import com.athaydes.kunion.Union
import com.athaydes.kunion.Union.U2.Companion.toU2A
import com.athaydes.kunion.Union.U2.Companion.toU2B
import com.athaydes.kunion.Union.U4.Companion.toU4A
import com.athaydes.kunion.Union.U4.Companion.toU4B
import com.athaydes.kunion.Union.U4.Companion.toU4C
import com.athaydes.kunion.Union.U4.Companion.toU4D
import net.lecnam.myblindtest.exceptions.EntityNotFound
import net.lecnam.myblindtest.exceptions.EntityNotFoundForJoueur
import net.lecnam.myblindtest.exceptions.InvalidOptionsPartie
import net.lecnam.myblindtest.exceptions.InvalidPartieState
import net.lecnam.myblindtest.exceptions.NoPlaylistForJoueur
import net.lecnam.myblindtest.librairie.playlist.PlaylistService
import net.lecnam.myblindtest.librairie.playlist.dynamic.DynamicPlaylistService
import net.lecnam.myblindtest.librairie.tag.TagService

class PartieServiceImpl(
    private val partieDAO: PartieDAO,
    private val playlistService: PlaylistService,
    private val dynamicPlaylistService: DynamicPlaylistService,
    private val tagService: TagService
) : PartieService {
    companion object {
        private val CHARACTER_POOL = listOf(
            '0' to '9',
            'A' to 'Z'
        ).joinToString(separator = "") { (it.first..it.second).joinToString(separator = "") }

        private const val MIN_CODE_LENGTH = 6

        private const val DEFAULT_ROUNDS = 10
    }

    override fun create(idJoueur: Long): Either<NoPlaylistForJoueur, CodePartieDTO> {
        val codesPartie = partieDAO.getAllCodesPartie()

        var codePartie: String
        do {
            codePartie = (1..MIN_CODE_LENGTH)
                .joinToString(separator = "") { CHARACTER_POOL.random().toString() }
        } while (codePartie in codesPartie)

        val refPlaylistWithNom = playlistService.findAll(idJoueur).asSequence()
            .filter { it.chansons.isNotEmpty() }
            .map { mapOf("id" to it.id) to it.nom }
            .firstOrNull()
            ?: dynamicPlaylistService.findAll(idJoueur).asSequence()
                .filter { it.chansons.isNotEmpty() }
                .map {
                    mapOf(
                        "id_tag" to it.idTag,
                        "id_joueur" to idJoueur,
                        "valeur_tag" to it.valeurTag
                    ).filterValues { v -> v != null } to "${
                    tagService.findById(it.idTag, idJoueur).fold({ null }, { t -> t })!!.nom
                    } ${it.valeurTag ?: ""}".trim()
                }
                .firstOrNull()
            ?: (emptyMap<String, Any>() to "")

        /*
         * Les informations implicites d'une partie :
         * - s'il y a un buzzer, la vérification est forcément manuelle
         * - si la vérification est manuelle, on ne vérifie pas tous les "champs" possibles à vérifier (c'est soit oui soit non)
         * - le mode blitz n'est possible que si la vérification n'est pas manuelle (tout le monde joue en même temps, ou quelqu'un valide une réponse qui termine la manche)
         */
        val partie = CreatePartie(
            refCreateur = idJoueur,
            codePartie = codePartie,
            nombreManches = DEFAULT_ROUNDS,
            checkText = false,
            buzzer = true,
            modeBlitz = true,
            checkAll = false,
            etat = EtatPartie.CREATED,
            refPlaylist = refPlaylistWithNom.first,
            nomPlaylist = refPlaylistWithNom.second
        )

        if (partie.refPlaylist.isEmpty()) {
            return NoPlaylistForJoueur(idJoueur).left()
        }

        partieDAO.insert(partie)

        return codePartie.toCodePartie().right()
    }

    override fun getByCodePartie(codePartie: CodePartieDTO): Either<EntityNotFound.Partie, Partie> {
        return partieDAO.getByCodePartie(codePartie)?.right()
            ?.flatMap { partie ->
                if (partie.etat == EtatPartie.FINISHED) {
                    EntityNotFound.Partie.byCodePartie(codePartie.codePartie).left()
                } else partie.right()
            }
            ?: EntityNotFound.Partie.byCodePartie(codePartie.codePartie).left()
    }

    override fun findById(idPartie: Long): Either<EntityNotFound.Partie, PartieDTO> {
        return partieDAO.getById(idPartie)?.toDTO()?.right() ?: EntityNotFound.Partie.byId(idPartie).left()
    }

    override fun nextEtat(idPartie: Long): Either<Union.U2<EntityNotFound.Partie, InvalidPartieState>, Unit> {
        return findById(idPartie)
            .mapLeft { it.toU2A() }
            .flatMap { partie ->
                when (partie.etat) {
                    EtatPartie.CREATED -> EtatPartie.OPENED.right()
                    EtatPartie.OPENED -> EtatPartie.STARTED.right()
                    EtatPartie.STARTED -> EtatPartie.FINISHED.right()
                    else -> InvalidPartieState.forEtat(idPartie).toU2B().left()
                }
            }
            .map { partieDAO.updateEtat(idPartie, it) }
    }

    override fun finish(idPartie: Long): Either<EntityNotFound.Partie, Unit> {
        if (!partieDAO.existsById(idPartie)) {
            return EntityNotFound.Partie.byId(idPartie).left()
        }

        return partieDAO.updateEtat(idPartie, EtatPartie.FINISHED).right()
    }

    override fun updateOptions(
        idPartie: Long,
        updatePartie: UpdatePartieDTO
    ): Either<Union.U4<EntityNotFound.Partie, EntityNotFound.Playlist, EntityNotFoundForJoueur.DynamicPlaylist, InvalidOptionsPartie>, Unit> {
        return findById(idPartie)
            .mapLeft { it.toU4A() }
            .flatMap { partie ->
                var maxManches = 1

                val refPlaylist = updatePartie.refPlaylist ?: partie.refPlaylist
                check(
                    refPlaylist.isEmpty() ||
                        refPlaylist.keys == setOf("id") ||
                        refPlaylist.keys == setOf("id_tag", "id_joueur") ||
                        refPlaylist.keys == setOf("id_tag", "id_joueur", "valeur_tag")
                ) { "Le format de l'identifiant de playlist n'est pas valide" }

                if ("id" in refPlaylist) {
                    val playlist = playlistService.findById(refPlaylist.getValue("id").toString().toLong())
                    if (playlist is Either.Left) {
                        return playlist.value.toU4B().left()
                    }

                    maxManches = (playlist as Either.Right).value.chansons.size
                }

                if ("id_tag" in refPlaylist) {
                    val dynamicPlaylist = dynamicPlaylistService.findBy(
                        refPlaylist.getValue("id_tag").toString().toLong(),
                        refPlaylist.getValue("id_joueur").toString().toLong(),
                        refPlaylist["valeur_tag"]?.toString()
                    )

                    if (dynamicPlaylist is Either.Left) {
                        return dynamicPlaylist.value.use(
                            { null }, // on va considérer que le client fait un input valide de playlist dynamique
                            { it }
                        )!!
                            .toU4C()
                            .left()
                    }

                    maxManches = (dynamicPlaylist as Either.Right).value.chansons.size
                }

                val partieUpdateForm = PartieUpdateForm(
                    idPartie,
                    nombreManches = (updatePartie.nombreManches ?: partie.nombreManches).coerceIn(1..maxManches),
                    checkText = updatePartie.checkText ?: partie.checkText,
                    buzzer = updatePartie.buzzer ?: partie.buzzer,
                    checkAll = updatePartie.checkAll ?: partie.checkAll,
                    modeBlitz = updatePartie.modeBlitz ?: partie.modeBlitz,
                    refPlaylist = updatePartie.refPlaylist ?: partie.refPlaylist,
                    nomPlaylist = updatePartie.refPlaylist?.let {
                        if ("id" in it) {
                            playlistService
                                .findById(it.getValue("id").toString().toLong())
                                .fold({ null }, { p -> p })!!
                                .nom
                        } else {
                            val nomTag = tagService.findById(
                                it.getValue("id_tag")
                                    .toString()
                                    .toLong(),
                                partie.refCreateur
                            )
                                .fold({ null }, { t -> t })!!
                                .nom
                            "$nomTag ${it["valeur_tag"] ?: ""}".trim()
                        }
                    } ?: partie.nomPlaylist
                )

                validatePartieUpdateForm(partieUpdateForm, partie.codePartie)
                    .mapLeft { it.toU4D() }
                    .map { partieDAO.updateOptions(partieUpdateForm) }
            }
    }

    private fun validatePartieUpdateForm(
        puf: PartieUpdateForm,
        codePartie: String
    ): Either<InvalidOptionsPartie, Unit> {
        with(puf) {
            val validationResult = listOf(
                checkText || buzzer, // on doit vérifier le texte, ou avoir un buzzer (ou les deux)
                !buzzer || !checkAll, // s'il y a un buzzer, on ne doit pas "tout vérifier"
                !checkAll || (!buzzer && checkText) // si on vérifie tout, il ne peut pas y avoir de buzzer, donc on ne peut vérifier que par du texte
            )
                .all { it }

            if (!validationResult) {
                return InvalidOptionsPartie(codePartie).left()
            }
            return Unit.right()
        }
    }

    override fun nextManche(idPartie: Long): Either<Union.U2<EntityNotFound.Partie, InvalidPartieState>, Unit> {
        return findById(idPartie)
            .mapLeft { it.toU2A() }
            .flatMap { partie ->
                if (partie.numeroManche == partie.nombreManches) {
                    InvalidPartieState.forNumeroManche(partie.id).toU2B().left()
                } else {
                    partieDAO.nextManche(idPartie).right()
                }
            }
    }
}
