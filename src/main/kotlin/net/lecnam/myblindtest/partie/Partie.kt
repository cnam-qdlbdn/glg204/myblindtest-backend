package net.lecnam.myblindtest.partie

import net.lecnam.myblindtest.base.HasId
import net.lecnam.myblindtest.partie.participant.Participant
import net.lecnam.myblindtest.partie.participant.ParticipantDTO
import net.lecnam.myblindtest.partie.participant.ParticipantData
import net.lecnam.myblindtest.partie.participant.toDTO
import net.lecnam.myblindtest.partie.resultat.ResultatManche
import net.lecnam.myblindtest.partie.resultat.ResultatMancheDTO
import net.lecnam.myblindtest.partie.resultat.ResultatMancheData
import net.lecnam.myblindtest.partie.resultat.toDTO
import org.jdbi.v3.core.mapper.reflect.ColumnName
import org.jdbi.v3.json.Json
import java.time.OffsetDateTime

interface PartieData {
    val nombreManches: Int
    val checkText: Boolean
    val buzzer: Boolean
    val modeBlitz: Boolean
    val checkAll: Boolean
    val refPlaylist: Map<String, *> // DynamicPlaylistUniqueIdentifier | id: Long
    val nomPlaylist: String
}

interface PartieMetadata {
    val codePartie: String
    val numeroManche: Int
    val etat: EtatPartie
    val refCreateur: Long
}

interface PartieDataAndMetadata : PartieData, PartieMetadata

interface PartieAdditionalData {
    val dateCreation: OffsetDateTime

    val participants: List<ParticipantData>
    val resultatsManches: List<ResultatMancheData>
}

enum class EtatPartie {
    CREATED, // état avant la première connexion du joueur au "salon" de la partie
    OPENED, // état une fois la première connexion effectuée, et le "salon" ouvert
    STARTED,
    FINISHED
}

data class CreatePartie(
    override val refCreateur: Long,
    override val codePartie: String,
    override val nombreManches: Int,
    override val checkText: Boolean,
    override val buzzer: Boolean,
    override val modeBlitz: Boolean,
    override val checkAll: Boolean,
    override val etat: EtatPartie,
    @Json
    override val refPlaylist: Map<String, *>,
    override val nomPlaylist: String
) : PartieDataAndMetadata {
    override val numeroManche = 1
}

data class UpdatePartieDTO(
    val nombreManches: Int? = null,
    val checkText: Boolean? = null,
    val buzzer: Boolean? = null,
    val modeBlitz: Boolean? = null,
    val checkAll: Boolean? = null,
    val refPlaylist: Map<String, *>? = null
)

data class PartieUpdateForm(
    override val id: Long,
    override val nombreManches: Int,
    override val checkText: Boolean,
    override val buzzer: Boolean,
    override val modeBlitz: Boolean,
    override val checkAll: Boolean,
    @Json
    override val refPlaylist: Map<String, *>,
    override val nomPlaylist: String
) : HasId, PartieData

data class Partie(
    override val id: Long,
    override val codePartie: String,
    override val nombreManches: Int,
    override val numeroManche: Int,
    override val checkText: Boolean,
    override val buzzer: Boolean,
    override val modeBlitz: Boolean,
    override val checkAll: Boolean,
    override val etat: EtatPartie,
    @ColumnName("fk_createur")
    override val refCreateur: Long,
    @Json
    override val refPlaylist: Map<String, *>,
    override val nomPlaylist: String,
    override val dateCreation: OffsetDateTime,
    override val participants: List<Participant>,
    override val resultatsManches: List<ResultatManche>,
    val chansons: List<Long> // NE DOIS JAMAIS ÊTRE EXPOSÉ
) : HasId, PartieDataAndMetadata, PartieAdditionalData

data class PartieDTO(
    override val id: Long,
    override val codePartie: String,
    override val nombreManches: Int,
    override val numeroManche: Int,
    override val checkText: Boolean,
    override val buzzer: Boolean,
    override val modeBlitz: Boolean,
    override val checkAll: Boolean,
    override val etat: EtatPartie,
    override val refCreateur: Long,
    override val refPlaylist: Map<String, *>,
    override val nomPlaylist: String,
    override val dateCreation: OffsetDateTime,
    override val participants: List<ParticipantDTO>,
    override val resultatsManches: List<ResultatMancheDTO>
) : HasId, PartieDataAndMetadata, PartieAdditionalData

data class CodePartieDTO(val codePartie: String)

fun String.toCodePartie() = CodePartieDTO(this)

fun Partie.toDTO() = PartieDTO(
    id,
    codePartie,
    nombreManches,
    numeroManche,
    checkText,
    buzzer,
    modeBlitz,
    checkAll,
    etat,
    refCreateur,
    refPlaylist,
    nomPlaylist,
    dateCreation,
    participants.map { it.toDTO() },
    resultatsManches.map { it.toDTO() }
)
