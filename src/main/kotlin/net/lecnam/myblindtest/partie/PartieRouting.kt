package net.lecnam.myblindtest.partie

import io.ktor.http.HttpStatusCode
import io.ktor.server.application.*
import io.ktor.server.auth.authenticate
import io.ktor.server.response.respond
import io.ktor.server.routing.Routing
import io.ktor.server.routing.get
import io.ktor.server.routing.post
import io.ktor.server.routing.route
import io.ktor.server.websocket.webSocket
import net.lecnam.myblindtest.exceptions.toErrorMessageContainer
import net.lecnam.myblindtest.ext.badRequest
import net.lecnam.myblindtest.ext.getPrincipalAuth
import net.lecnam.myblindtest.plugins.SESSION_KEY
import org.kodein.di.instance
import org.kodein.di.ktor.closestDI

fun Routing.partieRouting() {
    authenticate(SESSION_KEY) {
        route("/partie") {
            post {
                val partieService: PartieService by closestDI().instance()
                val auth = getPrincipalAuth()

                partieService.create(auth.joueur.id)
                    .fold(
                        { badRequest(it.message.toErrorMessageContainer()) },
                        { call.respond(HttpStatusCode.Created, it) }
                    )
            }

            get("/{codePartie}") {
                val codePartie = call.parameters["codePartie"]!!.toCodePartie()
                val partieService: PartieService by closestDI().instance()

                partieService.getByCodePartie(codePartie)
                    .fold(
                        { badRequest(it.message.toErrorMessageContainer()) },
                        { call.respond(it.toDTO()) }
                    )
            }

            webSocket {
                val partieManager by closestDI().instance<PartieManager>()

                partieManager.handle(this)
            }
        }
    }
}
