package net.lecnam.myblindtest.partie

import arrow.core.Either
import com.athaydes.kunion.Union
import net.lecnam.myblindtest.exceptions.EntityNotFound
import net.lecnam.myblindtest.exceptions.EntityNotFoundForJoueur
import net.lecnam.myblindtest.exceptions.InvalidOptionsPartie
import net.lecnam.myblindtest.exceptions.InvalidPartieState
import net.lecnam.myblindtest.exceptions.NoPlaylistForJoueur

interface PartieService {
    fun create(idJoueur: Long): Either<NoPlaylistForJoueur, CodePartieDTO>

    fun getByCodePartie(codePartie: CodePartieDTO): Either<EntityNotFound.Partie, Partie>

    fun findById(idPartie: Long): Either<EntityNotFound.Partie, PartieDTO>

    fun nextEtat(idPartie: Long): Either<Union.U2<EntityNotFound.Partie, InvalidPartieState>, Unit>

    fun finish(idPartie: Long): Either<EntityNotFound.Partie, Unit>

    fun updateOptions(
        idPartie: Long,
        updatePartie: UpdatePartieDTO
    ): Either<Union.U4<EntityNotFound.Partie, EntityNotFound.Playlist, EntityNotFoundForJoueur.DynamicPlaylist, InvalidOptionsPartie>, Unit>

    fun nextManche(idPartie: Long): Either<Union.U2<EntityNotFound.Partie, InvalidPartieState>, Unit>
}
