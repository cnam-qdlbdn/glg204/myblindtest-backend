package net.lecnam.myblindtest.api.deezer

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class Track(
    val id: Long,
    val readable: Boolean,
    val title: String,
    val preview: String,
    val artist: Artist
)
