package net.lecnam.myblindtest.api.deezer

import arrow.core.Either
import arrow.core.flatMap
import arrow.core.left
import arrow.core.right
import com.athaydes.kunion.Union
import com.athaydes.kunion.Union.U2.Companion.toU2A
import com.athaydes.kunion.Union.U2.Companion.toU2B
import com.fasterxml.jackson.databind.ObjectMapper
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.head
import io.ktor.client.statement.request
import io.ktor.http.HttpStatusCode
import net.lecnam.myblindtest.exceptions.InvalidChansonUrl
import net.lecnam.myblindtest.exceptions.InvalidPlaylistUrl
import net.lecnam.myblindtest.exceptions.NoDeezerPreview
import net.lecnam.myblindtest.ext.isValidUrl

class DeezerServiceImpl(
    private val httpClient: HttpClient,
    private val objectMapper: ObjectMapper
) : DeezerService {

    override suspend fun getDeezerTrackFromUrl(url: String): Either<Union.U2<InvalidChansonUrl, NoDeezerPreview>, Track> {
        /*
         * cas possibles :
         * - url de partage : https://deezer.page.link/r9jbqhWPxFMsrRB97
         * - url directe : https://www.deezer.com/fr/track/73001983 ou https://www.deezer.com/track/73001983
         * - url d'api : https://api.deezer.com/track/73001983
         */
        if (!url.isValidUrl()) {
            return InvalidChansonUrl("L'url est invalide").toU2A().left()
        }

        var trackUrl = url
        var trackId: Long? = null

        if (trackUrl.matches("https://deezer.page.link/\\w+".toRegex())) {
            val response = httpClient.head(url)
            if (response.status != HttpStatusCode.Found) {
                return InvalidChansonUrl("Problème lors de la redirection du lien Deezer : $response")
                    .toU2A()
                    .left()
            }

            trackUrl = response.request.url.toString() // follow redirect
        }

        val directTrackUrlRegex = "^https://www\\.deezer\\.com(?:/\\w+)?/track/(\\d+)(?:.+)?$".toRegex()
        if (trackUrl.matches(directTrackUrlRegex)) {
            trackId = directTrackUrlRegex
                .find(trackUrl)!!
                .groups[1]!!
                .value.toLong()
        }

        val apiUrlRegex = "^https://api\\.deezer\\.com/track/(\\d+)$".toRegex()
        if (trackUrl.matches(apiUrlRegex)) {
            trackId = apiUrlRegex
                .find(trackUrl)!!
                .groups[1]!!
                .value.toLong()
        }

        if (trackId == null) {
            return InvalidChansonUrl("Impossible de détecter l'identifiant de la chanson à partir de l'url $url")
                .toU2A()
                .left()
        }

        return httpClient.get("https://api.deezer.com/track/$trackId")
            .body<Map<String, Any>>()
            .let {
                if ("error" in it) objectMapper.convertValue(
                    it,
                    DeezerError::class.java
                ).left()
                else objectMapper.convertValue(it, Track::class.java).right()
            }
            .mapLeft { InvalidChansonUrl("Erreur de l'appel à l'API Deezer : ${it.error.message}").toU2A() }
            .flatMap {
                if (it.preview.isBlank()) {
                    NoDeezerPreview("Aucun extrait disponible pour la chanson \"${it.title}\" (${it.id})")
                        .toU2B()
                        .left()
                } else {
                    it.right()
                }
            }
    }

    override suspend fun getDeezerPlaylistFromUrl(url: String): Either<InvalidPlaylistUrl, DeezerPlaylist> {
        /*
         * cas possibles :
         * - url de partage : https://deezer.page.link/5BSPNm3gLnhodPRb9
         * - url directe : https://www.deezer.com/fr/playlist/7089916404 ou https://www.deezer.com/playlist/7089916404
         * - url d'api : https://api.deezer.com/playlist/7089916404
         */
        if (!url.isValidUrl()) {
            return InvalidPlaylistUrl("L'url est invalide").left()
        }

        var playlistUrl = url
        var playlistId: Long? = null

        if (playlistUrl.matches("https://deezer.page.link/\\w+".toRegex())) {
            val response = httpClient.head(url)
            if (response.status != HttpStatusCode.Found) {
                return InvalidPlaylistUrl("Problème lors de la redirection du lien Deezer : $response").left()
            }

            playlistUrl = response.request.url.toString() // follow redirect
        }

        val directPlaylistUrlRegex = "^https://www\\.deezer\\.com(?:/\\w+)?/playlist/(\\d+)(?:.+)?$".toRegex()
        if (playlistUrl.matches(directPlaylistUrlRegex)) {
            playlistId = directPlaylistUrlRegex
                .find(playlistUrl)!!
                .groups[1]!!
                .value.toLong()
        }

        val apiUrlRegex = "^https://api\\.deezer\\.com/playlist/(\\d+)$".toRegex()
        if (playlistUrl.matches(apiUrlRegex)) {
            playlistId = apiUrlRegex
                .find(playlistUrl)!!
                .groups[1]!!
                .value.toLong()
        }

        if (playlistId == null) {
            return InvalidPlaylistUrl("Impossible de détecter l'identifiant de la playlist à partir de l'url $url").left()
        }

        return httpClient.get("https://api.deezer.com/playlist/$playlistId")
            .body<Map<String, Any>>()
            .let {
                if ("error" in it) objectMapper.convertValue(
                    it,
                    DeezerError::class.java
                ).left()
                else objectMapper.convertValue(it, DeezerPlaylist::class.java)
                    .right()
            }
            .mapLeft { InvalidPlaylistUrl("Erreur de l'appel à l'API Deezer : ${it.error.message}") }
    }
}
