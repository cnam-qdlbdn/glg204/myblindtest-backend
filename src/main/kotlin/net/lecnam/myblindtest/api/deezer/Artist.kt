package net.lecnam.myblindtest.api.deezer

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class Artist(
    val name: String,
    val picture_xl: String
)
