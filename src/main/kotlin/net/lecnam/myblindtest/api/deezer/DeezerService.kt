package net.lecnam.myblindtest.api.deezer

import arrow.core.Either
import com.athaydes.kunion.Union
import net.lecnam.myblindtest.exceptions.InvalidChansonUrl
import net.lecnam.myblindtest.exceptions.InvalidPlaylistUrl
import net.lecnam.myblindtest.exceptions.NoDeezerPreview

interface DeezerService {
    suspend fun getDeezerTrackFromUrl(url: String): Either<Union.U2<InvalidChansonUrl, NoDeezerPreview>, Track>

    suspend fun getDeezerPlaylistFromUrl(url: String): Either<InvalidPlaylistUrl, DeezerPlaylist>
}
