package net.lecnam.myblindtest.api.deezer

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class DeezerError(
    val error: ErrorDescription
) {
    @JsonIgnoreProperties(ignoreUnknown = true)
    data class ErrorDescription(
        val type: String,
        val message: String,
        val code: Int
    )
}
