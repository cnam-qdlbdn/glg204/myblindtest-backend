package net.lecnam.myblindtest.api.deezer

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class DeezerPlaylist(
    val id: Long,
    val title: String,
    val tracks: Tracks
) {
    @JsonIgnoreProperties(ignoreUnknown = true)
    data class Tracks(
        val data: List<PlaylistTrack>
    )

    @JsonIgnoreProperties(ignoreUnknown = true)
    data class PlaylistTrack(
        val link: String
    )
}
