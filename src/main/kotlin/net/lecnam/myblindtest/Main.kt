package net.lecnam.myblindtest

import io.github.oshai.KotlinLogging
import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.plugins.HttpRequestRetry
import io.ktor.client.plugins.HttpRequestTimeoutException
import io.ktor.client.plugins.HttpTimeout
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.serialization.jackson.jackson
import io.ktor.server.application.*
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import net.lecnam.myblindtest.plugins.configureCORS
import net.lecnam.myblindtest.plugins.configureDI
import net.lecnam.myblindtest.plugins.configureHTTP
import net.lecnam.myblindtest.plugins.configureRouting
import net.lecnam.myblindtest.plugins.configureSecurity
import net.lecnam.myblindtest.plugins.configureSerialization
import net.lecnam.myblindtest.plugins.configureSockets
import net.lecnam.myblindtest.plugins.migrateDatabase

private val logger = KotlinLogging.logger {}

private val httpClient = HttpClient(CIO) {
    install(HttpTimeout) {
        requestTimeoutMillis = 40000 // Deezer met parfois jusqu'à 30 secondes à répondre
    }
    install(ContentNegotiation) {
        jackson()
    }
    install(HttpRequestRetry) {
        maxRetries = 5
        exponentialDelay()
        retryOnExceptionIf { _, cause ->
            cause is HttpRequestTimeoutException
        }
    }
}

fun main() {
    val configuration = getConfiguration()
    logger.debug { configuration }

    // TOUJOURS la première chose à faire !
    migrateDatabase(configuration)

    val module: Application.() -> Unit = {
        module(httpClient)
    }
    embeddedServer(Netty, port = configuration.ktor.server.port, module = module)
        .start(wait = true)
}

fun Application.module(httpClient: HttpClient = net.lecnam.myblindtest.httpClient, eagerDI: Boolean = true) {
    configureDI(httpClient, eagerDI)

    configureSecurity()
    configureHTTP()
    configureCORS()
    configureSerialization()
    configureSockets()
    configureRouting()
}
