package net.lecnam.myblindtest.joueur

import org.jdbi.v3.sqlobject.statement.SqlQuery

interface JoueurDAO {
    @SqlQuery(
        """
        select j.*, p2.code_partie
        from joueur j
                 left join (select p.fk_joueur, pa.code_partie
                            from participant p
                                     join partie pa on p.fk_partie = pa.id and pa.etat <> 'FINISHED') as p2 on p2.fk_joueur = j.id
        where j.id = :id
        """
    )
    fun findById(id: Long): Joueur?
}
