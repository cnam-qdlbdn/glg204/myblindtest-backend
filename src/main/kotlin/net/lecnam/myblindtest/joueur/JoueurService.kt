package net.lecnam.myblindtest.joueur

interface JoueurService {
    fun findById(id: Long): JoueurDTO?
}
