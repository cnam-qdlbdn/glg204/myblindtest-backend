package net.lecnam.myblindtest.joueur

import io.github.oshai.KotlinLogging
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.*
import io.ktor.server.auth.authenticate
import io.ktor.server.response.respond
import io.ktor.server.routing.Routing
import io.ktor.server.routing.get
import io.ktor.server.routing.route
import net.lecnam.myblindtest.exceptions.toErrorMessageContainer
import net.lecnam.myblindtest.ext.getPrincipalAuth
import net.lecnam.myblindtest.plugins.SESSION_KEY
import org.kodein.di.instance
import org.kodein.di.ktor.closestDI

private val logger = KotlinLogging.logger {}

fun Routing.joueurRouting() {
    authenticate(SESSION_KEY) {
        route("/joueur") {
            get {
                val auth = getPrincipalAuth()
                val joueur = auth.joueur
                logger.info { "[${joueur.login}] Who am i ?" }

                call.respond(joueur)
            }

            // Tout le monde peut consulter le profil des autres (si on est connecté)
            get("/{id}") {
                val joueurId = call.parameters["id"]!!.toLong()
                val joueurService by closestDI().instance<JoueurService>()

                joueurService.findById(joueurId)
                    ?.also { call.respond(it) }
                    ?: call.respond(
                        HttpStatusCode.NotFound,
                        "Le joueur avec l'id $joueurId n'existe pas".toErrorMessageContainer()
                    )
            }
        }
    }
}
