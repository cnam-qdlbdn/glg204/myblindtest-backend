package net.lecnam.myblindtest.joueur

import net.lecnam.myblindtest.partie.historique.HistoriquePartieService

class JoueurServiceImpl(
    private val historiquePartieService: HistoriquePartieService,
    private val joueurDAO: JoueurDAO
) : JoueurService {
    override fun findById(id: Long) = joueurDAO.findById(id)?.toDTO(historiquePartieService.getAllForJoueur(id))
}
