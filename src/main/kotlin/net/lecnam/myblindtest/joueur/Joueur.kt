package net.lecnam.myblindtest.joueur

import net.lecnam.myblindtest.base.HasId
import net.lecnam.myblindtest.partie.historique.HistoriquePartieDTO
import org.jdbi.v3.core.mapper.reflect.ColumnName
import java.time.OffsetDateTime
import java.util.Objects

interface JoueurData : HasId {
    val login: String
    val pseudo: String?
    val codePartieParticipant: String?
}

data class Joueur(
    override val id: Long,
    override val login: String,
    override val pseudo: String?,
    @ColumnName("code_partie")
    override val codePartieParticipant: String?,
    val dateCreation: OffsetDateTime
) : JoueurData

data class JoueurDTO(
    override val id: Long,
    override val login: String,
    override val pseudo: String?,
    override val codePartieParticipant: String?,
    val historiquesParties: List<HistoriquePartieDTO>
) : JoueurData {
    override fun hashCode(): Int {
        return Objects.hash(id, login, pseudo)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as JoueurDTO

        return id == other.id &&
            login == other.login &&
            pseudo == other.pseudo
    }
}

data class JoueurInSession(
    override val id: Long,
    override val login: String,
    override val pseudo: String?
) : JoueurData {
    override val codePartieParticipant: String? = null
}

fun Joueur.toDTO(historiquesParties: List<HistoriquePartieDTO>) = JoueurDTO(
    id,
    login,
    pseudo,
    codePartieParticipant,
    historiquesParties
)

fun JoueurDTO.toInSession() = JoueurInSession(
    id,
    login,
    pseudo
)
