package net.lecnam.myblindtest.authentication

import net.lecnam.myblindtest.joueur.JoueurDTO
import net.lecnam.myblindtest.joueur.JoueurInSession
import net.lecnam.myblindtest.joueur.toInSession
import org.jdbi.v3.core.mapper.reflect.ColumnName
import java.time.OffsetDateTime
import java.util.UUID

data class Authentication(
    val id: UUID,
    @ColumnName("fk_joueur")
    val refJoueur: Long,
    val dateDebut: OffsetDateTime,
    val dateFin: OffsetDateTime
)

data class AuthenticationDTO(
    val joueur: Long,
    val dateDebut: OffsetDateTime,
    val dateFin: OffsetDateTime
)

data class AuthenticationInSession(
    val ref: String,
    val joueur: JoueurInSession
)

fun Authentication.toInSession(joueur: JoueurDTO) = toInSession(joueur.toInSession())

fun Authentication.toInSession(joueur: JoueurInSession) = AuthenticationInSession(
    id.toString(),
    joueur
)

fun Authentication.toDTO() = AuthenticationDTO(
    refJoueur,
    dateDebut,
    dateFin
)
