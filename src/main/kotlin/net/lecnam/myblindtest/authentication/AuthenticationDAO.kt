package net.lecnam.myblindtest.authentication

import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys
import org.jdbi.v3.sqlobject.statement.SqlQuery
import org.jdbi.v3.sqlobject.statement.SqlUpdate
import org.jdbi.v3.sqlobject.transaction.Transaction
import java.time.OffsetDateTime
import java.util.UUID

interface AuthenticationDAO {

    @Transaction
    @SqlUpdate("insert into joueur_auth(fk_joueur, date_debut, date_fin) values (:refJoueur, :dateDebut, :dateFin)")
    @GetGeneratedKeys
    fun createAuthentication(refJoueur: Long, dateDebut: OffsetDateTime, dateFin: OffsetDateTime): Authentication

    @Transaction
    @SqlUpdate("update joueur_auth set date_fin = now() where id = :uuid and date_fin > now()")
    fun terminateAuthentication(uuid: UUID)

    @SqlQuery("select exists(select 1 from joueur_auth where id = :uuid and now() between date_debut and date_fin)")
    fun isValid(uuid: UUID): Boolean

    @Transaction
    @SqlUpdate("insert into joueur(login, passhash) values (:login, crypt(:password, gen_salt('md5')))")
    fun register(login: String, password: String)

    @SqlQuery("select exists(select 1 from joueur where login = :login)")
    fun existsByLogin(login: String): Boolean

    @SqlQuery("select id from joueur where login = :login and (passhash = crypt(:password, passhash))")
    fun findMatching(login: String, password: String): Long?
}
