package net.lecnam.myblindtest.authentication

import arrow.core.Either
import arrow.core.left
import arrow.core.right
import com.athaydes.kunion.Union
import com.athaydes.kunion.Union.U2.Companion.toU2A
import com.athaydes.kunion.Union.U2.Companion.toU2B
import io.konform.validation.Invalid
import io.konform.validation.Validation
import io.konform.validation.ValidationErrors
import io.konform.validation.jsonschema.maxLength
import io.konform.validation.jsonschema.minLength
import io.konform.validation.jsonschema.pattern
import net.lecnam.myblindtest.Configuration
import net.lecnam.myblindtest.exceptions.AlreadyExist
import net.lecnam.myblindtest.joueur.JoueurDTO
import net.lecnam.myblindtest.joueur.JoueurService
import net.lecnam.myblindtest.joueur.toInSession
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.kotlin.inTransactionUnchecked
import java.time.OffsetDateTime
import java.util.UUID

class AuthenticationServiceImpl(
    private val configuration: Configuration,
    private val joueurService: JoueurService,
    private val authenticationDAO: AuthenticationDAO,
    private val jdbi: Jdbi
) : AuthenticationService {

    private data class UserPassword(
        val login: String,
        val password: String
    )

    private val validateUserPassword = Validation {
        UserPassword::login {
            minLength(1)
            maxLength(50)
            pattern("^\\S*$") hint "Le login ne doit contenir aucun espace"
            pattern("^[\\w_-]+$") hint "Le login ne peut être qu'une combinaison de lettres majuscules, minuscules, de chiffres, et de tirets ('-' ou '_')"
        }

        UserPassword::password {
            minLength(1)
            maxLength(100) // Parfaitement arbitraire
        }
    }

    override fun register(
        login: String,
        password: String
    ): Either<Union.U2<ValidationErrors, AlreadyExist.Login>, Authentication> {
        val validationResult = validateUserPassword(UserPassword(login, password))

        return when {
            authenticationDAO.existsByLogin(login) -> AlreadyExist.Login(login).toU2B().left()
            validationResult is Invalid -> validationResult.errors.toU2A().left()
            else -> {
                jdbi.inTransactionUnchecked {
                    authenticationDAO.register(login, password)
                    authenticate(findMatchingJoueur(login, password)!!).right()
                }
            }
        }
    }

    override fun existsJoueur(login: String): Boolean {
        return authenticationDAO.existsByLogin(login)
    }

    override fun findMatchingJoueur(login: String, password: String): JoueurDTO? {
        return authenticationDAO.findMatching(login, password)?.let { joueurService.findById(it) }
    }

    override fun authenticate(joueur: JoueurDTO): Authentication {
        return authenticate(joueur.id)
    }

    private fun authenticate(joueurId: Long): Authentication {
        // TODO [Improve testability] utiliser un service plutôt qu'une fonction statique
        //  ce qui permettra aussi d'éviter les problèmes de timezone côté client
        val dateDebut = OffsetDateTime.now()
        return authenticationDAO.createAuthentication(
            joueurId,
            dateDebut,
            dateDebut + configuration.ktor.session.maxDuration
        )
    }

    override fun logout(auth: AuthenticationInSession) {
        authenticationDAO.terminateAuthentication(UUID.fromString(auth.ref))
    }

    override fun refresh(auth: AuthenticationInSession): Authentication {
        logout(auth)
        return authenticate(auth.joueur.id)
    }

    override fun isValid(auth: AuthenticationInSession): Boolean {
        return joueurService.findById(auth.joueur.id)?.toInSession() == auth.joueur &&
            authenticationDAO.isValid(UUID.fromString(auth.ref))
    }
}
