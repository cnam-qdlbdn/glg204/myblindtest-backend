package net.lecnam.myblindtest.authentication

import arrow.core.Either
import com.athaydes.kunion.Union
import io.konform.validation.ValidationErrors
import net.lecnam.myblindtest.exceptions.AlreadyExist
import net.lecnam.myblindtest.joueur.JoueurDTO

interface AuthenticationService {
    fun register(
        login: String,
        password: String
    ): Either<Union.U2<ValidationErrors, AlreadyExist.Login>, Authentication>

    fun findMatchingJoueur(login: String, password: String): JoueurDTO?

    fun authenticate(joueur: JoueurDTO): Authentication

    fun logout(auth: AuthenticationInSession)

    fun refresh(auth: AuthenticationInSession): Authentication

    fun isValid(auth: AuthenticationInSession): Boolean

    fun existsJoueur(login: String): Boolean
}
