package net.lecnam.myblindtest.authentication

import io.github.oshai.KotlinLogging
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.*
import io.ktor.server.auth.UserPasswordCredential
import io.ktor.server.auth.authenticate
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.Routing
import io.ktor.server.routing.post
import io.ktor.server.sessions.clear
import io.ktor.server.sessions.sessions
import io.ktor.server.sessions.set
import net.lecnam.myblindtest.exceptions.toErrorMessageContainer
import net.lecnam.myblindtest.ext.badRequest
import net.lecnam.myblindtest.ext.getPrincipalAuth
import net.lecnam.myblindtest.ext.toMessage
import net.lecnam.myblindtest.joueur.JoueurService
import net.lecnam.myblindtest.plugins.SESSION_KEY
import net.lecnam.myblindtest.plugins.UserSession
import org.kodein.di.instance
import org.kodein.di.ktor.closestDI

private val logger = KotlinLogging.logger {}

fun Routing.authenticationRouting() {
    post("/register") {
        val credential = call.receive<UserPasswordCredential>()
        val authenticationService by closestDI().instance<AuthenticationService>()
        val joueurService by closestDI().instance<JoueurService>()

        authenticationService.register(credential.name, credential.password)
            .mapLeft { u ->
                u.use(
                    { v ->
                        v.map { e ->
                            e.toMessage(
                                ".login" to "Le login",
                                ".password" to "Le mot de passe"
                            )
                        }
                    },
                    { listOf(it.message) }
                ).toErrorMessageContainer()
            }
            .fold(
                { badRequest(it) },
                {
                    logger.info { "Compte [${credential.name}] créé !" }

                    val joueur = joueurService.findById(it.refJoueur)!!

                    call.sessions.set(UserSession(it.toInSession(joueur)))
                    call.respond(it.toDTO())
                }
            )
    }

    post("/login") {
        val credential = call.receive<UserPasswordCredential>()
        val authenticationService by closestDI().instance<AuthenticationService>()

        val joueur = authenticationService.findMatchingJoueur(credential.name, credential.password)

        if (joueur == null) {
            call.respond(HttpStatusCode.Unauthorized, "Identifiants invalides".toErrorMessageContainer())
        } else {
            logger.info { "Connexion de [${joueur.login}]" }
            val auth = authenticationService.authenticate(joueur)

            call.sessions.set(UserSession(auth.toInSession(joueur)))
            call.respond(auth.toDTO())
        }
    }

    authenticate(SESSION_KEY) {
        post("/refresh") {
            val auth = getPrincipalAuth()
            val login = auth.joueur.login

            logger.info { "[$login] Rafraîchissement du cookie de session" }

            val authenticationService by closestDI().instance<AuthenticationService>()
            val refreshed = authenticationService.refresh(auth)

            call.sessions.set(UserSession(refreshed.toInSession(auth.joueur)))
            call.respond(refreshed.toDTO())
        }

        post("/logout") {
            val auth = getPrincipalAuth()
            val login = auth.joueur.login
            logger.info { "Déconnexion du joueur [$login]" }

            val authenticationService by closestDI().instance<AuthenticationService>()
            authenticationService.logout(auth)

            call.sessions.clear<UserSession>()
            call.respond(HttpStatusCode.NoContent)
        }
    }
}
